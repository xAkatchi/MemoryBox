# This migration adds the initial value for the storage_type
# table, this can then be used by the storage table as a
# discriminator field.

# --- !Ups

INSERT INTO storage_type (storage_type) VALUES ('FILE_SYSTEM');

# --- !Downs

DELETE FROM storage_type WHERE storage_type = 'FILE_SYSTEM';