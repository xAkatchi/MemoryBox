# This migration adds a slug field to the collection_item table.
# In this field we can store an identifier which we can use in the URL's to identify a collection item

# --- !Ups

ALTER TABLE collection_item ADD slug VARCHAR(255) NOT NULL AFTER id;
ALTER TABLE collection_item ADD UNIQUE fk_collection_item_slug_unique (slug);

# --- !Downs

ALTER TABLE collection_item DROP slug;