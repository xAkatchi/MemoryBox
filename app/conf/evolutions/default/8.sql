# This migration adds the 'general' token table structure, but it also adds the 'user' specific
# token implementation and it provides some default values for the token_type table
#
# The goal of the token table structure is to have a generic setup that can be used whenever
# tokens are needed (e.g. account-activation tokens, password reset tokens, collection invitation tokens etc...)

# --- !Ups

CREATE TABLE token_type (
  id       INT(11)      NOT NULL AUTO_INCREMENT PRIMARY KEY,
  type     VARCHAR(255) NOT NULL,
  sub_type VARCHAR(255) NOT NULL,

  CONSTRAINT unique_type_sub_type UNIQUE (type, sub_type)
);

CREATE TABLE token (
  id          INT(11)      NOT NULL AUTO_INCREMENT PRIMARY KEY,
  token       VARCHAR(255) NOT NULL,
  type_id     INT(11)      NOT NULL,
  valid_until TIMESTAMP    NOT NULL,
  created_at  TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,

  CONSTRAINT unique_token_type_id UNIQUE (token, type_id),

  CONSTRAINT fk_token_type_id
  FOREIGN KEY (type_id)
  REFERENCES token_type (id)
);

CREATE TABLE token_user (
  token_id INT(11)      NOT NULL PRIMARY KEY,
  user_id  VARCHAR(255) NOT NULL,

  CONSTRAINT fk_token_user_user_id
  FOREIGN KEY (user_id)
  REFERENCES user (id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

INSERT INTO token_type (type, sub_type)
VALUES ('USER', 'ACCOUNT_ACTIVATION');

# --- !Downs

DROP TABLE token_user;
DROP TABLE token;
DROP TABLE token_type;