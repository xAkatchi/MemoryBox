# This migration adds a table in which we can store the thumbnails for collection items.

# --- !Ups

CREATE TABLE collection_item_thumbnail (
  id                     VARCHAR(255) NOT NULL PRIMARY KEY,
  collection_item_id     INT(11)      NOT NULL,
  file_photo_details_id  INT(11)      NOT NULL,
  storage_file_system_id INT(11)      NOT NULL,

  CONSTRAINT fk_collection_item_thumbnail_collection_item_id
  FOREIGN KEY (collection_item_id)
  REFERENCES collection_item (id),

  CONSTRAINT fk_collection_item_thumbnail_file_photo_details_id
  FOREIGN KEY (file_photo_details_id)
  REFERENCES file_photo_details (id),

  CONSTRAINT fk_collection_item_thumbnail_storage_file_system_id
  FOREIGN KEY (storage_file_system_id)
  REFERENCES storage_file_system (id)
);

# --- !Downs

DROP TABLE collection_item_thumbnail;