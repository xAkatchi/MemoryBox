# This migration adds the basic tables required for Silhouette.
# It will add the tables which are used to store the
# objects that are being used by Silhouette.

#  --- !Ups

CREATE TABLE login_info (
  id           INT(11)      NOT NULL AUTO_INCREMENT PRIMARY KEY,
  provider_id  VARCHAR(255) NOT NULL,
  provider_key VARCHAR(255) NOT NULL UNIQUE
);

CREATE TABLE user_login_info (
  user_id       VARCHAR(255) NOT NULL,
  login_info_id INT(11)      NOT NULL,

  CONSTRAINT pk_user_login_info PRIMARY KEY (user_id, login_info_id),

  CONSTRAINT fk_user_login_info_user_id
  FOREIGN KEY (user_id)
  REFERENCES user (id),

  CONSTRAINT fk_user_login_info_login_info_id
  FOREIGN KEY (login_info_id)
  REFERENCES login_info (id)
);

CREATE TABLE password_info (
  id            INT(11)      NOT NULL AUTO_INCREMENT PRIMARY KEY,
  login_info_id INT(11)      NOT NULL,
  hasher        VARCHAR(255) NOT NULL,
  password      VARCHAR(255) NOT NULL,
  salt          VARCHAR(255),

  CONSTRAINT fk_password_info_login_info_id
  FOREIGN KEY (login_info_id)
  REFERENCES login_info (id)
);

# --- !Downs

DROP TABLE password_info;
DROP TABLE user_login_info;
DROP TABLE login_info;