# This migration adds the tables required for storing
# the collections.

# --- !Ups

CREATE TABLE collection (
  id          INT(11)      NOT NULL AUTO_INCREMENT PRIMARY KEY,
  slug        VARCHAR(255) NOT NULL UNIQUE,
  name        VARCHAR(255) NOT NULL,
  description TEXT         NOT NULL,
  created_at  TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE user_collection (
  user_id       VARCHAR(255) NOT NULL,
  collection_id INT(11)      NOT NULL,

  CONSTRAINT pk_user_collection PRIMARY KEY (user_id, collection_id),

  CONSTRAINT fk_user_collection_user_id
  FOREIGN KEY (user_id)
  REFERENCES user (id),

  CONSTRAINT fk_user_collection_collection
  FOREIGN KEY (collection_id)
  REFERENCES collection (id)
);

CREATE TABLE file_content_type (
  id           INT(11)      NOT NULL AUTO_INCREMENT PRIMARY KEY,
  content_type VARCHAR(255) NOT NULL UNIQUE
);

CREATE TABLE file_details (
  id              INT(11)          NOT NULL AUTO_INCREMENT PRIMARY KEY,
  content_type_id INT(11)          NOT NULL,
  file_size       INT(11) UNSIGNED NOT NULL
  COMMENT 'in bytes',
  hash            VARCHAR(255)     NOT NULL,

  CONSTRAINT fk_file_details_content_type_id
  FOREIGN KEY (content_type_id)
  REFERENCES file_content_type (id)
);

CREATE TABLE file_photo_details (
  id     INT(11) NOT NULL PRIMARY KEY,
  height INT(11) NOT NULL
  COMMENT 'in pixels',
  width  INT(11) NOT NULL
  COMMENT 'in pixels',

  CONSTRAINT fk_file_photo_details_id
  FOREIGN KEY (id)
  REFERENCES file_details (id)
);

CREATE TABLE storage_type (
  id           INT(11)      NOT NULL AUTO_INCREMENT PRIMARY KEY,
  storage_type VARCHAR(255) NOT NULL UNIQUE
);

CREATE TABLE storage (
  id              INT(11)   NOT NULL AUTO_INCREMENT PRIMARY KEY,
  storage_type_id INT(11)   NOT NULL,
  created_at      TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,

  CONSTRAINT fk_storage_storage_type_id
  FOREIGN KEY (storage_type_id)
  REFERENCES storage_type (id)
);

CREATE TABLE storage_file_system (
  id        INT(11)      NOT NULL PRIMARY KEY,
  file_path VARCHAR(255) NOT NULL
  COMMENT 'the absolute path to the file on the file system',

  CONSTRAINT fk_storage_file_system_id
  FOREIGN KEY (id)
  REFERENCES storage (id)
);

CREATE TABLE collection_item (
  id              INT(11)      NOT NULL AUTO_INCREMENT PRIMARY KEY,
  collection_id   INT(11)      NOT NULL,
  uploaded_by_id  VARCHAR(255) NOT NULL,
  storage_id      INT(11)      NOT NULL,
  file_details_id INT(11)      NOT NULL,
  name            VARCHAR(255) NOT NULL,
  description     TEXT         NOT NULL,
  created_at      TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,

  CONSTRAINT fk_collection_item_collection_id
  FOREIGN KEY (collection_id)
  REFERENCES collection (id),

  CONSTRAINT fk_collection_item_uploaded_by_id
  FOREIGN KEY (uploaded_by_id)
  REFERENCES user (id),

  CONSTRAINT fk_collection_item_storage_id
  FOREIGN KEY (storage_id)
  REFERENCES storage (id),

  CONSTRAINT fk_collection_item_file_details_id
  FOREIGN KEY (file_details_id)
  REFERENCES file_details (id)
);

# --- !Downs

DROP TABLE collection_item;
DROP TABLE storage_file_system;
DROP TABLE storage;
DROP TABLE storage_type;
DROP TABLE file_photo_details;
DROP TABLE file_details;
DROP TABLE file_content_type;
DROP TABLE user_collection;
DROP TABLE collection;
