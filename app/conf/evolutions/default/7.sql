# This migration makes the description field of the collection and collection items nullable
# We specifically don't null the collection_item.name because we'd like to use the file name value for that field
# (and it introduces no more risk then any other input field with regards to the security of using filenames)

# --- !Ups

ALTER TABLE collection MODIFY description TEXT NULL;
ALTER TABLE `collection_item` DROP `description`;

# --- !Downs

ALTER TABLE collection CHANGE `description` `description` TEXT NOT NULL;
ALTER TABLE `collection_item` ADD `description` TEXT NOT NULL AFTER `name`;
