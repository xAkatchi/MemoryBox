# This migration adds the table in which the User
# can be stored.

#  --- !Ups

CREATE TABLE user (
  id         VARCHAR(255) NOT NULL PRIMARY KEY,
  first_name VARCHAR(255) NOT NULL,
  last_name  VARCHAR(255) NOT NULL,
  email      VARCHAR(255) NOT NULL UNIQUE,
  avatar_url VARCHAR(255),
  activated  TINYINT(1)   NOT NULL DEFAULT 0
);

# --- !Downs

DROP TABLE user;