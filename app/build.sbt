name := """MemoryBox"""
organization := "io.codekrijger"

// We use a generic version, since we have git / docker to handle our versioning
// (e.g. we push images with a generic version, no need for double version management here)
version := "release"

// We don't want to have API documentation in the generated (dist) package
// thus we disable that here
sources in (Compile, doc) := Seq.empty
publishArtifact in (Compile, packageDoc) := false

// See: https://github.com/KyleU/boilerplay/issues/3
resolvers += Resolver.jcenterRepo
// See: https://github.com/sbt/sbt-native-packager/issues/1063
resolvers += Resolver.sbtPluginRepo("releases")

// Needed for Catz, as stated on their Github
scalacOptions += "-Ypartial-unification"

lazy val MemoryBox = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.12.6"

javaOptions in Test += "-Dconfig.file=conf/application.test.conf"
javaOptions in Test += "-Dlogger.file=conf/logback.test.xml"

// We'd like to remove the generated Reverse classes from the code coverage
// percentage since there is no point in measuring them (since we can't write
// tests for them).
// See: https://stackoverflow.com/a/44346781
coverageExcludedPackages := "<empty>;Reverse.*;router\\.*"

libraryDependencies ++= Seq(
  guice,
  ehcache,
  "com.h2database" % "h2" % "1.4.194" % Test,
  "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.2" % Test,
  // https://mvnrepository.com/artifact/org.mockito/mockito-core
  "org.mockito" % "mockito-core" % "2.15.0" % Test,
  // https://github.com/playframework/play-slick
  "com.typesafe.play" %% "play-slick" % "3.0.3",
  "com.typesafe.play" %% "play-slick-evolutions" % "3.0.3",
  // https://mvnrepository.com/artifact/mysql/mysql-connector-java
  "mysql" % "mysql-connector-java" % "5.1.41",
  // http://silhouette.rocks/
  "com.mohiva" %% "play-silhouette" % "5.0.0",
  "com.mohiva" %% "play-silhouette-password-bcrypt" % "5.0.0",
  "com.mohiva" %% "play-silhouette-crypto-jca" % "5.0.0",
  "com.mohiva" %% "play-silhouette-persistence" % "5.0.0",
  "com.mohiva" %% "play-silhouette-testkit" % "5.0.0" % "test",
  // https://github.com/codingwell/scala-guice
  "net.codingwell" %% "scala-guice" % "4.2.0",
  // https://github.com/iheartradio/ficus
  "com.iheart" %% "ficus" % "1.4.3",
  // https://github.com/typelevel/cats
  "org.typelevel" %% "cats-core" % "1.1.0",
  "org.typelevel" %% "cats-macros" % "1.1.0",
  "org.typelevel" %% "cats-kernel" % "1.1.0",
  // https://github.com/sksamuel/scrimage
  "com.sksamuel.scrimage" %% "scrimage-core" % "2.1.8",
  // https://github.com/playframework/play-mailer
  "com.typesafe.play" %% "play-mailer" % "6.0.1",
  "com.typesafe.play" %% "play-mailer-guice" % "6.0.1",
  // https://github.com/enragedginger/akka-quartz-scheduler
  "com.enragedginger" %% "akka-quartz-scheduler" % "1.7.1-akka-2.5.x",
)
