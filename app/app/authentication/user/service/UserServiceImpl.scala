package authentication.user.service

import java.util.UUID
import javax.inject.Inject

import authentication.user.User
import authentication.user.dao.UserDAO
import com.mohiva.play.silhouette.api.LoginInfo

import scala.concurrent.{ExecutionContext, Future}

/**
  * Handles actions to users.
  *
  * @param userDAO The user DAO implementation.
  * @param ex      The execution context.
  */
class UserServiceImpl @Inject()(userDAO: UserDAO)(implicit ex: ExecutionContext) extends UserService {

  /**
    * Retrieves a user that matches the specified id.
    *
    * @param id The id to retrieve a user.
    * @return The retrieved user or None if no user could be retrieved for the given id.
    */
  def retrieve(id: UUID): Future[Option[User]] = userDAO.find(id)

  /**
    * Retrieves a user that matches the specified login info.
    *
    * @param loginInfo The login info to retrieve a user.
    * @return The retrieved user or None if no user could be retrieved for the given login info.
    */
  def retrieve(loginInfo: LoginInfo): Future[Option[User]] = userDAO.find(loginInfo)

  /**
    * Retrieves a user that matches the given email
    *
    * @param email The email of the [[User]]
    * @return The retrieved [[User]] or None
    */
  def retrieve(email: String): Future[Option[User]] = userDAO.find(email)

  /**
    * Saves a user.
    *
    * @param user The user to save.
    * @return The saved user.
    */
  def save(user: User): Future[User] = userDAO.save(user)
}
