package authentication.user.service

import java.util.UUID

import authentication.user.User
import com.mohiva.play.silhouette.api.services.IdentityService
import com.mohiva.play.silhouette.impl.providers.CommonSocialProfile

import scala.concurrent.Future

/**
  * All the [[User]] related logic should be routed through the [[UserService]]
  * to the [[authentication.user.dao.UserDAO]] (thus the [[authentication.user.dao.UserDAO]]
  * shouldn't be invoked directly.
  */
trait UserService extends IdentityService[User] {

  /**
    * Retrieves a user that matches the specified id.
    *
    * @param id The id to retrieve a user.
    * @return The retrieved user or None if no user could be retrieved for the given id.
    */
  def retrieve(id: UUID): Future[Option[User]]

  /**
    * Retrieves a user that matches the given email
    *
    * @param email The email of the [[User]]
    * @return The retrieved [[User]] or None
    */
  def retrieve(email: String): Future[Option[User]]

  /**
    * Saves a user.
    *
    * @param user The user to save.
    * @return The saved user.
    */
  def save(user: User): Future[User]
}
