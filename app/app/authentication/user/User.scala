package authentication.user

import java.util.UUID

import com.mohiva.play.silhouette.api.{Identity, LoginInfo}
import play.api.libs.json.{JsValue, Json, Writes}

/**
  * The user object.
  *
  * @param id        The unique id of the user.
  * @param loginInfo The linked login info.
  * @param firstName Maybe the first name of the authenticated user.
  * @param lastName  Maybe the last name of the authenticated user.
  * @param email     Maybe the email of the authenticated provider.
  * @param avatarURL Maybe the avatar URL of the authenticated provider.
  * @param activated Indicates that the user has activated his/her account.
  */
case class User(id: UUID,
                loginInfo: LoginInfo,
                firstName: String,
                lastName: String,
                email: String,
                avatarURL: Option[String],
                activated: Boolean) extends Identity

object User {
  implicit val userWrites: Writes[User] = (user: User) => Json.obj(
    "id" -> user.id.toString,
    "firstName" -> user.firstName,
    "email" -> user.email,
    "lastName" -> user.lastName,
    "avatarURL" -> user.avatarURL
  )
}
