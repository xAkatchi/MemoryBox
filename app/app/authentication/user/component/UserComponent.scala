package authentication.user.component

import play.api.db.slick.HasDatabaseConfigProvider
import slick.jdbc.JdbcProfile

/**
  * A trait containing a reference to the `user` table.
  *
  * Extend this trait whenever you have a desire to do something with the user table.
  * The `user` value is already defined and thus can be used straight away to query the table.
  *
  * [[https://github.com/playframework/play-slick/blob/master/samples/computer-database/app/dao/ComputersDAO.scala
  * Used as inspiration]]
  */
trait UserComponent {
  self: HasDatabaseConfigProvider[JdbcProfile] =>

  import profile.api._

  protected val tblUser = TableQuery[Users]

  /**
    * The case class that represents the entries inside the table.
    *
    * @param id        The id of the user (as stringified [[java.util.UUID]]
    * @param firstName The first name of the user
    * @param lastName  The last name of the user
    * @param email     The email of the user
    * @param avatarURL The (possible) url to the avatar of the user
    * @param activated A flag indicating whether or not the account is activatedç
    */
  case class DBUser(id: String, firstName: String, lastName: String,
                    email: String, avatarURL: Option[String], activated: Boolean)

  /**
    * This class contains a mapping from the database table to an object.
    * ([[http://slick.lightbend.com/doc/3.0.3/gettingstarted.html#schema as required by Slick]])
    *
    * @param tag
    */
  class Users(tag: Tag) extends Table[DBUser](tag, "user") {
    def id = column[String]("id", O.PrimaryKey)
    def firstName = column[String]("first_name")
    def lastName = column[String]("last_name")
    def email = column[String]("email", O.Unique)
    def avatarURL = column[Option[String]]("avatar_url")
    def activated = column[Boolean]("activated")

    def * = (id, firstName, lastName, email, avatarURL, activated) <> (DBUser.tupled, DBUser.unapply)
  }

}
