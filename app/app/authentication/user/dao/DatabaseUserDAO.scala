package authentication.user.dao

import java.util.UUID
import javax.inject.{Inject, Singleton}

import authentication.authInfo.component.{LoginInfoComponent, UserLoginInfoComponent}
import authentication.user.User
import authentication.user.component.UserComponent
import com.mohiva.play.silhouette.api.LoginInfo
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

/**
  * An implementation of the UserDAO using a Database (through Slick)
  * as storage layer.
  *
  * @param dbConfigProvider The slick database that this DAO is using internally.
  * @param executionContext A CPU bound execution context. Slick manages blocking JDBC calls with its
  *                         own internal thread pool, so Play's default execution context is fine here.
  */
@Singleton
class DatabaseUserDAO @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)
                               (implicit executionContext: ExecutionContext)
  extends UserDAO
    with HasDatabaseConfigProvider[JdbcProfile]
    with UserComponent
    with UserLoginInfoComponent
    with LoginInfoComponent {

  import profile.api._

  /**
    * Finds a user by its login info.
    *
    * @param loginInfo The login info of the user to find.
    * @return The found user or None if no user for the given login info could be found.
    */
  def find(loginInfo: LoginInfo): Future[Option[User]] = {
    val query = for {
      dbLoginInfo <- tblLoginInfo
        if dbLoginInfo.providerId === loginInfo.providerID && dbLoginInfo.providerKey === loginInfo.providerKey
      dbUserLoginInfo <- tblUserLoginInfo if dbUserLoginInfo.loginInfoId === dbLoginInfo.id
      dbUser <- tblUser if dbUserLoginInfo.userId === dbUser.id
    } yield (dbUser, dbLoginInfo)

    db.run(query.result.headOption).map(maybeResult => maybeResult.map {
      case (dbUser: DBUser, dbLoginInfo: DBLoginInfo) => createUser(dbUser, dbLoginInfo)
    })
  }

  /**
    * Finds a user by its user id.
    *
    * @param userId The id of the user to find.
    * @return The found user or None if no user for the given id could be found.
    */
  def find(userId: UUID): Future[Option[User]] = {
    val query = for {
      dbUser <- tblUser if dbUser.id === userId.toString
      dbUserLoginInfo <- tblUserLoginInfo if dbUserLoginInfo.userId === dbUser.id
      dbLoginInfo <- tblLoginInfo if dbUserLoginInfo.loginInfoId === dbLoginInfo.id
    } yield (dbUser, dbLoginInfo)

    db.run(query.result.headOption).map(maybeResult => maybeResult.map {
      case (dbUser: DBUser, dbLoginInfo: DBLoginInfo) => createUser(dbUser, dbLoginInfo)
    })
  }

  /**
    * Finds a user by its email.
    *
    * @param email The email of the [[User]] to find.
    * @return The found [[User]] or None
    */
  def find(email: String): Future[Option[User]] = {
    val query = for {
      dbUser <- tblUser if dbUser.email === email
      dbUserLoginInfo <- tblUserLoginInfo if dbUserLoginInfo.userId === dbUser.id
      dbLoginInfo <- tblLoginInfo if dbUserLoginInfo.loginInfoId === dbLoginInfo.id
    } yield (dbUser, dbLoginInfo)

    db.run(query.result.headOption).map(maybeResult => maybeResult.map {
      case (dbUser: DBUser, dbLoginInfo: DBLoginInfo) => createUser(dbUser, dbLoginInfo)
    })
  }

  /**
    * Saves a user.
    *
    * @param userToSave The user to save.
    * @return The saved user.
    */
  def save(userToSave: User): Future[User] = {
    val wrappedUser = DBUser(
      userToSave.id.toString,
      userToSave.firstName,
      userToSave.lastName,
      userToSave.email,
      userToSave.avatarURL,
      userToSave.activated
    )
    val wrappedLoginInfo = DBLoginInfo(
      None,
      userToSave.loginInfo.providerID,
      userToSave.loginInfo.providerKey
    )

    val loginInfoAction = {
      val insertLoginInfo = tblLoginInfo.returning(tblLoginInfo.map(_.id))
        .into((info, id) => info.copy(id = Some(id))) += wrappedLoginInfo

      for {
        loginInfoOption <- findDBLoginInfoByLoginInfo(userToSave.loginInfo).result.headOption
        loginInfo <- loginInfoOption.map(DBIO.successful).getOrElse(insertLoginInfo)
      } yield loginInfo
    }

    def userLoginInfoAction(loginInfo: DBLoginInfo) = {
      tblUserLoginInfo.filter(_.userId === wrappedUser.id).exists.result.flatMap { exists =>
        if (exists) {
          DBIO.successful(None)
        } else {
          tblUserLoginInfo += DBUserLoginInfo(wrappedUser.id, loginInfo.id.get)
        }
      }
    }

    val actions = (for {
      _ <- tblUser.insertOrUpdate(wrappedUser)
      loginInfo <- loginInfoAction
      _ <- userLoginInfoAction(loginInfo)
    } yield ()).transactionally

    db.run(actions).map(_ => userToSave)
  }

  /**
    * A 'factory' function that creates a [[User]] from the given details.
    *
    * @param dbUser The DB entry representing a [[User]]
    * @param dbLoginInfo The DB entry containing the login credentials of the [[User]]
    * @return A [[User]]
    */
  def createUser(dbUser: DBUser, dbLoginInfo: DBLoginInfo): User = User(
    UUID.fromString(dbUser.id),
    LoginInfo(dbLoginInfo.providerId, dbLoginInfo.providerKey),
    dbUser.firstName,
    dbUser.lastName,
    dbUser.email,
    dbUser.avatarURL,
    dbUser.activated
  )
}
