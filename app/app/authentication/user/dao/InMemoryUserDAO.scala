package authentication.user.dao

import java.util.UUID
import javax.inject.Singleton

import authentication.user.User
import com.mohiva.play.silhouette.api.LoginInfo

import scala.collection.mutable
import scala.concurrent.Future

/**
  * An implementation of the UserDAO using an in-memory HashMap
  * as storage layer.
  */
@Singleton
class InMemoryUserDAO extends UserDAO {
  val users: mutable.HashMap[UUID, User] = mutable.HashMap()

  /**
    * Finds a user by its login info.
    *
    * @param loginInfo The login info of the user to find.
    * @return The found user or None if no user for the given login info could be found.
    */
  def find(loginInfo: LoginInfo): Future[Option[User]] = Future.successful(
    users.find(_._2.loginInfo == loginInfo).map(_._2)
  )

  /**
    * Finds a user by its user id.
    *
    * @param userId The id of the user to find.
    * @return The found user or None if no user for the given id could be found.
    */
  def find(userId: UUID): Future[Option[User]] =
    Future.successful(users.get(userId))

  /**
    * Finds a user by its email.
    *
    * @param email The email of the [[User]] to find.
    * @return The found [[User]] or None
    */
  def find(email: String): Future[Option[User]] =
    Future.successful(users.find(_._2.email == email).map(_._2))

  /**
    * Saves a user.
    *
    * @param user The user to save.
    * @return The saved user.
    */
  def save(user: User): Future[User] = {
    users += (user.id -> user)
    Future.successful(user)
  }
}
