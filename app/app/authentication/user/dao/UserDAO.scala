package authentication.user.dao

import java.util.UUID

import authentication.user.User
import com.mohiva.play.silhouette.api.LoginInfo

import scala.concurrent.Future

/**
  * Implementations of this class should give access to the user object.
  */
trait UserDAO {

  /**
    * Finds a user by its login info.
    *
    * @param loginInfo The login info of the user to find.
    * @return The found user or None if no user for the given login info could be found.
    */
  def find(loginInfo: LoginInfo): Future[Option[User]]

  /**
    * Finds a user by its user id.
    *
    * @param userId The id of the user to find.
    * @return The found user or None if no user for the given id could be found.
    */
  def find(userId: UUID): Future[Option[User]]

  /**
    * Finds a user by its email.
    *
    * @param email The email of the [[User]] to find.
    * @return The found [[User]] or None
    */
  def find(email: String): Future[Option[User]]

  /**
    * Saves a user.
    *
    * @param user The user to save.
    * @return The saved user.
    */
  def save(user: User): Future[User]
}
