package authentication.authInfo.component

import authentication.user.component.UserComponent
import play.api.db.slick.HasDatabaseConfigProvider
import slick.jdbc.JdbcProfile

/**
  * A trait containing a reference to the `user_login_info` table.
  *
  * Extend this trait whenever you have a desire to do something with the user_login_info table.
  * The `user_login_info` value is already defined and thus can be used straight away to query the table.
  *
  * [[https://github.com/playframework/play-slick/blob/master/samples/computer-database/app/dao/ComputersDAO.scala
  * Used as inspiration]]
  */
trait UserLoginInfoComponent extends UserComponent with LoginInfoComponent {
  self: HasDatabaseConfigProvider[JdbcProfile] =>

  import profile.api._

  protected val tblUserLoginInfo = TableQuery[UserLoginInfos]

  /**
    * The case class that represents the entries inside the table.
    *
    * @param userId The id of the [[authentication.user.User]]
    * @param loginInfoId The id of the [[DBLoginInfo]]
    */
  case class DBUserLoginInfo(userId: String, loginInfoId: Int)

  /**
    * This class contains a mapping from the database table to an object.
    * ([[http://slick.lightbend.com/doc/3.0.3/gettingstarted.html#schema as required by Slick]])
    *
    * @param tag
    */
  class UserLoginInfos(tag: Tag) extends Table[DBUserLoginInfo](tag, "user_login_info") {
    def userId = column[String]("user_id")
    def loginInfoId = column[Int]("login_info_id")

    def pk = primaryKey("pk_user_login_info", (userId, loginInfoId))

    def userFK = foreignKey("fk_user_login_info_user_id", userId, tblUser)(_.id)
    def loginInfoFK = foreignKey("fk_user_login_info_login_info_id", loginInfoId, tblLoginInfo)(_.id)

    def * = (userId, loginInfoId) <> (DBUserLoginInfo.tupled, DBUserLoginInfo.unapply)
  }
}
