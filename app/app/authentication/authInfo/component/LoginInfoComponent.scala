package authentication.authInfo.component

import com.mohiva.play.silhouette.api.LoginInfo
import play.api.db.slick.HasDatabaseConfigProvider
import slick.jdbc.JdbcProfile

/**
  * A trait containing a reference to the `login_info` table.
  *
  * Extend this trait whenever you have a desire to do something with the login_info table.
  * The `login_info` value is already defined and thus can be used straight away to query the table.
  *
  * [[https://github.com/playframework/play-slick/blob/master/samples/computer-database/app/dao/ComputersDAO.scala
  * Used as inspiration]]
  */
trait LoginInfoComponent {
  self: HasDatabaseConfigProvider[JdbcProfile] =>

  import profile.api._

  protected val tblLoginInfo = TableQuery[LoginInfos]

  /**
    * The case class the represents the entries inside the table.
    *
    * @param id          The primary key
    * @param providerId  The id of the provider (e.g. 'credentials')
    * @param providerKey The key that's associated with the provider (e.g. the email)
    */
  case class DBLoginInfo(id: Option[Int], providerId: String, providerKey: String)

  /**
    * This class contains a mapping from the database table to an object.
    * ([[http://slick.lightbend.com/doc/3.0.3/gettingstarted.html#schema as required by Slick]])
    *
    * @param tag
    */
  protected class LoginInfos(tag: Tag) extends Table[DBLoginInfo](tag, "login_info") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def providerId = column[String]("provider_id")
    def providerKey = column[String]("provider_key", O.Unique)

    def * = (id.?, providerId, providerKey) <> (DBLoginInfo.tupled, DBLoginInfo.unapply)
  }

  /**
    * This method will try to find a [[DBLoginInfo]] that's a representation for the given
    * [[LoginInfo]]
    *
    * @param loginInfo The [[LoginInfo]] who's counterpart [[DBLoginInfo]] should be found
    * @return
    */
  protected def findDBLoginInfoByLoginInfo(loginInfo: LoginInfo): Query[LoginInfos, DBLoginInfo, Seq] =
    tblLoginInfo.filter(dbLoginInfo =>
      dbLoginInfo.providerId === loginInfo.providerID &&
      dbLoginInfo.providerKey === loginInfo.providerKey
    )
}
