package authentication.authInfo.component

import com.mohiva.play.silhouette.api.LoginInfo
import play.api.db.slick.HasDatabaseConfigProvider
import slick.jdbc.JdbcProfile

/**
  * A trait containing a reference to the `password_info` table.
  *
  * Extend this trait whenever you have a desire to do something with the password_info table.
  * The `password_info` value is already defined and thus can be used straight away to query the table.
  *
  * [[https://github.com/playframework/play-slick/blob/master/samples/computer-database/app/dao/ComputersDAO.scala
  * Used as inspiration]]
  */
trait PasswordInfoComponent extends LoginInfoComponent {
  self: HasDatabaseConfigProvider[JdbcProfile] =>

  import profile.api._

  protected val tblPasswordInfo = TableQuery[PasswordInfos]

  /**
    * The case class that represents the entries inside the table.
    *
    * @param id          The primary key
    * @param loginInfoId The linked [[LoginInfoComponent.DBLoginInfo]]
    * @param hasher      The used hasher
    * @param password    The encrypted password
    * @param salt        The salt
    */
  case class DBPasswordInfo(id: Option[Int], loginInfoId: Int, hasher: String, password: String, salt: Option[String])

  /**
    * This class contains a mapping from the database table to an object.
    * ([[http://slick.lightbend.com/doc/3.0.3/gettingstarted.html#schema as required by Slick]])
    *
    * @param tag
    */
  class PasswordInfos(tag: Tag) extends Table[DBPasswordInfo](tag, "password_info") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def loginInfoId = column[Int]("login_info_id")
    def hasher = column[String]("hasher")
    def password = column[String]("password")
    def salt = column[Option[String]]("salt")

    def loginInfoFK = foreignKey("fk_password_info_login_info_id", loginInfoId, tblLoginInfo)(_.id)

    def * = (id.?, loginInfoId, hasher, password, salt) <> (DBPasswordInfo.tupled, DBPasswordInfo.unapply)
  }

  /**
    * This method will try to find a [[DBPasswordInfo]] that's linked to the given
    * [[LoginInfo]] (through the [[DBLoginInfo]] object)
    *
    * @param loginInfo The [[LoginInfo]] who's related [[DBPasswordInfo]] should be found
    * @return
    */
  protected def findDBPasswordInfoByLoginInfo(loginInfo: LoginInfo): Query[PasswordInfos, DBPasswordInfo, Seq] = for {
    dbLoginInfo <- findDBLoginInfoByLoginInfo(loginInfo)
    dbPasswordInfo <- tblPasswordInfo if dbPasswordInfo.loginInfoId === dbLoginInfo.id
  } yield dbPasswordInfo

  /**
    * This method will try to find all the [[DBPasswordInfo]] objects that contain the
    * given [[LoginInfo]].
    *
    * We use a subquery workaround here instead of a join to get the [[DBPasswordInfo]]
    * this because Slick only supports selecting from a single table for update/delete queries.
    * (see: [[https://github.com/slick/slick/issues/684 link]]).
    *
    * @param loginInfo The [[LoginInfo]] who's related [[DBPasswordInfo]] should be found
    * @return
    */
  protected def findDBPasswordInfosWithLoginInfo(loginInfo: LoginInfo): Query[PasswordInfos, DBPasswordInfo, Seq] =
    tblPasswordInfo.filter(_.loginInfoId in findDBLoginInfoByLoginInfo(loginInfo).map(_.id))
}
