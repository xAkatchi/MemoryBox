package authentication.authInfo.dao

import javax.inject.{Inject, Singleton}

import authentication.authInfo.component.{LoginInfoComponent, PasswordInfoComponent}
import com.mohiva.play.silhouette.api.LoginInfo
import com.mohiva.play.silhouette.api.util.PasswordInfo
import com.mohiva.play.silhouette.persistence.daos.DelegableAuthInfoDAO
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

/**
  * An implementation of the DelegableAuthInfoDAO using a Database (through Slick)
  * as storage layer.
  *
  * Most of the source code from this class has been obtained from over
  * [[https://github.com/sbrunk/play-silhouette-slick-seed/blob/master/app/models/daos/PasswordInfoDAO.scala here]].
  *
  * @param dbConfigProvider The slick database that this DAO is using internally.
  * @param executionContext A CPU bound execution context. Slick manages blocking JDBC calls with its
  *                         own internal thread pool, so Play's default execution context is fine here.
  */
@Singleton
class PasswordInfoDAO @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)
                               (implicit executionContext: ExecutionContext)
  extends DelegableAuthInfoDAO[PasswordInfo]
    with HasDatabaseConfigProvider[JdbcProfile]
    with LoginInfoComponent
    with PasswordInfoComponent {

  import profile.api._

  /**
    * Finds the auth info which is linked with the specified login info.
    *
    * @param loginInfo The linked login info.
    * @return The retrieved auth info or None if no auth info could be retrieved for the given login info.
    */
  def find(loginInfo: LoginInfo): Future[Option[PasswordInfo]] =
    db.run(findDBPasswordInfoByLoginInfo(loginInfo).result.headOption).map(_.map(dbPasswordInfo =>
      PasswordInfo(dbPasswordInfo.hasher, dbPasswordInfo.password, dbPasswordInfo.salt))
    )

  /**
    * Adds new auth info for the given login info.
    *
    * @param loginInfo The login info for which the auth info should be added.
    * @param authInfo  The auth info to add.
    * @return The added auth info.
    */
  def add(loginInfo: LoginInfo, authInfo: PasswordInfo): Future[PasswordInfo] = {
    val query = findDBLoginInfoByLoginInfo(loginInfo).result.head.flatMap(dbLoginInfo =>
      tblPasswordInfo += DBPasswordInfo(None, dbLoginInfo.id.get, authInfo.hasher, authInfo.password, authInfo.salt)
    ).transactionally

    db.run(query).map(_ => authInfo)
  }

  /**
    * Updates the auth info for the given login info.
    *
    * @param loginInfo The login info for which the auth info should be updated.
    * @param authInfo  The auth info to update.
    * @return The updated auth info.
    */
  def update(loginInfo: LoginInfo, authInfo: PasswordInfo): Future[PasswordInfo] = {
    val query = findDBPasswordInfosWithLoginInfo(loginInfo)
        .map(dbPasswordInfo => (dbPasswordInfo.hasher, dbPasswordInfo.password, dbPasswordInfo.salt))
        .update((authInfo.hasher, authInfo.password, authInfo.salt))

    db.run(query).map(_ => authInfo)
  }

  /**
    * Saves the auth info for the given login info.
    *
    * This method either adds the auth info if it doesn't exists or it updates the auth info
    * if it already exists.
    *
    * @param loginInfo The login info for which the auth info should be saved.
    * @param authInfo  The auth info to save.
    * @return The saved auth info.
    */
  def save(loginInfo: LoginInfo, authInfo: PasswordInfo): Future[PasswordInfo] =
    find(loginInfo).flatMap {
      case Some(_) => update(loginInfo, authInfo)
      case None => add(loginInfo, authInfo)
    }

  /**
    * Removes the auth info for the given login info.
    *
    * @param loginInfo The login info for which the auth info should be removed.
    * @return A future to wait for the process to be completed.
    */
  def remove(loginInfo: LoginInfo): Future[Unit] =
    db.run(findDBPasswordInfosWithLoginInfo(loginInfo).delete).map(_ => ())
}
