package authentication.utils

import authentication.user.User
import com.mohiva.play.silhouette.api.Env
import com.mohiva.play.silhouette.impl.authenticators.JWTAuthenticator

/**
  * The environment defines the key components needed by a Silhouette endpoint.
  *
  * [[https://www.silhouette.rocks/docs/environment Documentation]]
  */
trait JwtEnv extends Env {
  type I = User
  type A = JWTAuthenticator
}
