package authentication.api

import java.util.UUID

import javax.inject.Inject
import authentication.api.SignUpForm.Data
import authentication.utils.JwtEnv
import authentication.user.User
import authentication.user.service.UserService
import authentication.api.SignUpForm.form
import utils.FormErrorWrites
import com.mohiva.play.silhouette.api._
import com.mohiva.play.silhouette.api.repositories.AuthInfoRepository
import com.mohiva.play.silhouette.api.services.AvatarService
import com.mohiva.play.silhouette.api.util.PasswordHasherRegistry
import com.mohiva.play.silhouette.impl.providers.CredentialsProvider
import play.api.data.FormError
import play.api.i18n.{I18nSupport, Messages}
import play.api.libs.json.{JsValue, Json, Writes}
import play.api.mvc.{AbstractController, Action, ControllerComponents}

import scala.concurrent.{ExecutionContext, Future}

/**
  * The `Sign Up` controller.
  *
  * @param cc                     The controller components
  * @param silhouette             The Silhouette stack.
  * @param userService            The user service implementation.
  * @param authInfoRepository     The auth info repository implementation.
  * @param avatarService          The avatar service implementation.
  * @param passwordHasherRegistry The password hasher registry.
  * @param ec                     The used execution context.
  */
class SignUpController @Inject()(cc: ControllerComponents,
                                 silhouette: Silhouette[JwtEnv],
                                 userService: UserService,
                                 authInfoRepository: AuthInfoRepository,
                                 avatarService: AvatarService,
                                 passwordHasherRegistry: PasswordHasherRegistry)
                                (implicit ec: ExecutionContext)
  extends AbstractController(cc) with I18nSupport {

  /**
    * Handles the submitted JSON data.
    *
    * @return The result to display.
    */
  def submit: Action[JsValue] = Action.async(parse.json) { implicit request =>
    form.bindFromRequest.fold(
      formWithErrors =>
        Future.successful(BadRequest(Json.toJson(formWithErrors.errors)(FormErrorWrites.FormErrorSeqWrites))),
      data => {
        val loginInfo = LoginInfo(CredentialsProvider.ID, data.email)

        userService.retrieve(loginInfo).flatMap {
          case Some(user) =>
            Future.successful(BadRequest(Json.obj("message" -> Messages("user.exists"))))
          case None =>
            val authenticatorService = silhouette.env.authenticatorService
            val authInfo = passwordHasherRegistry.current.hash(data.password)
            val createdUser = createUser(loginInfo, data)

            for {
              avatar <- avatarService.retrieveURL(data.email)
              user <- userService.save(createdUser.copy(avatarURL = avatar))

              authInfo <- authInfoRepository.add(loginInfo, authInfo)

              authenticator <- authenticatorService.create(loginInfo)
              token <- authenticatorService.init(authenticator)
            } yield {
              Ok(Json.obj("token" -> token))
            }
        }
      }
    )
  }

  /**
    * This method will create an [[User]] with respect to the given details
    *
    * @param loginInfo The [[LoginInfo]] that the user has entered whilst signing up
    * @param data      The form data from the user
    * @return A new [[User]] object
    */
  private def createUser(loginInfo: LoginInfo, data: Data): User =
    User(
      id = UUID.randomUUID(),
      loginInfo = loginInfo,
      firstName = data.firstName,
      lastName = data.lastName,
      email = data.email,
      avatarURL = None,
      activated = false
    )
}
