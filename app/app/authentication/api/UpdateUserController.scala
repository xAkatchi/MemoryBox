package authentication.api

import authentication.user.User
import authentication.user.service.UserService
import authentication.utils.JwtEnv
import com.mohiva.play.silhouette.api.Silhouette
import javax.inject.Inject
import play.api.i18n.{I18nSupport, Messages}
import play.api.libs.json.{JsValue, Json}
import play.api.mvc.{AbstractController, Action, ControllerComponents}

import scala.concurrent.{ExecutionContext, Future}

/**
  * This controller handles the updating of an [[User]]
  *
  * @param cc          The controller components
  * @param silhouette  The Silhouette stack
  * @param userService The service to interact with users
  * @param ec          The used execution context
  */
class UpdateUserController @Inject()(cc: ControllerComponents,
                                     silhouette: Silhouette[JwtEnv],
                                     userService: UserService)
                                    (implicit ec: ExecutionContext)
  extends AbstractController(cc) with I18nSupport {

  /**
    * Tries to update the logged in [[User]] with regards to the given
    * form.
    *
    * @return The updated [[User]] on a successful update
    */
  def updateUser: Action[JsValue] = silhouette.SecuredAction.async(parse.json) { implicit request =>
    request.body.validate[UpdateUserForm.Data].map { data =>
      val userToUpdate = request.identity.copy(
        firstName = data.firstName,
        lastName = data.lastName
      )

      userService
        .save(userToUpdate)
        .map(updatedUser => Ok(Json.toJson(updatedUser)))
    }.recoverTotal { _ =>
      Future.successful(BadRequest(Json.obj("message" -> Messages("invalid.data"))))
    }
  }
}
