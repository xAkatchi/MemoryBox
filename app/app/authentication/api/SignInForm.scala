package authentication.api

import play.api.data.Form
import play.api.data.Forms._
import play.api.libs.json.{Json, OFormat}

/**
  * The form which handles the submission of the credentials.
  */
object SignInForm {

  /**
    * A play framework form.
    */
  val form = Form(
    mapping(
      "email" -> email,
      "password" -> nonEmptyText
    )(Data.apply)(Data.unapply)
  )

  /**
    * The form data.
    *
    * @param email    The email of the user.
    * @param password The password of the user.
    */
  case class Data(email: String, password: String)

  /**
    * The companion object.
    */
  object Data {
    /**
      * Converts the [[Data]] object to Json and vice versa.
      */
    implicit val jsonFormat: OFormat[Data] = Json.format[Data]
  }

}
