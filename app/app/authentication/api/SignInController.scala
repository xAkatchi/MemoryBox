package authentication.api

import javax.inject.Inject

import authentication.user.service.UserService
import authentication.utils.JwtEnv
import com.mohiva.play.silhouette.api.exceptions.ProviderException
import com.mohiva.play.silhouette.api.{LoginInfo, Silhouette}
import com.mohiva.play.silhouette.api.repositories.AuthInfoRepository
import com.mohiva.play.silhouette.api.util.{Clock, Credentials}
import com.mohiva.play.silhouette.impl.providers.CredentialsProvider
import play.api.Configuration
import play.api.i18n.{I18nSupport, Messages}
import play.api.libs.json.{JsValue, Json}
import play.api.mvc._
import utils.ClassLogger

import scala.concurrent.{ExecutionContext, Future}

/**
  * The `Sign In` controller.
  *
  * @param cc                  The controller components
  * @param silhouette          The Silhouette stack.
  * @param userService         The user service implementation.
  * @param authInfoRepository  The auth info repository implementation.
  * @param credentialsProvider The credentials provider.
  * @param configuration       The Play configuration.
  * @param clock               The clock instance.
  * @param ec                  The used execution context.
  */
class SignInController @Inject()(cc: ControllerComponents,
                                 silhouette: Silhouette[JwtEnv],
                                 userService: UserService,
                                 authInfoRepository: AuthInfoRepository,
                                 credentialsProvider: CredentialsProvider,
                                 configuration: Configuration,
                                 clock: Clock)
                                (implicit ec: ExecutionContext)
  extends AbstractController(cc) with I18nSupport with ClassLogger {

  /**
    * Handles the submitted JSON data.
    *
    * @return The result to display.
    */
  def submit: Action[JsValue] = Action.async(parse.json) { implicit request =>
    request.body.validate[SignInForm.Data].map { data =>
      credentialsProvider.authenticate(Credentials(data.email, data.password)).flatMap { loginInfo =>
        handleLoginInfo(loginInfo)
      }.recover {
        // The credential provider can throw an exception in several cases
        // (e.g. invalid password or identity not found)
        // We don't want to show that exception to the user, so we'll return an error message
        // whenever such an exception occurs
        case pe: ProviderException =>
          logger.error("Got an exception whilst trying to sign in.", pe)
          BadRequest(Json.obj("message" -> Messages("invalid.credentials")))
      }
    }.recoverTotal(error =>
      Future.successful(BadRequest(Json.obj("message" -> Messages("invalid.data")))))
  }

  /**
    * This method handles the given [[LoginInfo]] object and returns the corresponding
    * [[Result]] after handling the [[LoginInfo]].
    *
    * @param loginInfo The [[LoginInfo]] that the user has entered whilst signing in.
    * @return Either an [[Ok]] result with a token if everything went fine, or otherwise an
    *         [[BadRequest]] result. Either way, the [[Result]] is wrapped inside a [[Future]].
    */
  private def handleLoginInfo(loginInfo: LoginInfo)(implicit request: Request[_]): Future[Result] = {
    val authenticatorService = silhouette.env.authenticatorService

    userService.retrieve(loginInfo).flatMap {
      case Some(user) if user.activated => authenticatorService.create(loginInfo).flatMap { authenticator =>
        authenticatorService.init(authenticator).map(token => Ok(Json.obj("token" -> token)))
      }
      case Some(user) => Future.successful(BadRequest(Json.obj("message" -> Messages("activate.account"))))
      case None => Future.successful(BadRequest(Json.obj("message" -> Messages("invalid.credentials"))))
    }
  }
}
