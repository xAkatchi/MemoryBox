package authentication.api

import play.api.data.Form
import play.api.data.Forms._
import play.api.libs.json.{Json, OFormat}

/**
  * The form which contains the details that should be updated for the user.
  */
object UpdateUserForm {

  /**
    * A play framework form.
    */
  val form = Form(
    mapping(
      "firstName" -> nonEmptyText,
      "lastName" -> nonEmptyText
    )(Data.apply)(Data.unapply)
  )

  /**
    * The form data.
    *
    * @param firstName The first name of the user.
    * @param lastName  The last name of the user.
    */
  case class Data(firstName: String, lastName: String)

  /**
    * The companion object.
    */
  object Data {
    /**
      * Converts the [[Data]] object to Json and vice versa.
      */
    implicit val jsonFormat: OFormat[Data] = Json.format[Data]
  }

}
