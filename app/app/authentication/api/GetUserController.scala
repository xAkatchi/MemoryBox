package authentication.api

import authentication.user.service.UserService
import authentication.user.User
import authentication.utils.JwtEnv
import com.mohiva.play.silhouette.api.Silhouette
import javax.inject.Inject
import play.api.libs.json.Json
import play.api.mvc.{AbstractController, Action, AnyContent, ControllerComponents}

import scala.concurrent.{ExecutionContext, Future}

/**
  * This controller handles the retrieval of [[User user(s)]]
  *
  * @param cc          The controller components
  * @param silhouette  The Silhouette stack
  * @param userService The service to interact with users
  * @param ec          The used execution context
  */
class GetUserController @Inject()(cc: ControllerComponents,
                                  silhouette: Silhouette[JwtEnv],
                                  userService: UserService)
                                 (implicit ec: ExecutionContext)
  extends AbstractController(cc) {

  /**
    * Tries to obtain the currently logged in [[User]]
    *
    * @return Either the logged in user or NotFound
    */
  def getLoggedInUser: Action[AnyContent] = silhouette.SecuredAction.async { implicit request =>
    val loggedInUser: User = request.identity

    Future { Ok(Json.toJson(loggedInUser)) }
  }
}
