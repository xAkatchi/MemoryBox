package utils

import java.security.MessageDigest

/**
  * The goal of this class is to hash given input
  */
class Hasher {
  /**
    * @param stringToHash The string to be hashed
    * @return The hashed given input string
    */
  def hash(stringToHash: String): String = {
    val messageDigest: MessageDigest = MessageDigest.getInstance("SHA-256")

    messageDigest.update(stringToHash.getBytes)

    new String(messageDigest.digest)
  }
}
