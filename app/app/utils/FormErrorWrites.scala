package utils

import play.api.data.FormError
import play.api.i18n.Messages
import play.api.libs.json.{JsValue, Json, Writes}

/**
  * This util class provides some 'writes' for the [[FormError]] objects.
  */
object FormErrorWrites {
  /**
    * Writes a Seq[FormError] to [[JsValue]].
    *
    * @param messages Implicitly required for translations
    * @return The [[JsValue]] of the given Seq[]
    */
  implicit def FormErrorSeqWrites(implicit messages: Messages): Writes[Seq[FormError]] =
    (o: Seq[FormError]) => Json.toJson(o.map(Json.toJson(_)))

  /**
    * Writes a [[FormError]] to [[JsValue]]
    *
    * @param messages Implicitly required for translations
    * @return The [[JsValue]] of the given [[FormError]]
    */
  implicit def FormErrorWrites(implicit messages: Messages): Writes[FormError] = (o: FormError) => Json.obj(
    "key" -> Json.toJson(o.key),
    "message" -> Json.toJson(Messages(o.message))
  )
}
