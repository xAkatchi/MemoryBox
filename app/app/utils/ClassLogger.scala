package utils

import play.api.Logger

/**
  * Implement this to get a named logger in scope.
  */
trait ClassLogger {
  /**
    * A named logger instance.
    */
  val logger = Logger(this.getClass)
}
