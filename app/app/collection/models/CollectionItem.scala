package collection.models

import java.util.{Date, UUID}

import play.api.libs.json.{Json, Writes}
import authentication.user.User

/**
  * The CollectionItem object
  *
  * @param slug        The unique slug of the collection
  * @param collection  The collection to whom this item belongs
  * @param name        The name of the item
  * @param fileStorage The [[Storage]] object, detailing where/how the file is stored
  * @param fileDetails The [[FileDetails]] object, containing details about the file
  * @param createdAt   The time at which this item was created
  * @param uploadedBy  The [[User]] who has uploaded this item
  */
case class CollectionItem(
  slug: UUID,
  collection: Collection,
  name: String,
  fileStorage: Storage,
  fileDetails: FileDetails,
  createdAt: Date,
  uploadedBy: User
)

object CollectionItem {
  implicit val collectionItemWrites: Writes[CollectionItem] = (collectionItem: CollectionItem) => Json.obj(
    "slug" -> collectionItem.slug,
    "name" -> collectionItem.name,
    "fileUrl" -> s"/collection/${collectionItem.collection.slug}/${collectionItem.slug}/serve",
    "fileDetails" -> collectionItem.fileDetails,
    "createdAt" -> collectionItem.createdAt,
    "uploadedBy" -> collectionItem.uploadedBy
  )
}
