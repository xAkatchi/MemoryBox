package collection.models

import java.util.UUID

import play.api.libs.json.{Json, Writes}

/**
  * @param id                The unique id of the thumbnail
  * @param collectionItem    The [[CollectionItem]] to whom this thumbnail belongs
  * @param photoDetails      Contains the details about the thumbnails
  * @param fileSystemStorage Contains details about the location where the thumbnail is stored
  */
case class CollectionItemThumbnail(
  id: UUID,
  collectionItem: CollectionItem,
  photoDetails: PhotoDetails,
  fileSystemStorage: FileSystemStorage
)

object CollectionItemThumbnail {
  implicit val collectionItemThumbnailWrites: Writes[CollectionItemThumbnail] =
    (thumbnail: CollectionItemThumbnail) => Json.obj(
      "id" -> thumbnail.id,
      "fileDetails" -> thumbnail.photoDetails,
      "fileUrl" -> s"/collection/${thumbnail.collectionItem.collection.slug}/${thumbnail.collectionItem.slug}/${thumbnail.id}/serve"
    )
}
