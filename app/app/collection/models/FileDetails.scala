package collection.models

import play.api.libs.json.{JsValue, Json, Writes}

/**
  * The FileDetails object, which is used to supplement the [[CollectionItem]]
  * by defining details about the stored file.
  *
  * @param fileSize    The size of the file, in bytes
  * @param hash        The hash of the file
  * @param contentType The content-type of the file, indicating what kind of file it is
  */
abstract class FileDetails(fileSize: Long, hash: String, contentType: String)

/**
  * An extension on the [[FileDetails]] containing the details for photo files
  *
  * @param height      The height of the file, in pixels
  * @param width       The width of the file, in pixels
  * @param fileSize    The size of the file, in bytes
  * @param hash        The hash of the file
  * @param contentType The content-type of the file, indicating what kind of file it is
  */
case class PhotoDetails(height: Int, width: Int, fileSize: Long, hash: String, contentType: String) extends FileDetails(fileSize, hash, contentType)

object FileDetails {
  implicit val fileDetailsWrites: Writes[FileDetails] = {
    case photoDetails: PhotoDetails => Json.obj(
      "height" -> photoDetails.height,
      "width" -> photoDetails.width,
      "fileSize" -> photoDetails.fileSize,
      "contentType" -> photoDetails.contentType
    )
    case _ => throw new RuntimeException("Unsupported FileDetails type.")
  }
}
