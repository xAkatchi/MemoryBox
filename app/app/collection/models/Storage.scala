package collection.models

import play.api.libs.json.{Json, Writes}

/**
  * The Storage object, which is used to supplement the [[CollectionItem]]
  * by defining details about the storage location of the item.
  */
abstract class Storage

/**
  * An extension on the [[Storage]] containing the details for items
  * stored on the file system.
  *
  * @param filePath The absolute path to the file on the file system
  */
case class FileSystemStorage(filePath: String) extends Storage

object Storage {
  implicit val storageWrites: Writes[Storage] = {
    case fileSystemStorage: FileSystemStorage => Json.obj(
      "type" -> "file_system",
      "filePath" -> fileSystemStorage.filePath
    )
    case _ => throw new RuntimeException("Unsupported Storage type.")
  }
}
