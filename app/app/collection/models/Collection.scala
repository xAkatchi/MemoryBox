package collection.models

import java.util.Date

import play.api.libs.functional.syntax.unlift
import play.api.libs.json.{JsPath, Writes}
import play.api.libs.functional.syntax._

/**
  * The collection object.
  *
  * @param slug        The unique slug of the collection
  * @param name        The name of the collection
  * @param description The optional description of the collection
  * @param createdAt   The time when the collection was created
  */
case class Collection(
  slug: String,
  name: String,
  description: Option[String],
  createdAt: Date
)

object Collection {
  implicit val collectionWrites: Writes[Collection] = (
    (JsPath \ "slug").write[String] and
      (JsPath \ "name").write[String] and
      (JsPath \ "description").write[Option[String]] and
      (JsPath \ "createdAt").write[Date]
    )(unlift(Collection.unapply))
}
