package collection.data.dao

import java.util.{Date, UUID}

import authentication.authInfo.component.{LoginInfoComponent, UserLoginInfoComponent}
import javax.inject.{Inject, Singleton}
import authentication.user.User
import authentication.user.dao.UserDAO
import collection.data.component.UserCollectionComponent
import collection.models.Collection
import com.mohiva.play.silhouette.api.LoginInfo
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

/**
  * An implementation of the [[UserCollectionDAO]] using a Database (through Slick)
  * as storage layer.
  *
  * @param dbConfigProvider The slick database that this DAO is using internally.
  * @param userDAO          Used to verify that the given [[User]] objects do exist
  * @param collectionDAO    Used to verify that the given [[Collection]] objects do exist
  * @param executionContext A CPU bound execution context. Slick manages blocking JDBC calls with its
  *                         own internal thread pool, so Play's default execution context is fine here.
  */
@Singleton
class DatabaseUserCollectionDAO @Inject()(protected val dbConfigProvider: DatabaseConfigProvider,
                                          protected val userDAO: UserDAO,
                                          protected val collectionDAO: CollectionDAO)
                                         (implicit executionContext: ExecutionContext)
  extends UserCollectionDAO
    with HasDatabaseConfigProvider[JdbcProfile]
    with UserCollectionComponent
    with LoginInfoComponent
    with UserLoginInfoComponent {

  import profile.api._

  /**
    * Find all the [[Collection]]s that belong to the given user
    *
    * @param user The user for whom the collections need to be retrieved
    * @return Either a [[Seq]] containing the collections or an empty [[Seq]] in the case
    *         of an invalid user / no collections.
    */
  override def find(user: User): Future[Seq[Collection]] = {
    val query = for {
      (dbUserCollection, dbCollection) <- tblUserCollection join tblCollection on (_.collectionId === _.id)
      if dbUserCollection.userId === user.id.toString
    } yield dbCollection

    db.run(query.result).map(dbCollections => {
      dbCollections.map(dbCollection => Collection(
        dbCollection.slug,
        dbCollection.name,
        dbCollection.description,
        new Date(dbCollection.createdAt.getTime)
      ))
    })
  }

  /**
    * Adds a relationship between the given [[User]] and [[Collection]], given that
    * this relationship does not exist yet (in the case that it does exist, [[None]] will
    * be returned)
    *
    * @param user       The [[User]] who needs to be linked to the given [[Collection]]
    * @param collection The [[Collection]] that needs to be linked to the given [[User]]
    * @return Either the linked [[Collection]] or [[None]] if the linking failed
    */
  override def add(user: User, collection: Collection): Future[Option[Collection]] = {
    find(user).flatMap(_.find(_.slug == collection.slug) match {
      case Some(_) => Future.successful(None)
      case None =>
        val query = for {
          dbCollection <- tblCollection.filter(_.slug === collection.slug)
          dbUser <- tblUser.filter(_.id === user.id.toString)
        } yield (dbCollection, dbUser)

        db.run(query.result.headOption).flatMap {
          case Some((dbCollection, dbUser)) =>
            val wrappedUserCollection = DBUserCollection(dbUser.id.toString, dbCollection.id.get)

            db.run(tblUserCollection += wrappedUserCollection)
              .map(_ => Some(collection))
          case None => Future.successful(None)
        }
    })
  }

  /**
    * @param collection The collection whose members should be returned
    * @return The members of the given [[Collection]]
    */
  override def getMembers(collection: Collection): Future[Seq[User]] = {
    def toUser(tuple: (DBUser, DBLoginInfo)): User = {
      val (dbUser, dbLoginInfo) = tuple

      User(
        UUID.fromString(dbUser.id),
        LoginInfo(dbLoginInfo.providerId, dbLoginInfo.providerKey),
        dbUser.firstName,
        dbUser.lastName,
        dbUser.email,
        dbUser.avatarURL,
        dbUser.activated
      )
    }

    val query = for {
      dbCollection <- tblCollection if dbCollection.slug === collection.slug
      dbUserCollection <- tblUserCollection if dbUserCollection.collectionId === dbCollection.id
      dbUser <- tblUser if dbUser.id === dbUserCollection.userId
      dbUserLoginInfo <- tblUserLoginInfo if dbUserLoginInfo.userId === dbUser.id
      dbLoginInfo <- tblLoginInfo if dbLoginInfo.id === dbUserLoginInfo.loginInfoId
    } yield (dbUser, dbLoginInfo)

    db.run(query.result).map {
      case seq: Seq[(DBUser, DBLoginInfo)] =>
        seq.map(toUser)
      case _ =>
        Seq()
    }
  }

  /**
    * @param user The [[User]] who should be checked against
    * @param collection The [[Collection]] of which the given user must be a member
    * @return True in the case that the [[User]] belongs, otherwise false
    */
  override def belongsToCollection(user: User, collection: Collection): Future[Boolean] = {
    db.run(
      tblUserCollection.join(tblCollection).on(_.collectionId === _.id)
        .filter(e => e._1.userId === user.id.toString && e._2.slug === collection.slug)
        .exists.result
    )
  }
}
