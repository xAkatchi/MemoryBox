package collection.data.dao

import collection.data.component.{FileDetailsComponent, FilePhotoDetailsComponent}
import com.google.inject.{Inject, Singleton}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

/**
  * This class is focused on storing / retrieving FileDetails objects from the database
  *
  * @param dbConfigProvider The slick database that this DAO is using internally
  * @param executionContext A CPU bound execution context. Slick manages blocking JDBC calls with its
  *                         own internal thread pool, so Play's default execution context is fine here.
  */
@Singleton
class DatabaseFileDetailsDAO @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)
                                      (implicit executionContext: ExecutionContext)
  extends FilePhotoDetailsComponent
    with HasDatabaseConfigProvider[JdbcProfile] {

  import profile.api._

  /**
    * @param fileDetails The [[collection.models.FileDetails]] whose [[DBFileDetails]]
    *                    needs to be retrieved or created
    * @return Either the found / created [[DBFileDetails]] or None if an error occurred
    */
  def getOrCreateFileDetails(fileDetails: collection.models.FileDetails): Future[Option[DBFileDetails]] = {
    fileDetails match {
      case item: collection.models.PhotoDetails => getOrCreate(item)
      case _ => Future.successful(None)
    }
  }

  /**
    * @param fileDetails The [[collection.models.PhotoDetails]] whose matching
    *                    [[DBFileDetails]] should be retrieved or created
    * @return Either the found / created [[collection.models.PhotoDetails]] or None
    *         if an error occurred
    */
  private def getOrCreate(fileDetails: collection.models.PhotoDetails): Future[Option[DBFileDetails]] = {
    getOrCreateContentType(fileDetails.contentType).flatMap { dbContentType => {
      val insertFileDetailsQuery = tblFileDetails
        .returning(tblFileDetails.map(_.id))
        .into((item, id) => item.copy(id = Some(id))) +=
        DBFileDetails(None, dbContentType.id.get, fileDetails.fileSize, fileDetails.hash)

      db.run(insertFileDetailsQuery).flatMap(dbFileDetails => {
        val insertPhotoDetailsQuery = tblFilePhotoDetails +=
          DBFilePhotoDetails(dbFileDetails.id.get, fileDetails.height, fileDetails.width)

        db.run(insertPhotoDetailsQuery).map(_ => Some(dbFileDetails))
      })
    }}
  }

  /**
    * @param contentType The contentType whose matching [[DBContentType]] should be
    *                    retrieved or created
    * @return Either the found / created [[DBContentType]] or None if an error occurred
    */
  private def getOrCreateContentType(contentType: String): Future[DBContentType] = {
    val query =
      tblContentType.filter(_.contentType === contentType).result.headOption.flatMap {
        case Some(dbContentType) =>
          DBIO.successful(dbContentType)
        case None =>
          val contentTypeId = (tblContentType returning tblContentType.map(_.id)) +=
            DBContentType(None, contentType)

          contentTypeId.map { id => DBContentType(Some(id), contentType) }
      }.transactionally

    db.run(query)
  }
}
