package collection.data.dao

import collection.models.Collection

import scala.concurrent.Future

/**
  * Implementations of this class should give access to the [[Collection]] object
  * in the persistence layer
  */
trait CollectionDAO {
  /**
    * Find a [[Collection]] by its slug
    *
    * @param slug The slug of the [[Collection]] to find
    * @return The found [[Collection]] or None if no [[Collection]] could be found
    */
  def find(slug: String): Future[Option[Collection]]

  /**
    * Updates an existing [[Collection]] by updating it to match the given [[Collection]].
    * (if a [[Collection]] exists with the same slug as the given [[Collection]])
    *
    * @param collection The [[Collection]] which should be updated
    * @return The updated [[Collection]] (if any)
    */
  def update(collection: Collection): Future[Option[Collection]]

  /**
    * Adds the given [[Collection]]
    *
    * @param collection The [[Collection]] to update
    * @return The added [[Collection]] (if any)
    */
  def add(collection: Collection): Future[Option[Collection]]
}
