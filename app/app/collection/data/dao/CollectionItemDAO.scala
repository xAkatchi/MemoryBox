package collection.data.dao

import java.util.UUID

import collection.models.{Collection, CollectionItem}

import scala.concurrent.Future

/**
  * Implementations of this class should give access to the [[CollectionItem]] object
  * in the persistence layer
  */
trait CollectionItemDAO {
  /**
    * Find a [[CollectionItem]] by its slug
    *
    * @param slug The slug of the [[CollectionItem]] to find
    * @return The found [[CollectionItem]] or None if no [[CollectionItem]] could be found
    */
  def find(slug: UUID): Future[Option[CollectionItem]]

  /**
    * Find all the [[CollectionItem]]s that belong to the given [[Collection]]
    *
    * @param collection The [[Collection]] whose [[CollectionItem]]s should be retrieved
    * @return The [[CollectionItem]]s that belong to the given [[Collection]]
    */
  def find(collection: Collection): Future[Seq[CollectionItem]]

  /**
    * Saves the given [[CollectionItem]] (linked to the given [[Collection]], through the [[CollectionItem]])
    *
    * @param collectionItem The [[CollectionItem]] that should be saved
    * @return The [[CollectionItem]] if the saving went successful or None if the [[CollectionItem]] couldn't be added
    */
  def save(collectionItem: CollectionItem): Future[Option[CollectionItem]]
}
