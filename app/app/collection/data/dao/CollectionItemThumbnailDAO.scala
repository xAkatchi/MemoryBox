package collection.data.dao

import java.util.UUID

import collection.models.{CollectionItem, CollectionItemThumbnail}

import scala.concurrent.Future

/**
  * Implementations of this class should give access to the [[CollectionItemThumbnail]] object
  * in the persistence layer
  */
trait CollectionItemThumbnailDAO {
  /**
    * @param id The id of the [[CollectionItemThumbnail]] that should be fetched
    * @return Either the found [[CollectionItemThumbnail]] or [[None]]
    */
  def find(id: UUID): Future[Option[CollectionItemThumbnail]]

  /**
    * @param collectionItem The [[CollectionItem]] whose matching [[CollectionItemThumbnail]] should be fetched
    * @return The [[CollectionItemThumbnail]]s that belong to the given [[CollectionItem]]
    */
  def find(collectionItem: CollectionItem): Future[Seq[CollectionItemThumbnail]]

  /**
    * @param collectionItemThumbnail The [[CollectionItemThumbnail]] that needs to be saved
    * @return Either the created [[CollectionItemThumbnail]] on success, or [[None]] on failure
    */
  def save(collectionItemThumbnail: CollectionItemThumbnail): Future[Option[CollectionItemThumbnail]]
}
