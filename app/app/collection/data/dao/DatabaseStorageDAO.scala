package collection.data.dao

import java.sql.Timestamp

import collection.data.component.StorageFileSystemComponent
import collection.models._
import com.google.inject.{Inject, Singleton}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

/**
  * This class is focused on storing / retrieving Storage objects from the database
  *
  * @param dbConfigProvider The slick database that this DAO is using internally
  * @param executionContext A CPU bound execution context. Slick manages blocking JDBC calls with its
  *                         own internal thread pool, so Play's default execution context is fine here.
  */
@Singleton
class DatabaseStorageDAO @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)
                                  (implicit executionContext: ExecutionContext)
  extends StorageFileSystemComponent
    with HasDatabaseConfigProvider[JdbcProfile] {

  import profile.api._

  /**
    * Tries to get (or create if it doesn't exist) the matching DBStorage
    * for the given Storage.
    *
    * @param storage The storage whose matching [[DBStorage]] should be fetched
    * @return The matching [[DBStorage]]
    */
  def getOrCreateStorage(storage: Storage): Future[Option[DBStorage]] = {
    storage match {
      case item: FileSystemStorage => getOrCreate(item)
      case _ => Future.successful(None)
    }
  }

  /**
    * @param storage The [[FileSystemStorage]] whose matching [[DBStorage]] should be retrieved
    *                or created
    * @return Either the found / created [[DBStorage]] or None if an error occurred
    */
  private def getOrCreate(storage: FileSystemStorage): Future[Option[DBStorage]] = {
    getOrCreateStorageType(FileSystem).flatMap { dbStorageType => {
      val insertStorageQuery = tblStorage
        .returning(tblStorage.map(_.id))
        .into((item, id) => item.copy(id = Some(id))) +=
        DBStorage(None, dbStorageType.id.get, new Timestamp(System.currentTimeMillis()))

      db.run(insertStorageQuery).flatMap(dbStorage => {
        val insertFileSystemStorageQuery = tblStorageFileSystem +=
          DBStorageFileSystem(dbStorage.id.get, storage.filePath)

        db.run(insertFileSystemStorageQuery).map(_ => Some(dbStorage))
      })
    }}
  }

  /**
    * @param storageType The storageType whose matching [[DBStorageType]] should be retrieved
    *                    or created
    * @return Either the found / created [[DBStorageType]] or None if an error occurred
    */
  private def getOrCreateStorageType(storageType: String): Future[DBStorageType] = {
    val query =
      tblStorageType.filter(_.storageType === storageType).result.headOption.flatMap {
        case Some(dbStorageType) =>
          DBIO.successful(dbStorageType)
        case None =>
          val storageTypeId = (tblStorageType returning tblStorageType.map(_.id)) +=
            DBStorageType(None, storageType)

          storageTypeId.map { id => DBStorageType(Some(id), storageType) }
      }.transactionally

    db.run(query)
  }
}
