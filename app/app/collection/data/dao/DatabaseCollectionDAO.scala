package collection.data.dao

import java.sql.Timestamp
import java.util.Date
import javax.inject.{Inject, Singleton}

import collection.data.component.CollectionComponent
import collection.models.Collection
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

/**
  * An implementation of the [[CollectionDAO]] using a Database (through Slick)
  * as storage layer.
  *
  * @param dbConfigProvider The slick database that this DAO is using internally.
  * @param executionContext A CPU bound execution context. Slick manages blocking JDBC calls with its
  *                         own internal thread pool, so Play's default execution context is fine here.
  */
@Singleton
class DatabaseCollectionDAO @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)
                                     (implicit executionContext: ExecutionContext)
  extends CollectionDAO
    with HasDatabaseConfigProvider[JdbcProfile]
    with CollectionComponent {

  import profile.api._

  /**
    * Finds a [[Collection]] by its slug
    *
    * @param slug The slug of the [[Collection]] to find
    * @return The found [[Collection]] or None if no [[Collection]] could be found
    */
  override def find(slug: String): Future[Option[Collection]] = {
    val query = tblCollection.filter(_.slug === slug)

    db.run(query.result.headOption).map(maybeCollection => maybeCollection.map {
      case (dbCollection: DBCollection) => Collection(
        dbCollection.slug,
        dbCollection.name,
        dbCollection.description,
        new Date(dbCollection.createdAt.getTime)
      )
    })
  }

  /**
    * Updates the given [[Collection]] if a [[Collection]] exists with the
    * same slug as the given [[Collection]]
    *
    * @param collection The [[Collection]] which should be updated
    * @return The updated [[Collection]] (if any)
    */
  override def update(collection: Collection): Future[Option[Collection]] = {
    val query = tblCollection
      .filter(_.slug === collection.slug)
      .map(dbCollection => (dbCollection.name, dbCollection.description))
      .update((collection.name, collection.description))

    // See: https://stackoverflow.com/a/36846346
    db.run(query).map {
      case 0 => None
      case _ => Some(collection)
    }
  }

  /**
    * Adds the given [[Collection]]
    *
    * @param collection The [[Collection]] to update
    * @return The added [[Collection]] (if any)
    */
  override def add(collection: Collection): Future[Option[Collection]] = {
    find(collection.slug).flatMap {
      case Some(_) => Future.successful(None)
      case None =>
        val query = tblCollection += DBCollection(
          None,
          collection.slug,
          collection.name,
          collection.description,
          new Timestamp(collection.createdAt.getTime)
        )

        db.run(query).map(_ => Some(collection))
    }
  }
}
