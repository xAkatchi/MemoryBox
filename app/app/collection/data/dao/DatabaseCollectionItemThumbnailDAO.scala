package collection.data.dao

import java.util.UUID
import javax.inject.Singleton

import collection.data.component.{CollectionItemComponent, CollectionItemThumbnailComponent}
import collection.models._
import com.google.inject.Inject
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

/**
  * An implementation of the CollectionItemThumbnailDAO using a Database (through Slick)
  * as storage layer.
  *
  * @param dbConfigProvider  The slick database that this DAO is using internally.
  * @param collectionItemDAO Used to obtain details about [[CollectionItem]]s
  * @param dbFileDetailsDAO  The DAO which can be used to store [[FileDetails]]
  * @param dbStorageDAO      The DAO which can be used to store [[Storage]] objects
  * @param executionContext  A CPU bound execution context. Slick manages blocking JDBC calls with its
  *                          own internal thread pool, so Play's default execution context is fine here.
  */
@Singleton
class DatabaseCollectionItemThumbnailDAO @Inject()(protected val dbConfigProvider: DatabaseConfigProvider,
                                                   protected val collectionItemDAO: CollectionItemDAO,
                                                   protected val dbFileDetailsDAO: DatabaseFileDetailsDAO,
                                                   protected val dbStorageDAO: DatabaseStorageDAO)
                                                  (implicit executionContext: ExecutionContext)
  extends CollectionItemThumbnailDAO
    with HasDatabaseConfigProvider[JdbcProfile]
    with CollectionItemComponent
    with CollectionItemThumbnailComponent {

  import profile.api._

  /**
    * @param id The id of the [[CollectionItemThumbnail]] that should be fetched
    * @return Either the found [[CollectionItemThumbnail]] or [[None]]
    */
  override def find(id: UUID): Future[Option[CollectionItemThumbnail]] = {
    val query = for {
      thumbnail <- tblCollectionItemThumbnails.filter(_.id === id.toString)

      collectionItem <- tblCollectionItem.filter(_.id === thumbnail.collectionItemId)

      storage <- tblStorage.filter(_.id === thumbnail.storageFileSystemId)
      storageFileSystem <- tblStorageFileSystem.filter(_.id === storage.id)

      fileDetails <- tblFileDetails.filter(_.id === thumbnail.filePhotoDetailsId)
      photoDetails <- tblFilePhotoDetails.filter(_.id === fileDetails.id)
      contentType <- tblContentType.filter(_.id === fileDetails.contentTypeId)
    } yield (thumbnail, collectionItem, storage, storageFileSystem, fileDetails, photoDetails, contentType)

    db.run(query.result.headOption).flatMap {
      case Some((dbThumbnail, dbCollectionItem, dbStorage, dbFileSystemStorage, dbFileDetails, dbPhotoDetails, dbContentType)) =>
        createThumbnail(dbThumbnail, dbCollectionItem, dbStorage, dbFileSystemStorage, dbFileDetails, dbPhotoDetails, dbContentType)
      case None => Future.successful(None)
    }
  }

  /**
    * @param collectionItem The [[CollectionItem]] whose matching [[CollectionItemThumbnail]] should be fetched
    * @return The [[CollectionItemThumbnail]]s that belong to the given [[CollectionItem]]
    */
  override def find(collectionItem: CollectionItem): Future[Seq[CollectionItemThumbnail]] = {
    val query = for {
      collectionItem <- tblCollectionItem.filter(_.slug === collectionItem.slug.toString)

      thumbnail <- tblCollectionItemThumbnails.filter(_.collectionItemId === collectionItem.id)

      storage <- tblStorage.filter(_.id === thumbnail.storageFileSystemId)
      storageFileSystem <- tblStorageFileSystem.filter(_.id === storage.id)

      fileDetails <- tblFileDetails.filter(_.id === thumbnail.filePhotoDetailsId)
      photoDetails <- tblFilePhotoDetails.filter(_.id === fileDetails.id)
      contentType <- tblContentType.filter(_.id === fileDetails.contentTypeId)
    } yield (thumbnail, collectionItem, storage, storageFileSystem, fileDetails, photoDetails, contentType)

    // If failing futurs becomes an issue, perhaps this might solve it:
    // https://stackoverflow.com/questions/20874186/scala-listfuture-to-futurelist-disregarding-failed-futures
    db.run(query.result).map(results => results.map(result =>
      createThumbnail(
        result._1, result._2, result._3,
        result._4, result._5, result._6,
        result._7
      )
        .filter(_.isDefined)
        .map(_.get))
    )
      .map(Future.sequence(_))
      .flatten
  }

  /**
    * This method will create a [[CollectionItemThumbnail]] with regards to the given data
    *
    * @param dbThumbnail         The DB wrapper for the [[CollectionItemThumbnail]]
    * @param dbCollectionItem    The DB wrapper for the [[CollectionItem]]
    * @param dbStorage           The DB wrapper for the [[collection.models.Storage]]
    * @param dbStorageFileSystem The DB wrapper for the [[FileSystemStorage]]
    * @param dbFileDetails       The DB wrapper for the [[FileDetails]]
    * @param dbPhotoDetails      The DB wrapper for the [[FilePhotoDetails]]
    * @param dbContentType       The DB object indicating what kind of [[FileDetails]] we're dealing with
    * @return Either the build [[CollectionItemThumbnail]] or [[None]] if it failed to build such an item
    */
  private def createThumbnail(dbThumbnail: DBCollectionItemThumbnail, dbCollectionItem: DBCollectionItem,
                              dbStorage: DBStorage, dbStorageFileSystem: DBStorageFileSystem,
                              dbFileDetails: DBFileDetails, dbPhotoDetails: DBFilePhotoDetails,
                              dbContentType: DBContentType): Future[Option[CollectionItemThumbnail]] = {
    collectionItemDAO.find(UUID.fromString(dbCollectionItem.slug)).map {
      case Some(foundCollectionItem) =>
        val fileDetails = PhotoDetails(
          dbPhotoDetails.height, dbPhotoDetails.width, dbFileDetails.fileSize, dbFileDetails.hash, dbContentType.contentType
        )
        val storage = FileSystemStorage(dbStorageFileSystem.filePath)

        Some(CollectionItemThumbnail(
          UUID.fromString(dbThumbnail.id),
          foundCollectionItem,
          fileDetails,
          storage
        ))
      case _ => None
    }
  }

  /**
    * @param collectionItemThumbnail The [[CollectionItemThumbnail]] that needs to be saved
    * @return Either the created [[CollectionItemThumbnail]] on success, or [[None]] on failure
    */
  override def save(collectionItemThumbnail: CollectionItemThumbnail): Future[Option[CollectionItemThumbnail]] = {
    val query = tblCollectionItem.filter(_.slug === collectionItemThumbnail.collectionItem.slug.toString)

    db.run(query.result.headOption).flatMap {
      case Some(dbCollectionItem) =>
        val maybeDBStorage = dbStorageDAO.getOrCreateStorage(collectionItemThumbnail.fileSystemStorage)
        val maybeDBFileDetails = dbFileDetailsDAO.getOrCreateFileDetails(collectionItemThumbnail.photoDetails)

        maybeDBStorage.flatMap {
          case Some(dbStorage) => maybeDBFileDetails.flatMap {
            case Some(dbFileDetails) =>
              val dbCollectionItemThumbnail = DBCollectionItemThumbnail(
                collectionItemThumbnail.id.toString,
                dbCollectionItem.id.get,
                dbFileDetails.id.get,
                dbStorage.id.get
              )

              db.run(tblCollectionItemThumbnails += dbCollectionItemThumbnail)
                .map(_ => Some(collectionItemThumbnail))
            case None => Future.successful(None)
          }
          case None => Future.successful(None)
        }
      case None => Future.successful(None)
    }
  }
}
