package collection.data.dao

import java.sql.Timestamp
import java.util.{Date, UUID}

import authentication.authInfo.component.UserLoginInfoComponent
import authentication.user.dao.UserDAO
import authentication.user.User
import collection.data.component.{CollectionItemComponent, StorageFileSystemComponent, FilePhotoDetailsComponent}
import collection.exceptions.UnsupportedContentTypeException
import collection.models._
import com.google.inject.{Inject, Singleton}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

/**
  * An implementation of the CollectionItemDAO using a Database (through Slick)
  * as storage layer.
  *
  * @param dbConfigProvider The slick database that this DAO is using internally.
  * @param collectionDAO    Used for obtaining details about the given collection
  * @param userDAO          Used to obtain details from the user that created the [[CollectionItem]]
  * @param dbFileDetailsDAO The DAO which can be used to store [[FileDetails]]
  * @param dbStorageDAO     The DAO which can be used to store [[Storage]] objects
  * @param executionContext A CPU bound execution context. Slick manages blocking JDBC calls with its
  *                         own internal thread pool, so Play's default execution context is fine here.
  */
@Singleton
class DatabaseCollectionItemDAO @Inject()(protected val dbConfigProvider: DatabaseConfigProvider,
                                          protected val collectionDAO: CollectionDAO,
                                          protected val userDAO: UserDAO,
                                          protected val dbFileDetailsDAO: DatabaseFileDetailsDAO,
                                          protected val dbStorageDAO: DatabaseStorageDAO)
                                         (implicit executionContext: ExecutionContext)
  extends CollectionItemDAO
    with HasDatabaseConfigProvider[JdbcProfile]
    with CollectionItemComponent
    with UserLoginInfoComponent
    with StorageFileSystemComponent
    with FilePhotoDetailsComponent {

  import profile.api._

  /**
    * This method tries to find the [[CollectionItem]] with the given id
    *
    * @param slug The slug of the [[CollectionItem]] to find
    * @return The found [[CollectionItem]] or None if no [[CollectionItem]] could be found
    */
  override def find(slug: UUID): Future[Option[CollectionItem]] = {
    val query = for {
      collectionItem <- tblCollectionItem.filter(_.slug === slug.toString)
      collection <- tblCollection.filter(_.id === collectionItem.collectionId)

      user <- tblUser.filter(_.id === collectionItem.uploadedById)
      userLoginInfo <- tblUserLoginInfo.filter(_.userId === user.id)

      storage <- tblStorage.filter(_.id === collectionItem.storageId)
      storageFileSystem <- tblStorageFileSystem.filter(_.id === storage.id)
      storageType <- tblStorageType.filter(_.id === storage.storageTypeId)

      fileDetails <- tblFileDetails.filter(_.id === collectionItem.fileDetailsId)
      contentType <- tblContentType.filter(_.id === fileDetails.contentTypeId)
      photoDetails <- tblFilePhotoDetails.filter(_.id === fileDetails.id)
    } yield (collection, collectionItem, user, storage, storageFileSystem, storageType, fileDetails, contentType, photoDetails)

    db.run(query.result.headOption).flatMap {
      case Some((dbCollection, dbCollectionItem, dbUser, dbStorage, dbFileSystemStorage, dbStorageType, dbFileDetails, dbContentType, dbPhotoDetails)) =>
        createCollectionItem(
          dbCollection, dbCollectionItem, dbUser,
          dbStorage, dbFileSystemStorage, dbStorageType,
          dbFileDetails, dbContentType, dbPhotoDetails
        )
      case None => Future.successful(None)
    }
  }

  /**
    * This method tries to find all the [[CollectionItem]] objects that belong
    * to the given [[Collection]]
    *
    * @param collection The [[Collection]] whose [[CollectionItem]]s should be retrieved
    * @return The [[CollectionItem]]s that belong to the given [[Collection]]
    */
  override def find(collection: Collection): Future[Seq[CollectionItem]] = {
    val query = for {
      collection <- tblCollection.filter(_.slug === collection.slug)
      collectionItem <- tblCollectionItem.filter(_.collectionId === collection.id)

      user <- tblUser.filter(_.id === collectionItem.uploadedById)
      userLoginInfo <- tblUserLoginInfo.filter(_.userId === user.id)

      storage <- tblStorage.filter(_.id === collectionItem.storageId)
      storageFileSystem <- tblStorageFileSystem.filter(_.id === storage.id)
      storageType <- tblStorageType.filter(_.id === storage.storageTypeId)

      fileDetails <- tblFileDetails.filter(_.id === collectionItem.fileDetailsId)
      contentType <- tblContentType.filter(_.id === fileDetails.contentTypeId)
      photoDetails <- tblFilePhotoDetails.filter(_.id === fileDetails.id)
    } yield (collection, collectionItem, user, storage, storageFileSystem, storageType, fileDetails, contentType, photoDetails)

    // If failing futurs becomes an issue, perhaps this might solve it:
    // https://stackoverflow.com/questions/20874186/scala-listfuture-to-futurelist-disregarding-failed-futures
    db.run(query.result).map(results => results.map(result =>
      createCollectionItem(
        result._1, result._2, result._3,
        result._4, result._5, result._6,
        result._7, result._8, result._9
      )
        .filter(_.isDefined)
        .map(_.get))
    )
      .map(Future.sequence(_))
      .flatten
  }

  /**
    * This method will create a [[CollectionItem]] with regards to the given database objects
    *
    * @param dbCollection        The DB wrapper for the [[Collection]]
    * @param dbCollectionItem    The DB wrapper for the [[CollectionItem]]
    * @param dbUser              The DB wrapper for the [[User]]
    * @param dbStorage           The DB wrapper for the [[Storage]]
    * @param dbStorageFileSystem The DB wrapper for the [[FileSystemStorage]]
    * @param dbStorageType       The DB object indicating what kind of [[Storage]] we're dealing with
    * @param dbFileDetails       The DB wrapper for the [[FileDetails]]
    * @param dbContentType       The DB object indicating what kind of [[FileDetails]] we're dealing with
    * @param dbPhotoDetails      The DB wrapper for the [[FilePhotoDetails]]
    * @return Either the build [[CollectionItem]] or [[None]] if it failed to build such an item
    * @throws UnsupportedContentTypeException in the case of an unsupported [[DBContentType]]
    */
  private def createCollectionItem(dbCollection: DBCollection, dbCollectionItem: DBCollectionItem, dbUser: DBUser,
                                   dbStorage: DBStorage, dbStorageFileSystem: DBStorageFileSystem, dbStorageType: DBStorageType,
                                   dbFileDetails: DBFileDetails, dbContentType: DBContentType, dbPhotoDetails: DBFilePhotoDetails
                                  ): Future[Option[CollectionItem]] = {
    val foundResults = for {
      foundUser <- userDAO.find(UUID.fromString(dbUser.id))
      foundCollection <- collectionDAO.find(dbCollection.slug)
    } yield (foundUser, foundCollection)

    foundResults.map {
      case (Some(foundUser), Some(foundCollection)) =>
        val storage = dbStorageType.storageType match {
          case FileSystem => FileSystemStorage(dbStorageFileSystem.filePath)
        }
        val fileDetails = dbContentType.contentType match {
          case contentType if contentType.startsWith("image/") =>
            PhotoDetails(
              dbPhotoDetails.height, dbPhotoDetails.width, dbFileDetails.fileSize, dbFileDetails.hash, contentType
            )
          case _ => throw new UnsupportedContentTypeException(s"Unsupported content type: ${dbContentType.contentType}")
        }

        Some(CollectionItem(
          UUID.fromString(dbCollectionItem.slug),
          foundCollection,
          dbCollectionItem.name,
          storage,
          fileDetails,
          new Date(dbCollectionItem.createdAt.getTime),
          foundUser
        ))
      case _ => None
    }
  }

  /**
    * This method saves the given [[CollectionItem]] and links it to the given
    * [[Collection]]
    *
    * @param collectionItem The [[CollectionItem]] that should be saved
    * @return The [[CollectionItem]] if the saving went successful or None if the [[CollectionItem]] couldn't be added
    */
  override def save(collectionItem: CollectionItem): Future[Option[CollectionItem]] = {
    val findCollectionQuery = tblCollection.filter(_.slug === collectionItem.collection.slug)

    userDAO.find(collectionItem.uploadedBy.id).flatMap {
      case Some(dbUser) =>
        db.run(findCollectionQuery.result.headOption).flatMap {
          case Some(dbCollection) =>
            val maybeDBStorage = dbStorageDAO.getOrCreateStorage(collectionItem.fileStorage)
            val maybeDBFileDetails = dbFileDetailsDAO.getOrCreateFileDetails(collectionItem.fileDetails)

            maybeDBStorage.flatMap {
              case Some(dbStorage) => maybeDBFileDetails.flatMap {
                case Some(dbFileDetails) =>
                  val dbCollectionItem = DBCollectionItem(
                    None,
                    collectionItem.slug.toString,
                    dbCollection.id.get,
                    collectionItem.uploadedBy.id.toString,
                    dbStorage.id.get,
                    dbFileDetails.id.get,
                    collectionItem.name,
                    new Timestamp(collectionItem.createdAt.getTime)
                  )

                  db.run(tblCollectionItem += dbCollectionItem)
                    .map(_ => Some(collectionItem))
                case None => Future.successful(None)
              }
              case None => Future.successful(None)
            }
          case None => Future.successful(None)
        }
      case None => Future.successful(None)
    }
  }
}
