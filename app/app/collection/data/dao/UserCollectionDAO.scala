package collection.data.dao

import authentication.user.User
import collection.models.Collection

import scala.concurrent.Future

/**
  * Implementations of this class should give access to the user-collection relationship
  * in the persistence layer
  */
trait UserCollectionDAO {
  /**
    * Find all the [[Collection]]s that belong to the given user
    *
    * @param user The user for whom the collections need to be retrieved
    * @return Either a [[Seq]] containing the collections or an empty [[Seq]] in the case
    *         of an invalid user / no collections.
    */
  def find(user: User): Future[Seq[Collection]]

  /**
    * Adds a relationship between the given [[User]] and [[Collection]], given that
    * this relationship does not exist yet (in the case that it does exist, [[None]] should
    * be returned)
    *
    * @param user       The [[User]] who needs to be linked to the given [[Collection]]
    * @param collection The [[Collection]] that needs to be linked to the given [[User]]
    * @return Either the linked [[Collection]] or [[None]] if the linking failed
    */
  def add(user: User, collection: Collection): Future[Option[Collection]]

  /**
    * Implementations of this method should return all the members of the given
    * [[Collection]]
    *
    * @param collection The collection whose members should be returned
    * @return The members of the given [[Collection]]
    */
  def getMembers(collection: Collection): Future[Seq[User]]

  /**
    * Implementations of this method should return whether or not the given
    * [[User]] belongs to the given [[Collection]]
    *
    * @param user The [[User]] who should be checked against
    * @param collection The [[Collection]] of which the given user must be a member
    * @return True in the case that the [[User]] belongs, otherwise false
    */
  def belongsToCollection(user: User, collection: Collection): Future[Boolean]
}
