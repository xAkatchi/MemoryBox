package collection.data.component

import play.api.db.slick.HasDatabaseConfigProvider
import slick.jdbc.JdbcProfile

/**
  * A trait containing a reference to the `file_photo_details` table.
  *
  * Include this trait whenever you have a desire to do something with the file_photo_details table.
  */
trait FilePhotoDetailsComponent extends FileDetailsComponent {
  self: HasDatabaseConfigProvider[JdbcProfile] =>

  import profile.api._

  protected val tblFilePhotoDetails = TableQuery[FilePhotoDetails]

  /**
    * The case class that represents the entries inside the table.
    *
    * @param id     The primary key
    * @param height The height of the photo (in pixels)
    * @param width  The width of the photo (in pixels)
    */
  case class DBFilePhotoDetails(id: Int, height: Int, width: Int)

  /**
    * This class contains a mapping from the database table to an object.
    * ([[http://slick.lightbend.com/doc/3.0.3/gettingstarted.html#schema as required by Slick]])
    *
    * @param tag
    */
  class FilePhotoDetails(tag: Tag) extends Table[DBFilePhotoDetails](tag, "file_photo_details") {
    def id = column[Int]("id", O.PrimaryKey)
    def height = column[Int]("height")
    def width = column[Int]("width")

    def fileDetailsFK = foreignKey("fk_file_photo_details_id", id, tblFileDetails)(_.id)

    def * = (id, height, width) <> (DBFilePhotoDetails.tupled, DBFilePhotoDetails.unapply)
  }

}
