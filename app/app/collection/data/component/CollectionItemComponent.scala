package collection.data.component

import java.sql.Timestamp

import authentication.user.component.UserComponent
import play.api.db.slick.HasDatabaseConfigProvider
import slick.jdbc.JdbcProfile

/**
  * A trait containing a reference to the `collection_item` table.
  *
  * Include this trait whenever you have a desire to do something with the collection_item table.
  */
trait CollectionItemComponent extends CollectionComponent
  with UserComponent
  with StorageComponent
  with FileDetailsComponent {
  self: HasDatabaseConfigProvider[JdbcProfile] =>

  import profile.api._

  protected val tblCollectionItem = TableQuery[CollectionItems]

  /**
    * The case class that represents the entries inside the table.
    *
    * @param id            The primary key
    * @param slug          The 'slug' of the collection item (can be used in for example the routing)
    * @param collectionId  A reference to the [[tblCollection]] table
    * @param uploadedById  A reference to the [[tblUser]] table
    * @param storageId     A reference to the [[tblStorage]] table
    * @param fileDetailsId A reference to the [[tblFileDetails]] table
    * @param name          The name of this item
    * @param createdAt     The timestamp whenever this item was first created
    */
  case class DBCollectionItem(id: Option[Int], slug: String, collectionId: Int, uploadedById: String, storageId: Int,
                              fileDetailsId: Int, name: String, createdAt: Timestamp)

  /**
    * This class contains a mapping from the database table to an object.
    * ([[http://slick.lightbend.com/doc/3.0.3/gettingstarted.html#schema as required by Slick]])
    *
    * @param tag
    */
  class CollectionItems(tag: Tag) extends Table[DBCollectionItem](tag, "collection_item") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def slug = column[String]("slug", O.Unique)
    def collectionId = column[Int]("collection_id")
    def uploadedById = column[String]("uploaded_by_id")
    def storageId = column[Int]("storage_id")
    def fileDetailsId = column[Int]("file_details_id")
    def name = column[String]("name")
    def createdAt = column[Timestamp]("created_at")

    def collectionFK = foreignKey("fk_collection_item_collection_id", collectionId, tblCollection)(_.id)
    def uploadedByFK = foreignKey("fk_collection_item_uploaded_by_id", uploadedById, tblUser)(_.id)
    def storageFK = foreignKey("fk_collection_item_storage_id", storageId, tblStorage)(_.id)
    def fileDetailsFK = foreignKey("fk_collection_item_file_details_id", fileDetailsId, tblFileDetails)(_.id)

    def * = (id.?, slug, collectionId, uploadedById, storageId, fileDetailsId, name, createdAt) <>
      (DBCollectionItem.tupled, DBCollectionItem.unapply)
  }

}
