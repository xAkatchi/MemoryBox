package collection.data.component

import java.sql.Timestamp

import play.api.db.slick.HasDatabaseConfigProvider
import slick.jdbc.JdbcProfile

/**
  * A trait containing a reference to the `storage` table.
  *
  * Include this trait whenever you have a desire to do something with the storage table.
  */
trait StorageComponent extends StorageTypeComponent {
  self: HasDatabaseConfigProvider[JdbcProfile] =>

  import profile.api._

  protected val tblStorage = TableQuery[Storages]

  /**
    * The case class that represents the entries inside the table.
    *
    * @param id            The primary key
    * @param storageTypeId A reference to the [[tblStorageType]] table
    * @param createdAt     The timestamp whenever this item was first created
    */
  case class DBStorage(id: Option[Int], storageTypeId: Int, createdAt: Timestamp)

  /**
    * This class contains a mapping from the database table to an object.
    * ([[http://slick.lightbend.com/doc/3.0.3/gettingstarted.html#schema as required by Slick]])
    *
    * @param tag
    */
  class Storages(tag: Tag) extends Table[DBStorage](tag, "storage") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def storageTypeId = column[Int]("storage_type_id")
    def createdAt = column[Timestamp]("created_at")

    def storageTypeFK = foreignKey("fk_storage_storage_type_id", storageTypeId, tblStorageType)(_.id)

    def * = (id.?, storageTypeId, createdAt) <> (DBStorage.tupled, DBStorage.unapply)
  }

}
