package collection.data.component

import play.api.db.slick.HasDatabaseConfigProvider
import slick.jdbc.JdbcProfile

/**
  * A trait containing a reference to the `collection_item_thumbnail` table.
  *
  * Include this trait whenever you have a desire to do something with the collection_item_thumbnail table.
  */
trait CollectionItemThumbnailComponent extends CollectionItemComponent
  with FilePhotoDetailsComponent
  with StorageFileSystemComponent {
  self: HasDatabaseConfigProvider[JdbcProfile] =>

  import profile.api._

  protected val tblCollectionItemThumbnails = TableQuery[CollectionItemThumbnails]

  /**
    * The case class that represents the entries inside the table.
    *
    * @param id                  The primary key
    * @param collectionItemId    A reference to the [[tblCollectionItem]] table
    * @param filePhotoDetailsId  A reference to the [[tblFilePhotoDetails]] table
    * @param storageFileSystemId A reference to the [[tblStorageFileSystem]] table
    */
  case class DBCollectionItemThumbnail(id: String, collectionItemId: Int, filePhotoDetailsId: Int,
                                       storageFileSystemId: Int)

  /**
    * This class contains a mapping from the database table to an object.
    * ([[http://slick.lightbend.com/doc/3.0.3/gettingstarted.html#schema as required by Slick]])
    *
    * @param tag
    */
  class CollectionItemThumbnails(tag: Tag) extends Table[DBCollectionItemThumbnail](tag, "collection_item_thumbnail") {
    def id = column[String]("id", O.PrimaryKey)
    def collectionItemId = column[Int]("collection_item_id")
    def filePhotoDetailsId = column[Int]("file_photo_details_id")
    def storageFileSystemId = column[Int]("storage_file_system_id")

    def collectionItemFK = foreignKey("fk_collection_item_thumbnail_collection_item_id",
      collectionItemId, tblCollectionItem)(_.id)
    def filePhotoDetailsFK = foreignKey("fk_collection_item_thumbnail_file_photo_details_id",
      filePhotoDetailsId, tblFilePhotoDetails)(_.id)
    def storageFileSystemFK = foreignKey("fk_collection_item_thumbnail_storage_file_system_id",
      storageFileSystemId, tblStorageFileSystem)(_.id)

    def * = (id, collectionItemId, filePhotoDetailsId, storageFileSystemId) <>
      (DBCollectionItemThumbnail.tupled, DBCollectionItemThumbnail.unapply)
  }
}
