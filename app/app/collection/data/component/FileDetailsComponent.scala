package collection.data.component

import play.api.db.slick.HasDatabaseConfigProvider
import slick.jdbc.JdbcProfile

/**
  * A trait containing a reference to the `file_details` table.
  *
  * Include this trait whenever you have a desire to do something with the file_details table.
  */
trait FileDetailsComponent extends FileContentTypeComponent {
  self: HasDatabaseConfigProvider[JdbcProfile] =>

  import profile.api._

  protected val tblFileDetails = TableQuery[FileDetails]

  /**
    * The case class that represents the entries inside the table.
    *
    * @param id            The primary key
    * @param contentTypeId A reference to the [[tblContentType]] table
    * @param fileSize      The size of the file in bytes
    * @param hash          The hash of the file (must be unique, thus duplicates should refer to the same
    *                      entry in the database)
    */
  case class DBFileDetails(id: Option[Int], contentTypeId: Int, fileSize: Long, hash: String)

  /**
    * This class contains a mapping from the database table to an object.
    * ([[http://slick.lightbend.com/doc/3.0.3/gettingstarted.html#schema as required by Slick]])
    *
    * @param tag
    */
  class FileDetails(tag: Tag) extends Table[DBFileDetails](tag, "file_details") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def contentTypeId = column[Int]("content_type_id")
    def fileSize = column[Long]("file_size")
    def hash = column[String]("hash", O.Unique)

    def contentTypeFK = foreignKey("fk_file_details_content_type_id", contentTypeId, tblContentType)(_.id)

    def * = (id.?, contentTypeId, fileSize, hash) <> (DBFileDetails.tupled, DBFileDetails.unapply)
  }

}
