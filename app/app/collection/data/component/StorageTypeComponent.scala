package collection.data.component

import play.api.db.slick.HasDatabaseConfigProvider
import slick.jdbc.JdbcProfile

/**
  * A trait containing a reference to the `storage_type` table.
  *
  * Include this trait whenever you have a desire to do something with the storage_type table.
  */
trait StorageTypeComponent {
  self: HasDatabaseConfigProvider[JdbcProfile] =>

  import profile.api._

  protected val tblStorageType = TableQuery[StorageTypes]

  val FileSystem = "FILE_SYSTEM"

  /**
    * The case class that represents the entries inside the table.
    *
    * @param id          The primary key
    * @param storageType A string defining what kind of storage we're dealing with (e.g. FILE_SYSTEM)
    */
  case class DBStorageType(id: Option[Int], storageType: String)

  /**
    * This class contains a mapping from the database table to an object.
    * ([[http://slick.lightbend.com/doc/3.0.3/gettingstarted.html#schema as required by Slick]])
    *
    * @param tag
    */
  class StorageTypes(tag: Tag) extends Table[DBStorageType](tag, "storage_type") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def storageType = column[String]("storage_type", O.Unique)

    def * = (id.?, storageType) <> (DBStorageType.tupled, DBStorageType.unapply)
  }

}
