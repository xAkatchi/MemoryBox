package collection.data.component

import play.api.db.slick.HasDatabaseConfigProvider
import slick.jdbc.JdbcProfile

/**
  * A trait containing a reference to the `file_content_type` table.
  *
  * Include this trait whenever you have a desire to do something with the file_content_type table.
  */
trait FileContentTypeComponent {
  self: HasDatabaseConfigProvider[JdbcProfile] =>

  import profile.api._

  protected val tblContentType = TableQuery[ContentTypes]

  /**
    * The case class that represents the entries inside the table.
    *
    * @param id          The primary key
    * @param contentType The content type (e.g. 'text/plain'), as defined over
    *                    [[https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types here]]
    */
  case class DBContentType(id: Option[Int], contentType: String)

  /**
    * This class contains a mapping from the database table to an object.
    * ([[http://slick.lightbend.com/doc/3.0.3/gettingstarted.html#schema as required by Slick]])
    *
    * @param tag
    */
  class ContentTypes(tag: Tag) extends Table[DBContentType](tag, "file_content_type") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def contentType = column[String]("content_type", O.Unique)

    def * = (id.?, contentType) <> (DBContentType.tupled, DBContentType.unapply)
  }

}
