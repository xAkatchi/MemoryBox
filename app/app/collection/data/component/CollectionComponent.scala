package collection.data.component

import java.sql.Timestamp

import play.api.db.slick.HasDatabaseConfigProvider
import slick.jdbc.JdbcProfile

/**
  * A trait containing a reference to the `collection` table.
  *
  * Include this trait whenever you have a desire to do something with the collection table.
  */
trait CollectionComponent {
  self: HasDatabaseConfigProvider[JdbcProfile] =>

  import profile.api._

  protected val tblCollection = TableQuery[Collections]

  /**
    * The case class that represents the entries inside the table.
    *
    * @param id          The primary key
    * @param slug        The 'slug' of the collection (can be used in for example the routing)
    * @param name        The name of the collection
    * @param description The optional description of the collection
    * @param createdAt   The timestamp whenever this collection was first created
    */
  case class DBCollection(id: Option[Int], slug: String, name: String,
                          description: Option[String], createdAt: Timestamp)

  /**
    * This class contains a mapping from the database table to an object.
    * ([[http://slick.lightbend.com/doc/3.0.3/gettingstarted.html#schema as required by Slick]])
    *
    * @param tag
    */
  class Collections(tag: Tag) extends Table[DBCollection](tag, "collection") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def slug = column[String]("slug", O.Unique)
    def name = column[String]("name")
    def description = column[Option[String]]("description")
    def createdAt = column[Timestamp]("created_at")

    def * = (id.?, slug, name, description, createdAt) <> (DBCollection.tupled, DBCollection.unapply)
  }

}
