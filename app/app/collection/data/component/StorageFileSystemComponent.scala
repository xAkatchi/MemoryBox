package collection.data.component

import play.api.db.slick.HasDatabaseConfigProvider
import slick.jdbc.JdbcProfile

/**
  * A trait containing a reference to the `storage_file_system` table.
  *
  * Include this trait whenever you have a desire to do something with the storage_file_system table.
  */
trait StorageFileSystemComponent extends StorageComponent {
  self: HasDatabaseConfigProvider[JdbcProfile] =>

  import profile.api._

  protected val tblStorageFileSystem = TableQuery[StorageFileSystems]

  /**
    * The case class that represents the entries inside the table.
    *
    * @param id       The primary key
    * @param filePath The (absolute) path where the file is stored on the file system
    */
  case class DBStorageFileSystem(id: Int, filePath: String)

  /**
    * This class contains a mapping from the database table to an object.
    * ([[http://slick.lightbend.com/doc/3.0.3/gettingstarted.html#schema as required by Slick]])
    *
    * @param tag
    */
  class StorageFileSystems(tag: Tag) extends Table[DBStorageFileSystem](tag, "storage_file_system") {
    def id = column[Int]("id", O.PrimaryKey)
    def filePath = column[String]("file_path", O.Unique)

    def storageFK = foreignKey("fk_storage_file_system_id", id, tblStorage)(_.id)

    def * = (id, filePath) <> (DBStorageFileSystem.tupled, DBStorageFileSystem.unapply)
  }

}
