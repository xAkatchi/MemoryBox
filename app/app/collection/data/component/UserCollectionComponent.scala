package collection.data.component

import authentication.user.component.UserComponent
import play.api.db.slick.HasDatabaseConfigProvider
import slick.jdbc.JdbcProfile

/**
  * A trait containing a reference to the `user_collection` table.
  *
  * Include this trait whenever you have a desire to do something with the user_collection table.
  */
trait UserCollectionComponent extends UserComponent with CollectionComponent {
  self: HasDatabaseConfigProvider[JdbcProfile] =>

  import profile.api._

  protected val tblUserCollection = TableQuery[UserCollections]

  /**
    * The case class that represents the entries inside the table.
    *
    * @param userId       A reference to the [[tblUser]] table
    * @param collectionId A reference to the [[tblCollection]] table
    */
  case class DBUserCollection(userId: String, collectionId: Int)

  /**
    * This class contains a mapping from the database table to an object.
    * ([[http://slick.lightbend.com/doc/3.0.3/gettingstarted.html#schema as required by Slick]])
    *
    * @param tag
    */
  class UserCollections(tag: Tag) extends Table[DBUserCollection](tag, "user_collection") {
    def userId = column[String]("user_id")
    def collectionId = column[Int]("collection_id")

    def pk = primaryKey("pk_user_collection", (userId, collectionId))

    def userFK = foreignKey("fk_user_collection_user_id", userId, tblUser)(_.id)
    def collectionFK = foreignKey("fk_user_collection_collection", collectionId, tblCollection)(_.id)

    def * = (userId, collectionId) <> (DBUserCollection.tupled, DBUserCollection.unapply)
  }

}
