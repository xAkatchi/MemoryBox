package collection.exceptions

/**
  * This exception can be thrown whenever an unsupported content type is encountered
  *
  * @param message The optional message of the exception
  * @param cause   The optional cause of the exception
  */
final class UnsupportedContentTypeException(private val message: String = "",
                                            private val cause: Throwable = None.orNull)
  extends Exception(message, cause)
