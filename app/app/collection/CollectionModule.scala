package collection

import collection.data.dao._
import collection.service._
import collection.utils._
import com.google.inject.{AbstractModule, Provides}
import com.sksamuel.scrimage.nio.{ImageWriter, JpegWriter}
import net.codingwell.scalaguice.ScalaModule
import play.api.Configuration
import play.api.db.slick.DatabaseConfigProvider

/**
  * The Guice module which wires all collection dependencies.
  *
  * [[https://www.playframework.com/documentation/2.6.x/ScalaDependencyInjection#programmatic-bindings Documentation]]
  */
class CollectionModule extends AbstractModule with ScalaModule {
  /**
    * We override our parent to configure
    * the desired bindings used by the collections
    */
  override def configure(): Unit = {
    bind[CollectionDAO].to[DatabaseCollectionDAO]
    bind[UserCollectionDAO].to[DatabaseUserCollectionDAO]
    bind[CollectionItemDAO].to[DatabaseCollectionItemDAO]
    bind[CollectionService].to[CollectionServiceImpl]
    bind[CollectionItemService].to[CollectionItemServiceImpl]
    bind[CollectionItemThumbnailService].to[CollectionItemThumbnailServiceImpl]
    bind[ImagePropertyReader].toInstance(new ImagePropertyReader)
    bind[FileHasher].toInstance(new FileHasher)
    bind[FilePartMover].toInstance(new FilePartMover)
    bind[CollectionItemThumbnailDAO].to[DatabaseCollectionItemThumbnailDAO]
  }

  /**
    * Provides the SlugGenerator.
    *
    * @return The SlugGenerator implementation.
    */
  @Provides
  def provideSlugGenerator(): SlugGenerator = new SlugGenerator

  /**
    * Provides the URLBuilder
    *
    * @param config The [[Configuration]] reader from Play
    * @return The [[URLBuilder]] instance
    */
  @Provides
  def provideURLBuilder(config: Configuration): URLBuilder =
    new URLBuilder(config)

  /**
    * @return The [[ImageWriter]] implementation
    */
  @Provides
  def provideImageWriter(): ImageWriter =
    JpegWriter().withCompression(50).withProgressive(true)

  /**
    * Provides the CollectionItemFilePathFactory.
    *
    * @param config The [[Configuration]] reader from Play
    * @return The CollectionItemFilePathFactory implementation.
    */
  @Provides
  def provideCollectionItemFilePathFactory(config: Configuration): CollectionItemFilePathFactory =
    new CollectionItemFilePathFactory(config)
}
