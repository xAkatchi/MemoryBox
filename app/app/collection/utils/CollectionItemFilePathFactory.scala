package collection.utils

import java.io.File
import java.nio.file.Paths

import javax.inject.Inject
import authentication.user.User
import org.apache.commons.io.FilenameUtils
import org.joda.time.DateTime
import play.api.Configuration
import play.api.libs.Files
import play.api.mvc.MultipartFormData

import scala.util.Random

/**
  * The goal of this factory is to be able to create a file path which can be used
  * when uploading new collection items.
  *
  * @param config The configuration file interpreter
  */
class CollectionItemFilePathFactory @Inject()(config: Configuration) {

  /**
    * Creates and returns a file path with respect to the given details.
    *
    * @param file The file that is being uploaded
    * @param user The [[User]] who is uploading the new collection item
    * @return The path where the collection item should be stored
    */
  def getUploadPath(file: MultipartFormData.FilePart[Files.TemporaryFile], user: User): String = {
    createPath(
      config.get[String]("memorybox.collection.item.upload.path.prefix"),
      Paths.get(file.filename).toString,
      user
    )
  }

  /**
    * Creates and returns a file path (for storing thumbnails) with respect to the given details.
    *
    * @param file The file that is being uploaded
    * @param user The [[User]] who is uploading the new thumbnail
    * @return The path where the thumbnail should be stored
    */
  def getThumbnailUploadPath(file: File, user: User): String =
    createPath(
      config.get[String]("memorybox.collection.item.thumbnail.upload.path.prefix"),
      file.getName,
      user
    )

  /**
    * A convenience function to quickly create a file path with respect to the given parameters
    *
    * @param prefix   The prefix of the file path
    * @param fileName The name of the file that's being uploaded
    * @param user     The user who is uploading the file
    * @return The file path where the file can be stored
    */
  private def createPath(prefix: String, fileName: String, user: User): String = {
    val dateTime = new DateTime()
    val filePath = Paths.get(fileName)
    val extension = FilenameUtils.getExtension(filePath.toString)

    val dir = s"$prefix" +
      s"${user.id.toString}/" +
      s"${dateTime.year.get}/" +
      s"${dateTime.monthOfYear.get}/" +
      s"${dateTime.dayOfMonth.get}/"

    dir + generateUniqueName(dir, extension)
  }

  /**
    * This method will generate a filename that's unique for the given directory.
    *
    * @param directory The directory for which the filename must be unique
    * @param extension The extension of the file which is getting an unique filename
    * @return A directory-unique file name
    */
  def generateUniqueName(directory: String, extension: String): String = {
    def attemptCreatingUniqueFileName(fileName: String): String = {
      val file = new File(directory + fileName)

      if (file.exists) {
        // If the file exists, we prepend a small piece of data, and try again
        attemptCreatingUniqueFileName(Random.alphanumeric.take(1).mkString + fileName)
      } else {
        fileName
      }
    }

    val randomPrefix = Random.alphanumeric.take(5).mkString
    val fileName = s"${randomPrefix}_${System.currentTimeMillis}.$extension"

    attemptCreatingUniqueFileName(fileName)
  }
}
