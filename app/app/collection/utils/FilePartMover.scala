package collection.utils

import java.io.File
import java.nio.file.Paths

import play.api.libs.Files
import play.api.mvc.MultipartFormData
import utils.ClassLogger

/**
  * The goal of this FilePartMover is to move the given [[MultipartFormData.FilePart]] to a
  * more definitive location (as passed along)
  */
class FilePartMover extends ClassLogger {

  /**
    * This method attempts to move the given [[MultipartFormData.FilePart]] to
    * the given filePath
    *
    * @param file     The temporarily FilePart which needs to be moved (e.g. 'tmp/new_image.png')
    * @param filePath The path where the file should be moved to
    * @return Either a [[Some(File)]] if the move was successful, otherwise [[None]]
    */
  def moveFile(file: MultipartFormData.FilePart[Files.TemporaryFile], filePath: String): Option[File] =
    for {
      newFile <- getFileIfNotExist(filePath)
      _ <- createParentDirectoriesIfNeeded(newFile)
      // We need to wrap the result from moveTo in an Option, to be able to use it in this for compression
      _ <- Some(file.ref.moveTo(Paths.get(filePath), replace = false))
    } yield newFile

  /**
    * @param filePath The filePath where the [[File]] should be located
    * @return Either [[Some(File)]] if the file didn't exist yet, or [[None]]
    *         if a [[File]] already exists at the given filePath
    */
  private def getFileIfNotExist(filePath: String): Option[File] = {
    val newFile = new File(filePath)

    if (newFile.exists) {
      logger.warn(s"Tried to move a FilePart to an already existing location: '$filePath'")
      None
    } else {
      Some(newFile)
    }
  }

  /**
    * @param file The [[File]] whose parent directories will be created (if they don't exist already)
    * @return Either a [[Some(File)]] on a successful creation, or [[None]] otherwise
    */
  private def createParentDirectoriesIfNeeded(file: File): Option[File] = {
    if (!file.getParentFile.exists && !file.getParentFile.mkdirs) {
      // We'll do the check again, because the directories might have been created by a different thread
      if (file.getParentFile.exists) {
        // Parent file has been created, probably by a different thread
        Some(file)
      } else {
        logger.error(s"mkdirs() returned false on the given filepath: '${file.getPath}'")
        None
      }
    } else {
      Some(file)
    }
  }
}
