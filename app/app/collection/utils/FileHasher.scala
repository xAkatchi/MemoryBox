package collection.utils

import java.security.{MessageDigest, DigestInputStream}
import java.io.{File, FileInputStream}

/**
  * The goal of this [[FileHasher]] is to be able to calculate
  * the hash of a given [[File]]
  */
class FileHasher {

  /**
    * This method creates a MD5 hash from the given [[File]]
    *
    * Obtained from: [[https://stackoverflow.com/a/41643076]]
    *
    * @param file The [[File]] whose hash should be calculated
    * @return The calculated hash
    */
  def computeMD5Hash(file: File): String = {
    val bufferSize = 8192
    val buffer = new Array[Byte](bufferSize)
    val md5 = MessageDigest.getInstance("MD5")

    val dis = new DigestInputStream(new FileInputStream(file), md5)

    try {
      while (dis.read(buffer) != -1) {}
    } finally {
      dis.close()
    }

    md5.digest.map("%02x".format(_)).mkString
  }
}
