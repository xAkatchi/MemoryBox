package collection.utils

import collection.models.Collection
import javax.inject.Inject
import play.api.Configuration

/**
  * The goal of this class is to build urls that can be used to redirect
  * users to pages in the Angular frontend.
  *
  * @param config The play configuration class
  */
class URLBuilder @Inject()(config: Configuration) {

  /**
    * Builds an url pointing towards the detail page of the given [[Collection]]
    *
    * @param collection The [[Collection]] to which the url must point
    * @return The url of the detail page
    */
  def buildCollectionDetailUrl(collection: Collection): String =
    s"${config.get[String]("ui.base-url")}dashboard/collection/${collection.slug}/photos"
}
