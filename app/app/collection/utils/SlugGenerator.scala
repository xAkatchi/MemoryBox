package collection.utils

import java.util.UUID

/**
  * The goal of this class is to generate a random slug
  * on invocation
  */
class SlugGenerator {
  /**
    * Whenever this method is invoked, it will generate
    * a random & unique slug.
    *
    * @return A random unique slug
    */
  def generate: String = UUID.randomUUID().toString
}
