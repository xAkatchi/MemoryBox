package collection.utils

import java.io.File
import java.nio.file.Files
import java.util.UUID

import javax.inject.Inject
import collection.models.{CollectionItem, CollectionItemThumbnail, FileSystemStorage, PhotoDetails}
import com.sksamuel.scrimage.Image
import com.sksamuel.scrimage.nio.ImageWriter
import utils.ClassLogger

/**
  * The goal of this class is to try and create a thumbnail for a given [[CollectionItem]]
  *
  * @param filePathFactory Used to determine where the thumbnail should be stored
  * @param fileHasher      Used to create a hash from the thumbnail
  * @param imageWriter     Used to write the thumbnail to the system
  */
class ThumbnailFactory @Inject()(filePathFactory: CollectionItemFilePathFactory,
                                 fileHasher: FileHasher,
                                 imageWriter: ImageWriter) extends ClassLogger {

  /**
    * Tries to create a thumbnail based on the given [[CollectionItem]] with respect
    * to the given height.
    *
    * @param item          The item of whom a thumbnail needs to be created
    * @param desiredHeight The desired height of the thumbnail
    * @return Either the created [[CollectionItemThumbnail]] or none
    */
  def createThumbnail(item: CollectionItem, desiredHeight: Int): Option[CollectionItemThumbnail] =
    item.fileDetails match {
      case _: PhotoDetails =>
        for {
          filePath <- getFilePath(item)
          file <- Some(new File(filePath))
          createdImage <- Some(Image.fromFile(file))
          resizedImage <- Some(createdImage.scaleToHeight(desiredHeight))
          pathToUpload <- Some(filePathFactory.getThumbnailUploadPath(file, item.uploadedBy))
          _ <- createParentDirectoriesIfNeeded(new File(pathToUpload))
          storedPath <- Some(resizedImage.output(pathToUpload)(imageWriter))
          storedFile <- Some(new File(storedPath.toString))
          storedFileHash <- Some(fileHasher.computeMD5Hash(storedFile))
          contentType <- Option(Files.probeContentType(storedPath))
        } yield CollectionItemThumbnail(
          UUID.randomUUID(),
          item,
          PhotoDetails(resizedImage.height, resizedImage.width, storedFile.length, storedFileHash, contentType),
          FileSystemStorage(storedPath.toString)
        )
      case _ =>
        logger.error("Unsupported file format (can't create a thumbnail)")
        None
    }

  /**
    * @param file The [[File]] whose parent directories will be created (if they don't exist already)
    * @return Either a [[Some(File)]] on a successful creation, or [[None]] otherwise
    */
  private def createParentDirectoriesIfNeeded(file: File): Option[File] = {
    if (!file.getParentFile.exists && !file.getParentFile.mkdirs) {
      // We'll do the check again, because the directories might have been created by a different thread
      if (file.getParentFile.exists) {
        // Parent file has been created, probably by a different thread
        Some(file)
      } else {
        logger.error(s"mkdirs() returned false on the given filepath: '${file.getPath}'")
        None
      }
    } else {
      Some(file)
    }
  }

  /**
    * @param item The item whose stored file path needs to be retrieved
    * @return Either the stored file path or none
    */
  private def getFilePath(item: CollectionItem): Option[String] = {
    item.fileStorage match {
      case storage: FileSystemStorage => Some(storage.filePath)
      case _ =>
        logger.error(s"Got a non FileSystemStorage whilst creating a thumbnail (${item.fileStorage.getClass})")
        None
    }
  }
}
