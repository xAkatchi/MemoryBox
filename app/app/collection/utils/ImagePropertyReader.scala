package collection.utils

import java.awt.Dimension
import java.io.{File, IOException}
import java.util
import javax.imageio.stream.FileImageInputStream
import javax.imageio.{ImageIO, ImageReader}

import org.apache.commons.io.FilenameUtils
import utils.ClassLogger

/**
  * The goal of the [[ImagePropertyReader]] is to be able to obtain the [[Dimension]]
  * from the given [[File]].
  */
class ImagePropertyReader extends ClassLogger {

  /**
    * This method will try to obtain the [[Dimension]] from the given [[File]]
    *
    * Source: [[https://stackoverflow.com/a/2911772]]
    *
    * @note That this is only relevant for [[File]]s that contain [[Dimension]],
    *       thus not text files and such.
    * @param file The [[File]] whose [[Dimension]] should be obtained
    * @return Either the [[Dimension]] of the given [[File]] (if found), or [[None]]
    */
  def getImageDimensions(file: File): Option[Dimension] = {
    val suffix: String = FilenameUtils.getExtension(file.getAbsolutePath)
    val iterator: util.Iterator[ImageReader] = ImageIO.getImageReadersBySuffix(suffix)

    if (iterator.hasNext) {
      val reader: ImageReader = iterator.next()

      try {
        val stream = new FileImageInputStream(file)

        reader.setInput(stream)
        val width = reader.getWidth(reader.getMinIndex)
        val height = reader.getHeight(reader.getMinIndex)
        stream.close()

        Some(new Dimension(width, height))
      } catch {
        case ioe: IOException =>
          logger.error(s"Got an IOException whilst reading the properties of a file (${file.getAbsolutePath}", ioe)
          None
      } finally {
        reader.dispose()
      }
    } else {
      logger.error(s"No reader found for the given file format (suffix: $suffix | path: ${file.getAbsolutePath})")
      None
    }
  }
}
