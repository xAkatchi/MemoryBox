package collection.api

import java.io.File
import java.util.UUID

import javax.inject.Inject
import authentication.utils.JwtEnv
import collection.models.{Collection, CollectionItem, FileSystemStorage}
import collection.service.{CollectionItemService, CollectionItemThumbnailService, CollectionService}
import com.mohiva.play.silhouette.api.Silhouette
import play.api.i18n.I18nSupport
import play.api.libs.json.{JsObject, JsValue, Json}
import play.api.mvc._
import utils.ClassLogger

import scala.concurrent.{ExecutionContext, Future}

/**
  * The controller tasked with actions related to the retrieval of [[collection.models.CollectionItem]]
  *
  * @param cc                    The controller components
  * @param silhouette            The Silhouette stack
  * @param collectionService     The collection service
  * @param collectionItemService The collection item service
  * @param thumbnailService      The service to interact with thumbnails
  * @param ec                    The used execution context
  */
class GetCollectionItemController @Inject()(cc: ControllerComponents,
                                            silhouette: Silhouette[JwtEnv],
                                            collectionService: CollectionService,
                                            collectionItemService: CollectionItemService,
                                            thumbnailService: CollectionItemThumbnailService)
                                           (implicit ec: ExecutionContext)
  extends AbstractController(cc) with I18nSupport with ClassLogger {

  /**
    * Tries to return the file from the given [[collection.models.CollectionItem]]
    * belonging to the given [[collection.models.Collection]]
    *
    * This endpoint is unprotected due to issues with passing the tokens on every request
    * + it might cause issues if we move the files to a cdn
    *
    * @param collectionSlug The slug of the [[collection.models.Collection]]
    * @param collectionItemSlug The slug of the [[collection.models.CollectionItem]]
    * @return The file (if any) or a NotFound response
    */
  def getFile(collectionSlug: String, collectionItemSlug: String): Action[AnyContent] = silhouette.UnsecuredAction.async { implicit request =>
    val collectionItem = for {
      collectionItem <- collectionItemService.find(UUID.fromString(collectionItemSlug))
    } yield collectionItem

    collectionItem.map {
      case Some(foundCollectionItem) =>
        foundCollectionItem.fileStorage match {
          case storage: FileSystemStorage =>
            Ok.sendFile(new File(storage.filePath))
          case _ =>
            logger.error(s"Unsupported Storage type found (collectionItemSlug: $collectionItemSlug)")
            NotFound
        }
      case _ => NotFound
    }
  }

  /**
    * Returns all the items that belong to the given [[Collection]]
    *
    * @param slug The slug of the [[Collection]]
    * @return The result which should be represented to the callee
    */
  def getItems(slug: String): Action[AnyContent] = silhouette.SecuredAction.async { implicit request =>
    collectionService.retrieveSingle(slug, request.identity).flatMap {
      case Some(foundCollection) =>
        getItemsAsJson(foundCollection).map(items => Ok(Json.toJson(items)))
      case None =>
        Future.successful(NotFound)
    }
  }

  /**
    * @param collectionItem The [[CollectionItem]] that should be checked against the [[Collection]]
    * @param collection The [[Collection]] to whom the [[CollectionItem]] should be checked against
    * @return Whether or not the given [[CollectionItem]] belongs to the given [[Collection]]
    */
  private def belongsTo(collectionItem: CollectionItem, collection: Collection): Boolean =
    collectionItem.collection == collection

  /**
    * @param collection The [[Collection]] whose [[CollectionItem]]s should be jsonified
    * @return The jsonified [[CollectionItem]]s
    */
  private def getItemsAsJson(collection: Collection): Future[Seq[JsValue]] =
    collectionItemService.find(collection).flatMap { items =>
      Future.sequence(items.map(itemToJson))
    }

  /**
    * @param item Transforms the given [[CollectionItem]] to a [[JsValue]]
    * @return The jsonified [[CollectionItem]]
    */
  private def itemToJson(item: CollectionItem): Future[JsValue] =
    thumbnailService.find(item).map { thumbnails =>
      Json.toJson(item).as[JsObject] +
        ("thumbnails" -> Json.toJson(thumbnails))
    }
}
