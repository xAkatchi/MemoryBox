package collection.api

import authentication.user.service.UserService
import authentication.user.User
import authentication.utils.JwtEnv
import cats.data.EitherT
import cats.implicits._
import collection.service.CollectionService
import collection.models.Collection
import collection.utils.URLBuilder
import com.mohiva.play.silhouette.api.Silhouette
import javax.inject.Inject
import play.api.i18n.{I18nSupport, Messages}
import play.api.libs.json.Json
import play.api.libs.mailer.{Email, MailerClient}
import play.api.mvc.{AbstractController, Action, AnyContent, ControllerComponents}

import scala.concurrent.{ExecutionContext, Future}

/**
  * The goal of this controller is to be able to add a [[User]] to a [[Collection]]
  *
  * @param cc                The controller components
  * @param silhouette        The Silhouette stack
  * @param collectionService The service to interact with collection
  * @param userService       The service to interact with users
  * @param mailerClient      The mailer client
  * @param ec                The used execution context
  */
class AddMemberToCollectionController @Inject()(cc: ControllerComponents,
                                                silhouette: Silhouette[JwtEnv],
                                                collectionService: CollectionService,
                                                userService: UserService,
                                                mailerClient: MailerClient,
                                                urlBuilder: URLBuilder)
                                               (implicit ec: ExecutionContext)
  extends AbstractController(cc) with I18nSupport {

  /**
    * The goal of this endpoint is to try and add the given [[User]] (defined in the post request)
    * to the given [[Collection]]
    *
    * @param collectionSlug The slug of the [[Collection]] to which the user should be added
    * @return The result which should be represented to the user
    */
  def addMember(collectionSlug: String): Action[AnyContent] =
    silhouette.SecuredAction.async { implicit request =>
      AddMemberToCollectionForm.form.bindFromRequest.fold(
        formWithErrors => { Future.successful(BadRequest(Json.obj("messages" -> Messages("invalid.data")))) },
        invitedEmail => {
          val additionResult = for {
            invitedUser <- EitherT.fromOptionF(
              userService.retrieve(invitedEmail),
              NotFound(Json.obj("messages" -> Messages("user.not.found")))
            )
            collection <- EitherT.fromOptionF(
              collectionService.retrieveSingle(collectionSlug, request.identity),
              NotFound(Json.obj("messages" -> Messages("collection.not.found")))
            )
            _ <- EitherT.fromOptionF(
              collectionService.addMember(invitedUser, request.identity, collection),
              BadRequest(Json.obj("messages" -> Messages("collection.member.addition.failed")))
            )
            _ <- EitherT.fromOptionF(
              sendInvitationMail(invitedUser, request.identity, collection),
              BadRequest(Json.obj("messages" -> Messages("collection.member.addition.mail.failed")))
            )
          } yield invitedUser

          additionResult.value.map {
            case Right(invitedUser) => Ok(Json.toJson(invitedUser))
            case Left(result) => result
          }
        }
      )
    }

  /**
    * This method will send an email to the [[User invitedUser]] informing them about the
    * invitation to the given [[Collection]]
    *
    * @param invitedUser       The [[User]] who got invited
    * @param invitee           The [[User]] who invited the other [[User]]
    * @param addedToCollection The [[Collection]] to which the [[User]] has been invited
    * @param messages          The i18n messages
    * @return A string containing the message id
    */
  private def sendInvitationMail(invitedUser: User, invitee: User, addedToCollection: Collection)(implicit messages: Messages): Future[Option[String]] = {
    val collectionDetailUrl = urlBuilder.buildCollectionDetailUrl(addedToCollection)

    Future.successful(Some(mailerClient.send(Email(
      subject = Messages("collection.member.addition.email.subject", addedToCollection.name),
      from = Messages("email.from"),
      to = Seq(invitedUser.email),
      bodyText = Some(collection.emails.txt.addMember(invitee, addedToCollection, collectionDetailUrl).body),
      bodyHtml = Some(collection.emails.html.addMember(invitee, addedToCollection, collectionDetailUrl).body)
    ))))
  }
}
