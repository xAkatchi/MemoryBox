package collection.api

import java.io.File
import java.nio.file.Paths
import java.util.{Date, UUID}
import javax.inject.Inject

import authentication.user.User
import authentication.utils.JwtEnv
import cats.data.EitherT
import cats.implicits._
import collection.api.models.MetaData
import collection.models._
import collection.service.{CollectionItemService, CollectionItemThumbnailService, CollectionService}
import collection.utils._
import com.mohiva.play.silhouette.api.Silhouette
import play.api.i18n.{I18nSupport, Messages, MessagesProvider}
import play.api.libs.Files
import play.api.mvc._
import play.api.libs.json._

import scala.concurrent.{ExecutionContext, Future}

/**
  * The goal of this controller is to be able to upload [[CollectionItem]]s which will
  * be linked to [[Collection]]s
  *
  * @param cc                             The controller components
  * @param silhouette                     The Silhouette stack
  * @param filePartMover                  The util class that should be used for moving the uploaded file
  * @param imagePropertyReader            The util class that should be used for reading properties from the uploaded
  *                                       image
  * @param fileHasher                     The util class that should be used to obtain the hash of the uploaded file
  * @param collectionItemFilePathFactory  The factory used to create the file path at which the file should be stored
  * @param collectionService              The service used to interact with [[Collection]] objects
  * @param collectionItemService          The service used to interact with [[CollectionItem]] objects
  * @param collectionItemThumbnailService The service used to interact with [[CollectionItemThumbnail]] objects
  * @param thumbnailFactory               Used to create thumbnails
  * @param ec                             The used execution context
  */
class CreateCollectionItemController @Inject()(cc: ControllerComponents,
                                               silhouette: Silhouette[JwtEnv],
                                               filePartMover: FilePartMover,
                                               imagePropertyReader: ImagePropertyReader,
                                               fileHasher: FileHasher,
                                               collectionItemFilePathFactory: CollectionItemFilePathFactory,
                                               collectionService: CollectionService,
                                               collectionItemService: CollectionItemService,
                                               collectionItemThumbnailService: CollectionItemThumbnailService,
                                               thumbnailFactory: ThumbnailFactory)
                                              (implicit ec: ExecutionContext)
  extends AbstractController(cc) with I18nSupport {

  private val maxFileSizeInBytes: Long = 1024 * 1024 * 20 // 20 MiB
  private val thumbnailHeight: Int = 300 // pixels

  /**
    * Handles the uploading of a [[CollectionItem]]
    *
    * @param collectionSlug The slug of the [[Collection]] to which the uploaded [[CollectionItem]] (if any)
    *                       should be linked
    * @return The [[Result]] which should be represented to the user
    */
  def upload(collectionSlug: String): Action[MultipartFormData[Files.TemporaryFile]] =
    silhouette.SecuredAction.async(parse.multipartFormData(maxFileSizeInBytes)) { implicit request =>

      val inputParameters = for {
        collection <- getCollection(collectionSlug, request.identity)

        filePart <- EitherT.fromEither[Future](request.body.file("file")
          .toRight(BadRequest(Json.obj("message" -> Messages("file.missing")))))

        rawMetaData <- EitherT.fromEither[Future](request.body.dataParts.get("metadata")
          .toRight(BadRequest(Json.obj("message" -> Messages("metadata.missing")))))

        metaData <- EitherT.fromEither[Future](validateRawMetaData(rawMetaData))

        uploadedFile <- EitherT.fromEither[Future](
          filePartMover.moveFile(filePart, collectionItemFilePathFactory.getUploadPath(filePart, request.identity))
            .toRight(Conflict(Json.obj("message" -> Messages("file.upload.failed.try.again"))))
        )
      } yield (collection, filePart, uploadedFile, metaData)

      inputParameters.value.flatMap {
        case Right((collection, filePart, uploadedFile, metaData)) =>
          filePart.contentType match {
            case Some(contentType) if contentType.startsWith("image/") =>
              handleImageUpload(uploadedFile, contentType, collection, metaData, request.identity)
            case _ => Future.successful(BadRequest(Json.obj("message" -> Messages("file.unsupported.content_type"))))
          }
        case Left(result) => Future.successful(result)
      }
    }

  /**
    * This method handles the uploading of an image (this includes saving into the database as well)
    *
    * @param uploadedFile The [[File]] that was uploaded (after it's moved to a more permanent location)
    * @param contentType The content type of the uploaded file
    * @param collection The [[Collection]] to which teh uploaded file should be linked
    * @param metaData The [[MetaData]] that was specified by the user
    * @param authenticatedUser The [[User]] that is doing this request
    * @param mp The [[MessagesProvider]], used for the translations
    * @return The [[Result]] which should be shown to the user
    */
  private def handleImageUpload(uploadedFile: File,
                                contentType: String,
                                collection: Collection,
                                metaData: MetaData,
                                authenticatedUser: User)
                               (implicit mp: MessagesProvider): Future[Result] = {
    val fileDetails = for {
      dimension <- imagePropertyReader.getImageDimensions(uploadedFile)
        .toRight(BadRequest(Json.obj("message" -> Messages("photo.extracting.dimensions.failed"))))
    } yield dimension

    fileDetails match {
      case Right(dimension) =>
        val hash = fileHasher.computeMD5Hash(uploadedFile)
        val storage = FileSystemStorage(uploadedFile.getPath)
        val details = PhotoDetails(dimension.height, dimension.width, uploadedFile.length, hash, contentType)
        val collectionItem = CollectionItem(
          UUID.randomUUID(), collection, metaData.name, storage,
          details, new Date(), authenticatedUser
        )

        val result = for {
          createdItem <- EitherT.fromOptionF(
            collectionItemService.create(collectionItem),
            BadRequest(Json.obj("message" -> Messages("collection.item.creation.failed")))
          )
          thumbnail <- EitherT.fromOption[Future](
            thumbnailFactory.createThumbnail(collectionItem, thumbnailHeight),
            BadRequest(Json.obj("message" -> Messages("collection.item.thumbnail.creation.failed")))
          )
          createdThumbnail <- EitherT.fromOptionF(
            collectionItemThumbnailService.save(thumbnail),
            BadRequest(Json.obj("message" -> Messages("collection.item.thumbnail.saving.failed")))
          )
        } yield (createdItem, createdThumbnail)

        result.value.flatMap {
          case Right((createdItem, createdThumbnail)) =>
            Future.successful(Ok(
              Json.toJson(createdItem).as[JsObject] +
                ("thumbnails" -> Json.toJson(Seq(createdThumbnail))
            )))
          case Left(errorResult) =>
            Future.successful(errorResult)
        }

      case Left(result) => Future.successful(result)
    }
  }

  /**
    * This method tries to obtain the [[Collection]] with the given slug
    *
    * @param collectionSlug The slug whose matching [[Collection]] should be searched for
    * @param authenticatedUser The [[User]] who is searching for the [[Collection]]
    * @param mp The [[MessagesProvider]], used for the translations
    * @return Either a [[Result]] in the case when the [[Collection]] couldn't be found,
    *         or the found [[Collection]] in the case that the [[Collection]] is found
    */
  private def getCollection(collectionSlug: String, authenticatedUser: User)
                           (implicit mp: MessagesProvider): EitherT[Future, Result, Collection] = {
    EitherT.fromOptionF(
      collectionService.retrieveSingle(collectionSlug, authenticatedUser),
      NotFound(Json.obj("messages" -> Messages("collection.not.found")))
    )
  }

  /**
    * This method validates the given 'raw' meta data from the user.
    *
    * @param rawMetaData The raw meta data from the user
    * @return Either a [[Result]] in the case that the validation failed, or a [[MetaData]] object
    *         in the case that the validation succeeded.
    */
  private def validateRawMetaData(rawMetaData: Seq[String]): Either[Result, MetaData] = {
    Json.parse(rawMetaData.head).validate[MetaData] match {
      case result: JsSuccess[MetaData] => Right(result.value)
      case e: JsError => Left(BadRequest(JsError.toJson(e)))
    }
  }
}
