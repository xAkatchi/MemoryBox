package collection.api

import java.io.File
import java.util.UUID

import javax.inject.Inject
import authentication.utils.JwtEnv
import collection.models.{Collection, CollectionItem, CollectionItemThumbnail, FileSystemStorage}
import collection.service.{CollectionItemService, CollectionItemThumbnailService, CollectionService}
import com.mohiva.play.silhouette.api.Silhouette
import play.api.i18n.I18nSupport
import play.api.mvc.{AbstractController, Action, AnyContent, ControllerComponents}
import utils.ClassLogger

import scala.concurrent.ExecutionContext

/**
  * The controller tasked with actions related to the retrieval of [[CollectionItemThumbnail]]
  *
  * @param cc                             The controller components
  * @param silhouette                     The Silhouette stack
  * @param collectionService              The collection service
  * @param collectionItemService          The collection item service
  * @param collectionItemThumbnailService The collection item thumbnail service
  * @param ec                             The used execution context
  */
class GetThumbnailController @Inject()(cc: ControllerComponents,
                                       silhouette: Silhouette[JwtEnv],
                                       collectionService: CollectionService,
                                       collectionItemService: CollectionItemService,
                                       collectionItemThumbnailService: CollectionItemThumbnailService)
                                      (implicit ec: ExecutionContext)
  extends AbstractController(cc) with I18nSupport with ClassLogger {

  /**
    * Tries to return the thumbnail as file.
    *
    * @param collectionSlug The slug of the [[Collection]]
    * @param itemSlug       The slug of the [[CollectionItem]]
    * @param thumbnailSlug  The slug of the [[CollectionItemThumbnail]]
    * @return The file (if any) or a [[NotFound]]
    */
  def getThumbnail(collectionSlug: String, itemSlug: String, thumbnailSlug: String): Action[AnyContent] =
    silhouette.SecuredAction.async { implicit request =>
      val result = for {
        collection <- collectionService.retrieveSingle(collectionSlug, request.identity)
        item <- collectionItemService.find(UUID.fromString(itemSlug))
        thumbnail <- collectionItemThumbnailService.find(UUID.fromString(thumbnailSlug))
      } yield (collection, item, thumbnail)

      result.map {
        case (Some(collection), Some(item), Some(thumbnail))
          // Make sure that everything belongs to each other to prevent people from accessing items that they
          // should not have access to.
          if belongToEachOther(collection, item, thumbnail) =>

          thumbnail.fileSystemStorage match {
            case storage: FileSystemStorage =>
              Ok.sendFile(new File(storage.filePath))
            case _ =>
              logger.error(s"Unsupported Storage type found (thumbnail: $thumbnailSlug)")
              NotFound
          }
        case _ => NotFound
      }
    }

  /**
    * @param collection The [[Collection]] to whom the [[CollectionItem]] should belong
    * @param item       The [[CollectionItem]] to whom the [[CollectionItemThumbnail]] should belong
    * @param thumbnail  The [[CollectionItemThumbnail]] itself
    * @return Whether or not the given objects 'belong' to each other
    */
  private def belongToEachOther(collection: Collection, item: CollectionItem, thumbnail: CollectionItemThumbnail): Boolean =
    collection == item.collection && item == thumbnail.collectionItem
}
