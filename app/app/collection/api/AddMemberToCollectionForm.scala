package collection.api

import authentication.user.User
import collection.models.Collection
import play.api.data.Form
import play.api.data.Forms._

/**
  * The form which handles the submissions of the additions of new [[User members]]
  * to a [[Collection]]
  */
object AddMemberToCollectionForm {

  /**
    * A play framework form.
    */
  val form = Form(
    single(
      "email" -> email
    )
  )
}
