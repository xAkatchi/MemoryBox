package collection.api.models

import collection.api.CreateCollectionItemController

import play.api.libs.json.{JsPath, Reads}

/**
  * The goal of the [[MetaData]] case class is to be a supplement inside the
  * [[CreateCollectionItemController]], where this case class contains the
  * meta data from the uploaded collection item.
  *
  * @param name        The name of the uploaded collection item
  */
case class MetaData(name: String)

object MetaData {
  implicit val metaDataReads: Reads[MetaData] =
    (JsPath \ "name").read[String].map(name => MetaData(name))
}
