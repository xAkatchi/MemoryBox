package collection.api

import java.util.Date
import javax.inject.Inject

import authentication.utils.JwtEnv
import com.mohiva.play.silhouette.api.Silhouette
import play.api.i18n.{I18nSupport, Messages}
import play.api.libs.json.{JsValue, Json}
import play.api.mvc.{AbstractController, Action, ControllerComponents}
import collection.models.Collection
import collection.service.CollectionService
import collection.utils.SlugGenerator

import scala.concurrent.{ExecutionContext, Future}

/**
  * This controller exposes the functionality to be able to create [[Collection]] objects
  *
  * @param cc                The controller components
  * @param silhouette        The Silhouette stack
  * @param collectionService The service to interact with collections
  * @param slugGenerator     The generator which will be used to create a slug
  * @param ec                The used execution context
  */
class CreateCollectionController @Inject()(cc: ControllerComponents,
                                           silhouette: Silhouette[JwtEnv],
                                           collectionService: CollectionService,
                                           slugGenerator: SlugGenerator)
                                          (implicit ec: ExecutionContext)
  extends AbstractController(cc) with I18nSupport {

  /**
    * Handles the submitted form, and tries to create a [[Collection]] with
    * details obtained from the form.
    *
    * @return The result which should be represented to the user
    */
  def submit: Action[JsValue] = silhouette.SecuredAction.async(parse.json) { implicit request =>
    request.body.validate[CreateCollectionForm.Data].map { data =>
      val collectionToCreate = Collection(slugGenerator.generate, data.name, data.description, new Date())

      collectionService.create(collectionToCreate, request.identity).map {
        case Some(createdCollection) => Ok(Json.toJson(createdCollection))
        case None => BadRequest(Json.obj("message" -> Messages("collection.creation.failed")))
      }
    }.recoverTotal { _ =>
      Future.successful(BadRequest(Json.obj("message" -> Messages("invalid.data"))))
    }
  }
}
