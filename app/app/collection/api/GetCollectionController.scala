package collection.api

import javax.inject.Inject
import authentication.utils.JwtEnv
import authentication.user.User
import collection.models.Collection
import collection.service.{CollectionItemService, CollectionItemThumbnailService, CollectionService}
import com.mohiva.play.silhouette.api.Silhouette
import play.api.i18n.I18nSupport
import play.api.libs.json.{JsObject, Json}
import play.api.mvc._

import scala.concurrent.{ExecutionContext, Future}

/**
  * This controller exposes the functionality to be able to 'fetch' [[Collection]] objects
  *
  * @param cc                    The controller components
  * @param silhouette            The Silhouette stack
  * @param collectionService     The service to interact with collections
  * @param ec                    The used execution context
  */
class GetCollectionController @Inject()(cc: ControllerComponents,
                                        silhouette: Silhouette[JwtEnv],
                                        collectionService: CollectionService)
                                       (implicit ec: ExecutionContext)
  extends AbstractController(cc) with I18nSupport {

  /**
    * Handles the fetching of all the [[Collection]] objects that belong to the authenticated
    * [[User]]
    *
    * @return The result which should be represented to the [[User]]
    */
  def getAll: Action[AnyContent] = silhouette.SecuredAction.async { implicit request =>
    collectionService.retrieveAll(request.identity)
      .map(collections => Ok(Json.toJson(collections)))
  }

  /**
    * Handles the fetching of a single [[Collection]] object with the given slug
    *
    * @param slug The slug that should be used when fetching the [[Collection]] object
    * @return The result which should be represented to the [[User]]
    */
  def getDetails(slug: String): Action[AnyContent] = silhouette.SecuredAction.async { implicit request =>
    collectionService.retrieveSingle(slug, request.identity).flatMap {
      case Some(foundCollection) =>
        collectionService.getMembersFromCollection(foundCollection).map(members => {
          // Adds the 'members' to the returned collection output so that the outer world doesn't
          // know that it's separated internally
          // Trick obtained from: https://stackoverflow.com/questions/22855710/play-framework-add-a-field-to-json-object/22856358#22856358
          Ok(
            Json.toJson(foundCollection).as[JsObject] +
              ("members" -> Json.toJson(members))
          )
        })
      case None => Future.successful(NotFound)
    }
  }
}
