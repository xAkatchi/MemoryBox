package collection.api

import play.api.data.Form
import play.api.data.Forms._
import play.api.libs.json.{Json, OFormat}
import collection.models.Collection

/**
  * The form which handles the submission of new [[Collection]] objects
  */
object CreateCollectionForm {

  /**
    * A play framework form.
    */
  val form = Form(
    mapping(
      "name" -> nonEmptyText(1, 64),
      "description" -> optional(text)
    )(Data.apply)(Data.unapply)
  )

  /**
    * The form data.
    *
    * @param name        The name of the [[Collection]].
    * @param description The description of the [[Collection]]
    */
  case class Data(name: String, description: Option[String])

  /**
    * The companion object.
    */
  object Data {
    /**
      * Converts the [[Data]] object to Json and vice versa.
      */
    implicit val jsonFormat: OFormat[Data] = Json.format[Data]
  }

}
