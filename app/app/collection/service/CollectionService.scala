package collection.service

import authentication.user.User
import collection.models.Collection

import scala.concurrent.Future

/**
  * Implementations of this class make it possible for the presentation layer
  * to communicate with the data/model layer through abstraction.
  */
trait CollectionService {

  /**
    * Implementations of this method should return all the [[Collection]] objects
    * that the given [[User]] can access.
    *
    * @param user The user whose [[Collection]]s should be fetched
    * @return A sequence of [[Collection]] objects
    */
  def retrieveAll(user: User): Future[Seq[Collection]]

  /**
    * Implementations of this method should try to return the [[Collection]]
    * with the given slug.
    *
    * The given [[User]] must have access to view the [[Collection]] otherwise
    * [[None]] should be returned
    *
    * @param slug The slug of the [[Collection]] that needs to be found
    * @param user The [[User]] who should have access to the fetched [[Collection]]
    * @return Either a [[Collection]] if one could be found, or [[None]] otherwise
    */
  def retrieveSingle(slug: String, user: User): Future[Option[Collection]]

  /**
    * Implementations of this method should return the members
    * of the given [[Collection]]
    *
    * @param collection The [[Collection]] whose members should be returned
    * @return The members of the given collection
    */
  def getMembersFromCollection(collection: Collection): Future[Seq[User]]

  /**
    * Implementations of this method should try to add the given
    * member to the given collection
    *
    * @param invitedUser The [[User]] that should be added
    * @param inviter The [[User]] who sent out the invite
    * @param collection The [[Collection]] to whom the invited user should be added
    * @return Either a [[Collection]] on success or None otherwise
    */
  def addMember(invitedUser: User, inviter: User, collection: Collection): Future[Option[Collection]]

    /**
    * Implementations of this method should try to create the given [[Collection]]
    * and grant access, to the just created [[Collection]], to the given [[User]]
    *
    * @param collection The collection which should be saved
    * @param user       The user who should gain access to the collection
    * @return The created [[Collection]] or [[None]] if the creation failed
    */
  def create(collection: Collection, user: User): Future[Option[Collection]]
}
