package collection.service

import javax.inject.Inject

import authentication.user.User
import collection.data.dao.{CollectionDAO, UserCollectionDAO}
import collection.models.Collection

import scala.concurrent.{ExecutionContext, Future}

/**
  * An implementation of the [[CollectionService]] that delegates the actions through
  * to the respective DAO's.
  *
  * @param collectionDAO     The [[CollectionDAO]] which will be used when delegating actions
  * @param userCollectionDAO The [[UserCollectionDAO]] which will be used when delegating actions
  * @param ec                The execution context
  */
class CollectionServiceImpl @Inject()(collectionDAO: CollectionDAO, userCollectionDAO: UserCollectionDAO)
                                     (implicit ec: ExecutionContext)
  extends CollectionService {

  /**
    * Returns all the [[Collection]] objects that the given [[User]]
    * can access.
    *
    * @param user The user whose [[Collection]]s should be fetched
    * @return A sequence of [[Collection]] objects
    */
  override def retrieveAll(user: User): Future[Seq[Collection]] =
    userCollectionDAO.find(user)

  /**
    * Tries to find the [[Collection]] with the given slug.
    * The given [[User]] must however have access to the [[Collection]]
    * otherwise [[None]] will be returned.
    *
    * @param slug The slug of the [[Collection]] that needs to be found
    * @param user The [[User]] who should have access to the fetched [[Collection]]
    * @return Either a [[Collection]] if one could be found, or [[None]] otherwise
    */
  override def retrieveSingle(slug: String, user: User): Future[Option[Collection]] =
    retrieveAll(user)
      .map(_.find(_.slug == slug))

  /**
    * @param collection The [[Collection]] whose members should be returned
    * @return The members of the given collection
    */
  override def getMembersFromCollection(collection: Collection): Future[Seq[User]] =
    userCollectionDAO.getMembers(collection)

  /**
    * @param invitedUser The [[User]] that should be added
    * @param inviter The [[User]] who sent out the invite
    * @param collection The [[Collection]] to whom the invited user should be added
    * @return Either a [[Collection]] on success or None otherwise
    */
  override def addMember(invitedUser: User, inviter: User, collection: Collection): Future[Option[Collection]] = {
    val checks = for {
      inviterBelongs <- userCollectionDAO.belongsToCollection(inviter, collection)
      invitedUserBelongs <- userCollectionDAO.belongsToCollection(invitedUser, collection)
    } yield (inviterBelongs, invitedUserBelongs)

    checks.flatMap { case (inviterBelongs, inviteeBelongs) =>
      if (inviterBelongs && !inviteeBelongs) {
        userCollectionDAO.add(invitedUser, collection).map {
          case Some(updatedCollection) => Some(updatedCollection)
          case _ => None
        }
      } else {
        Future.successful(None)
      }
    }
  }

  /**
    * Tries to create the given [[Collection]] and grant access,
    * to the just created [[Collection]], to the given [[User]]
    *
    * @param collection The collection which should be saved
    * @param user       The user who should gain access to the collection
    * @return The created [[Collection]] or [[None]] if the creation failed
    */
  override def create(collection: Collection, user: User): Future[Option[Collection]] =
    collectionDAO.add(collection).flatMap {
      case Some(createdCollection) => userCollectionDAO.add(user, createdCollection)
      case None => Future.successful(None)
    }
}
