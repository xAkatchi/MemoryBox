package collection.service

import java.util.UUID
import javax.inject.Inject

import collection.data.dao.CollectionItemThumbnailDAO
import collection.models.{CollectionItem, CollectionItemThumbnail}

import scala.concurrent.{ExecutionContext, Future}

/**
  * An implementation of the [[CollectionItemThumbnailService]] that delegates the actions
  * through to the respective DAO(s).
  *
  * @param collectionItemThumbnailDAO The [[CollectionItemThumbnailDAO]] which will be used when delegating actions
  * @param ec                         The execution context
  */
class CollectionItemThumbnailServiceImpl @Inject()(collectionItemThumbnailDAO: CollectionItemThumbnailDAO)
                                                  (implicit ec: ExecutionContext)
  extends CollectionItemThumbnailService {
  /**
    * @param id The id of the [[CollectionItemThumbnail]]
    * @return Either the found [[CollectionItemThumbnail]] or [[None]]
    */
  override def find(id: UUID): Future[Option[CollectionItemThumbnail]] =
    collectionItemThumbnailDAO.find(id)

  /**
    * @param collectionItem The [[CollectionItem]] whose [[CollectionItemThumbnail]]s need to be
    *                       obtained
    * @return The [[CollectionItemThumbnail]]s belonging to the given [[CollectionItem]]
    */
  override def find(collectionItem: CollectionItem): Future[Seq[CollectionItemThumbnail]] =
    collectionItemThumbnailDAO.find(collectionItem)

  /**
    * @param collectionItemThumbnail The [[CollectionItemThumbnail]] that should be created
    * @return Either [[CollectionItemThumbnail]] on success or [[None]] otherwise
    */
  override def save(collectionItemThumbnail: CollectionItemThumbnail): Future[Option[CollectionItemThumbnail]] =
    collectionItemThumbnailDAO.save(collectionItemThumbnail)
}
