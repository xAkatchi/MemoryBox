package collection.service

import java.util.UUID

import collection.models.{CollectionItem, CollectionItemThumbnail}

import scala.concurrent.Future

/**
  * Implementations of this class should make it possible to communicate
  * with the data/model layer through abstraction
  */
trait CollectionItemThumbnailService {
  /**
    * @param id The id of the [[CollectionItemThumbnail]]
    * @return Either the found [[CollectionItemThumbnail]] or [[None]]
    */
  def find(id: UUID): Future[Option[CollectionItemThumbnail]]

  /**
    * @param collectionItem The [[CollectionItem]] whose [[CollectionItemThumbnail]]s need to be
    *                       obtained
    * @return The [[CollectionItemThumbnail]]s belonging to the given [[CollectionItem]]
    */
  def find(collectionItem: CollectionItem): Future[Seq[CollectionItemThumbnail]]

  /**
    * @param collectionItemThumbnail The [[CollectionItemThumbnail]] that should be created
    * @return Either [[CollectionItemThumbnail]] on success or [[None]] otherwise
    */
  def save(collectionItemThumbnail: CollectionItemThumbnail): Future[Option[CollectionItemThumbnail]]
}
