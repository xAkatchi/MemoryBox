package collection.service

import java.util.UUID

import collection.models.{Collection, CollectionItem}

import scala.concurrent.Future

/**
  * Implementations of this class should make it possible to communicate
  * with the data/model layer through abstraction
  */
trait CollectionItemService {
  /**
    * Find a [[CollectionItem]] by its slug
    *
    * @param slug The slug of the [[CollectionItem]] to find
    * @return The found [[CollectionItem]] or None if no [[CollectionItem]] could be found
    */
  def find(slug: UUID): Future[Option[CollectionItem]]

  /**
    * Find all the [[CollectionItem]]s that belong to the given [[Collection]]
    *
    * @param collection The [[Collection]] whose [[CollectionItem]]s should be retrieved
    * @return The [[CollectionItem]]s that belong to the given [[Collection]]
    */
  def find(collection: Collection): Future[Seq[CollectionItem]]

  /**
    * Implementations of this method should try to create the given [[CollectionItem]]
    * linked to the given [[Collection]] (inside the CollectionItem)
    *
    * @param collectionItem The [[CollectionItem]] that should be created
    * @return Either [[Some(CollectionItem)]] on a successful creation, or [[None]] otherwise
    */
  def create(collectionItem: CollectionItem): Future[Option[CollectionItem]]
}
