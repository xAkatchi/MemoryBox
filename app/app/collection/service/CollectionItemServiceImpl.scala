package collection.service

import java.util.UUID
import javax.inject.Inject

import collection.data.dao.CollectionItemDAO
import collection.models.{Collection, CollectionItem}

import scala.concurrent.{ExecutionContext, Future}

/**
  * An implementation of the [[CollectionItemService]] that delegates the actions
  * through to the respective DAO(s).
  *
  * @param collectionItemDAO The [[CollectionItemDAO]] which will be used when delegating actions
  * @param ec                The execution context
  */
class CollectionItemServiceImpl @Inject()(collectionItemDAO: CollectionItemDAO)
                                         (implicit ec: ExecutionContext)
  extends CollectionItemService {

  /**
    * @param slug The slug of the [[CollectionItem]] to find
    * @return The found [[CollectionItem]] or None if no [[CollectionItem]] could be found
    */
  def find(slug: UUID): Future[Option[CollectionItem]] =
    collectionItemDAO.find(slug)

  /**
    * @param collection The [[Collection]] whose [[CollectionItem]]s should be retrieved
    * @return The [[CollectionItem]]s that belong to the given [[Collection]]
    */
  def find(collection: Collection): Future[Seq[CollectionItem]] =
    collectionItemDAO.find(collection)

  /**
    * Tries to create the given [[CollectionItem]] and link it to the given [[Collection]]
    *
    * @param collectionItem The [[CollectionItem]] that should be created
    * @return Either [[Some(CollectionItem)]] on a successful creation, or [[None]] otherwise
    */
  override def create(collectionItem: CollectionItem): Future[Option[CollectionItem]] =
    collectionItemDAO.save(collectionItem)
}
