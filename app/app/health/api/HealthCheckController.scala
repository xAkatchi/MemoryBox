package health.api

import com.google.inject.Inject
import play.api.mvc.{AbstractController, Action, AnyContent, ControllerComponents}

import scala.concurrent.Future

/**
  * This health check controller can be used by the ingress controller
  * inside the kubernetes cluster to know whether or not this service is up and running.
  *
  * @param cc The controller components.
  */
class HealthCheckController @Inject()(cc: ControllerComponents)
  extends AbstractController(cc) {

  /**
    * Returns a simple 200 response which is sufficient for the ingress
    * controller to know that this service is running
    *
    * @return The health response
    */
  def getHealth: Action[AnyContent] = Action.async { implicit request =>
    Future.successful(Ok)
  }

}
