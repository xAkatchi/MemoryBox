package core.quartz

import com.google.inject.{AbstractModule, Singleton}
import com.typesafe.akka.extension.quartz.QuartzSchedulerExtension
import net.codingwell.scalaguice.ScalaModule
import play.api.libs.concurrent.AkkaGuiceSupport

/**
  * The Guice module for the Quartz scheduler.
  *
  * See: [[https://github.com/enragedginger/akka-quartz-scheduler]]
  */
class QuartzSchedulerModule extends AbstractModule with ScalaModule with AkkaGuiceSupport {
  /**
    * We override our parent to configure
    * the desired bindings used by the Quartz module
    */
  override def configure(): Unit = {
    bind[QuartzSchedulerExtension].toProvider[QuartzSchedulerProvider].in[Singleton]
  }
}

