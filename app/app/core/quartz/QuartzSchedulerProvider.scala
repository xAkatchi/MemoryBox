package core.quartz

import akka.actor.ActorSystem
import com.google.inject.{Provider, Singleton}
import com.typesafe.akka.extension.quartz.QuartzSchedulerExtension
import javax.inject.Inject
import play.api.inject.ApplicationLifecycle
import utils.ClassLogger

import scala.concurrent.Future

/**
  * Provides the Quartz scheduler.
  *
  * Inspired by: [[https://github.com/setusoft/silhouette-play-react-seed/blob/master/src/main/scala/modules/QuartzSchedulerModule.scala]]
  *
  * @param system    The actor system.
  */
@Singleton
class QuartzSchedulerProvider @Inject()(system: ActorSystem)
  extends Provider[QuartzSchedulerExtension] with ClassLogger {

  /**
    * Gets the Quartz scheduler.
    *
    * @return The Quartz scheduler instance.
    */
  override def get(): QuartzSchedulerExtension = {
    QuartzSchedulerExtension(system)
  }
}
