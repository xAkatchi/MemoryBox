package core.token.data.enums

import core.token.data.enums

/**
  * This enum contains all the valid type values for the
  * `token_type` table
  */
object TokenType extends Enumeration {
  type TokenType = Value

  val USER: enums.TokenType.Value = Value
}
