package core.token.data.enums

import core.token.data.enums

/**
  * This enum contains all the valid subtype values for the
  * `token_type` table
  */
object TokenSubType extends Enumeration {
  type TokenSubType = Value

  val ACCOUNT_ACTIVATION: enums.TokenSubType.Value = Value
}
