package core.token.data.components

import authentication.user.User
import authentication.user.component.UserComponent
import play.api.db.slick.HasDatabaseConfigProvider
import slick.jdbc.JdbcProfile

/**
  * A trait containing a reference to the `token_user` table.
  *
  * Include this trait whenever you have a desire to do something with the token_user table.
  */
trait UserTokenComponent extends TokenComponent
  with UserComponent {
  self: HasDatabaseConfigProvider[JdbcProfile] =>

  import profile.api._

  protected val tblTokenUser = TableQuery[TokenUsers]

  /**
    * @param tokenId The referenced id of the main 'token' table
    * @param userId  The id of the [[User]] to whom this token is applicable
    */
  case class DBTokenUser(tokenId: Int, userId: String)

  /**
    * This class contains a mapping from the database table to an object.
    * ([[http://slick.lightbend.com/doc/3.0.3/gettingstarted.html#schema as required by Slick]])
    *
    * @param tag
    */
  class TokenUsers(tag: Tag) extends Table[DBTokenUser](tag, "token_user") {
    def tokenId = column[Int]("token_id", O.PrimaryKey)
    def userId = column[String]("user_id")

    def userFK = foreignKey("fk_token_user_user_id", userId, tblUser)(_.id)

    def * = (tokenId, userId) <> (DBTokenUser.tupled, DBTokenUser.unapply)
  }

  /**
    * @param tokens A reference containing all the [[DBToken]] entries whom should be included in the search
    * @param user   The [[User]] whose tokens should be found
    * @return A query prepared to search for the [[DBTokenUser]]
    */
  protected def findDBTokenUsers(tokens: Tokens, user: User): Query[TokenUsers, DBTokenUser, Seq] =
    tblTokenUser.filter(dbTokenUser =>
      dbTokenUser.tokenId === tokens.id &&
      dbTokenUser.userId === user.id.toString
    )
}
