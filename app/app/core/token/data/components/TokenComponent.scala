package core.token.data.components

import java.sql.Timestamp
import java.time.Instant

import play.api.db.slick.HasDatabaseConfigProvider
import slick.jdbc.JdbcProfile

/**
  * A trait containing a reference to the `token` table.
  *
  * Include this trait whenever you have a desire to do something with the token table.
  */
trait TokenComponent extends TokenTypeComponent {
  self: HasDatabaseConfigProvider[JdbcProfile] =>

  import profile.api._

  protected val tblToken = TableQuery[Tokens]

  /**
    * @param id         The primary key
    * @param token      The 'token' itself
    * @param typeId     The 'type' of the token (a reference to the 'token_type' table)
    * @param validUntil A timestamp indicating how long this token is valid
    * @param createdAt  A timestamp indicating whenever this token was created
    */
  case class DBToken(id: Option[Int], token: String, typeId: Int, validUntil: Timestamp, createdAt: Timestamp)

  /**
    * This class contains a mapping from the database table to an object.
    * ([[http://slick.lightbend.com/doc/3.0.3/gettingstarted.html#schema as required by Slick]])
    *
    * @param tag
    */
  class Tokens(tag: Tag) extends Table[DBToken](tag, "token") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def token = column[String]("token")
    def typeId = column[Int]("type_id")
    def validUntil = column[Timestamp]("valid_until")
    def createdAt = column[Timestamp]("created_at")

    def typeFK = foreignKey("fk_token_type_id", typeId, tblTokenType)(_.id)

    def * = (id.?, token, typeId, validUntil, createdAt) <> (DBToken.tupled, DBToken.unapply)
  }

  /**
    * @param dbTokenType The [[DBTokenType]] of the [[DBToken]] to be inserted
    * @param token       The 'token' of the [[DBToken]] to be inserted
    * @param validUntil  A timestamp indicating until when the [[DBToken]] is valid
    * @param createdAt   The timestamp indicating when this token was created
    * @return The created [[DBToken]] (inside a query-context)
    */
  protected def insertDBToken(dbTokenType: DBTokenType, token: String, validUntil: Timestamp, createdAt: Timestamp) = {
    val dbToken = DBToken(None, token, dbTokenType.id.get, validUntil, createdAt)

    tblToken
      .returning(tblToken.map(_.id))
      .into((item, id) => item.copy(id = Some(id))) += dbToken
  }
}
