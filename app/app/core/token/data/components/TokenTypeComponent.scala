package core.token.data.components

import core.token.data.enums.TokenSubType.TokenSubType
import core.token.data.enums.TokenType.TokenType
import play.api.db.slick.HasDatabaseConfigProvider
import slick.jdbc.JdbcProfile

/**
  * A trait containing a reference to the `token_type` table.
  *
  * Include this trait whenever you have a desire to do something with the token_type table.
  */
trait TokenTypeComponent {
  self: HasDatabaseConfigProvider[JdbcProfile] =>

  import profile.api._

  protected val tblTokenType = TableQuery[TokenTypes]

  /**
    * @param id           The primary key
    * @param tokenType    The type indication of the token
    * @param tokenSubType The subtype indication of the token
    */
  case class DBTokenType(id: Option[Int], tokenType: String, tokenSubType: String)

  /**
    * This class contains a mapping from the database table to an object.
    * ([[http://slick.lightbend.com/doc/3.0.3/gettingstarted.html#schema as required by Slick]])
    *
    * @param tag
    */
  class TokenTypes(tag: Tag) extends Table[DBTokenType](tag, "token_type") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def tokenType = column[String]("type")
    def tokenSubType = column[String]("sub_type")

    def * = (id.?, tokenType, tokenSubType) <> (DBTokenType.tupled, DBTokenType.unapply)
  }

  /**
    * @param tokenType    The type of the [[DBTokenType]] to be found
    * @param tokenSubType The subtype of the [[DBTokenType]] to be found
    * @return A query prepared to search for the [[DBTokenType]]
    */
  protected def findTokenTypes(tokenType: TokenType, tokenSubType: TokenSubType): Query[TokenTypes, DBTokenType, Seq] =
    tblTokenType.filter(result =>
      result.tokenType === tokenType.toString &&
      result.tokenSubType === tokenSubType.toString
    )
}
