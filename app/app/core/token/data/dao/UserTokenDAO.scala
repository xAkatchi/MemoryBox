package core.token.data.dao

import java.sql.SQLException
import java.time.Instant

import authentication.user.User
import core.token.model.UserToken
import core.token.data.enums.TokenSubType.TokenSubType

import scala.concurrent.Future

/**
  * Implementations of this class should give access to the [[UserToken]] object
  * in the persistence layer
  */
trait UserTokenDAO {
  /**
    * @param user    The [[User]] whose [[UserToken]]s should be obtained
    * @param subType The subtype of the tokens that should be obtained
    * @return A list of [[UserToken]] objects that belong to the given criteria
    */
  def find(user: User, subType: TokenSubType): Future[Seq[UserToken]]

  /**
    * @param token   The 'token' of the [[UserToken]] that should be obtained
    * @param user    The [[User]] whose [[UserToken]] should be obtained
    * @param subType The subtype of the token that should be obtained
    * @return Either the [[UserToken]] when something was found, or [[None]] otherwise
    */
  def find(token: String, user: User, subType: TokenSubType): Future[Option[UserToken]]

  /**
    * @return A list of expired [[UserToken]]s
    */
  def findExpired(): Future[Seq[UserToken]]

  /**
    * @param token   The token to check
    * @param subType The subtype of the token to check for
    * @return
    */
  def doesTokenExist(token: String, subType: TokenSubType): Future[Boolean]

  /**
    * @param token      The 'token' of the [[UserToken]] that should be stored
    * @param validUntil An [[Instant]] indicating until when the token should be valid
    * @param subType    The subtype of the [[UserToken]]
    * @param user       The [[User]] for whom the token should be created
    * @return The stored [[UserToken]] on success or [[None]] otherwise
    * @throws SQLException If a SQL constraint fails (e.g. the unique constraint)
    */
  def save(token: String, validUntil: Instant, subType: TokenSubType, user: User): Future[Option[UserToken]]

  /**
    * @param userToken The [[UserToken]] that should be removed
    * @return A boolean indicating whether or not the removal was successful
    */
  def remove(userToken: UserToken): Future[Boolean]
}
