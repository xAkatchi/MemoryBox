package core.token.data.dao

import java.sql.{SQLException, Timestamp}
import java.time.{Clock, Instant}
import java.util.UUID

import authentication.user.User
import core.token.data.components.UserTokenComponent
import core.token.model.UserToken
import core.token.data.enums.TokenSubType.TokenSubType
import core.token.data.enums.TokenType
import javax.inject.{Inject, Singleton}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

/**
  * An implementation of the [[UserTokenDAO]] using a Database (through Slick)
  * as storage layer.
  *
  * @param dbConfigProvider The slick database that this DAO is using internally.
  * @param clock            The clock instance (used for timing related logic)
  * @param ec               A CPU bound execution context. Slick manages blocking JDBC calls with its
  *                         own internal thread pool, so Play's default execution context is fine here.
  */
@Singleton
class UserTokenDAOImpl @Inject()(protected val dbConfigProvider: DatabaseConfigProvider,
                                 protected val clock: Clock)
                                (implicit ec: ExecutionContext)
  extends UserTokenDAO
    with HasDatabaseConfigProvider[JdbcProfile]
    with UserTokenComponent {

  import profile.api._

  /**
    * @param user    The [[User]] whose [[UserToken]]s should be obtained
    * @param subType The subtype of the tokens that should be obtained
    * @return A list of [[UserToken]] objects that belong to the given criteria
    */
  override def find(user: User, subType: TokenSubType): Future[Seq[UserToken]] = {
    val query = for {
      dbTokenType <- findTokenTypes(TokenType.USER, subType)
      dbToken <- tblToken.filter(_.typeId === dbTokenType.id)
      dbTokenUser <- findDBTokenUsers(dbToken, user)
    } yield (dbToken, dbTokenUser)

    db.run(query.result).map(_.map(result => createUserToken(result._1, result._2)))
  }

  /**
    * @param token   The 'token' of the [[UserToken]] that should be obtained
    * @param user    The [[User]] whose [[UserToken]] should be obtained
    * @param subType The subtype of the token that should be obtained
    * @return Either the [[UserToken]] when something was found, or [[None]] otherwise
    */
  override def find(token: String, user: User, subType: TokenSubType): Future[Option[UserToken]] = {
    val query = for {
      dbTokenType <- findTokenTypes(TokenType.USER, subType)
      dbToken <- tblToken if dbToken.typeId === dbTokenType.id && dbToken.token === token
      dbTokenUser <- findDBTokenUsers(dbToken, user)
    } yield (dbToken, dbTokenUser)

    db.run(query.result.headOption).map {
      case Some((dbToken: DBToken, dbTokenUser: DBTokenUser)) => Some(createUserToken(dbToken, dbTokenUser))
      case _ => None
    }
  }

  /**
    * @return A list of expired [[UserToken]]s
    */
  override def findExpired(): Future[Seq[UserToken]] = {
    val query = for {
      dbTokenType <- tblTokenType.filter(_.tokenType === TokenType.USER.toString)
      dbToken <- tblToken if dbToken.typeId === dbTokenType.id && dbToken.validUntil <= Timestamp.from(clock.instant)
      dbTokenUser <- tblTokenUser if dbTokenUser.tokenId === dbToken.id
    } yield (dbToken, dbTokenUser)

    db.run(query.result).map(_.map(result => createUserToken(result._1, result._2)))
  }

  /**
    * @param token   The token to check
    * @param subType The subtype of the token to check for
    * @return
    */
  override def doesTokenExist(token: String, subType: TokenSubType): Future[Boolean] =
    db.run(
      tblToken.join(tblTokenType).on(_.typeId === _.id)
        .filter { result =>
          result._1.token === token &&
          result._2.tokenType === TokenType.USER.toString &&
          result._2.tokenSubType === subType.toString
        }
        .exists.result
    )

  /**
    * @param token      The 'token' of the [[UserToken]] that should be stored
    * @param validUntil An [[Instant]] indicating until when the token should be valid
    * @param subType    The subtype of the [[UserToken]]
    * @param user       The [[User]] for whom the token should be created
    * @return The stored [[UserToken]] on success or [[None]] otherwise
    * @throws SQLException If a SQL constraint fails (e.g. the unique constraint)
    */
  override def save(token: String, validUntil: Instant, subType: TokenSubType, user: User): Future[Option[UserToken]] = {
    val getTokenTypeAction = findTokenTypes(TokenType.USER, subType).result.headOption

    def insertUserToken(dbToken: DBToken) =
      tblTokenUser += DBTokenUser(dbToken.id.get, user.id.toString)

    db.run(getTokenTypeAction).flatMap {
      case Some(dbTokenType: DBTokenType) =>
        val query = (for {
          dbToken <- insertDBToken(dbTokenType, token, Timestamp.from(validUntil), Timestamp.from(clock.instant()))
          _ <- insertUserToken(dbToken)
        } yield dbToken).transactionally

        db.run(query).map(dbToken => Some(UserToken(
          dbToken.id.get,
          dbToken.token,
          dbToken.validUntil.toInstant,
          user.id
        )))
      case None =>
        Future.successful(None)
    }
  }

  /**
    * @param userToken The [[UserToken]] that should be removed
    * @return A boolean indicating whether or not the removal was successful
    */
  override def remove(userToken: UserToken): Future[Boolean] = {
    val query = (for {
      tokenUserRowsAffected <- tblTokenUser.filter(_.tokenId === userToken.id).delete
      tokenRowsAffected <- tblToken.filter(_.id === userToken.id).delete
    } yield tokenUserRowsAffected + tokenRowsAffected).transactionally

    db.run(query).map(_ > 0)
  }

  /**
    * A convenience function to create a [[UserToken]]
    *
    * @param dbToken     The [[DBToken]] to which the [[UserToken]] should be linked
    * @param dbTokenUser The database representation of the [[UserToken]]
    * @return The created [[UserToken]] object
    */
  private def createUserToken(dbToken: DBToken, dbTokenUser: DBTokenUser): UserToken =
    UserToken(
      dbToken.id.get,
      dbToken.token,
      dbToken.validUntil.toInstant,
      UUID.fromString(dbTokenUser.userId)
    )
}
