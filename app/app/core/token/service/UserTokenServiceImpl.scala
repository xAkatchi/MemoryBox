package core.token.service

import java.time.Clock

import authentication.user.User
import core.token.data.dao.UserTokenDAO
import core.token.model.UserToken
import core.token.data.enums.TokenSubType
import javax.inject.Inject
import utils.Hasher

import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration._
import scala.util.Random

/**
  * This implementation focuses on providing the 'default' implementations for the
  * methods defined inside the [[UserTokenService]] interface
  *
  * @param userTokenDAO The [[UserTokenDAO]] which will be used when delegating actions
  * @param clock        The clock used for working with times
  * @param ec           The execution context
  */
class UserTokenServiceImpl @Inject()(userTokenDAO: UserTokenDAO,
                                     clock: Clock,
                                     hasher: Hasher)
                                    (implicit ec: ExecutionContext)
  extends UserTokenService {

  /**
    * This Map contains the config indicating how many tokens are allowed
    * to be active of a given type at any time.
    */
  private val tokenLimits = Map(TokenSubType.ACCOUNT_ACTIVATION -> 5)
  private val tokenLength: Int = 200

  /**
    * @param user             The [[User]] for whom the [[UserToken]] should be created
    * @param validityDuration The duration for how long the created [[UserToken]] should be valid
    * @return Either a [[UserToken]] on success or [[None]] at failure
    */
  override def createAccountActivationToken(user: User, validityDuration: FiniteDuration): Future[Option[UserToken]] = {
    def generateUniqueToken(): Future[String] = {
      val token = Random.alphanumeric.take(tokenLength).mkString

      userTokenDAO.doesTokenExist(token, TokenSubType.ACCOUNT_ACTIVATION).flatMap { doesExist =>
        if (doesExist) { generateUniqueToken() }
        else { Future.successful(token) }
      }
    }

    userTokenDAO
      .find(user, TokenSubType.ACCOUNT_ACTIVATION)
      .map(_.filter(token => !isExpired(token)))
      .flatMap(tokens => {
        if (tokens.length >= tokenLimits.getOrElse(TokenSubType.ACCOUNT_ACTIVATION, 0)) {
          Future.successful(None)
        } else {
          generateUniqueToken().flatMap { token =>
            val hashedToken = hasher.hash(token)

            userTokenDAO.save(
              hashedToken,
              clock.instant().plusSeconds(validityDuration.toSeconds),
              TokenSubType.ACCOUNT_ACTIVATION,
              user
            ).map {
              // Override the returned hashed token with the original token, this so that the callee can use
              // this token
              case Some(userToken) => Some(userToken.copy(token = token))
              case _ => None
            }
          }
        }
      })
  }

  /**
    * Checks whether or not the token / user combination is a valid account activation token combination.
    *
    * If it's valid, the respective token will be removed so that it can no longer be used.
    *
    * @param token The token to be checked
    * @param user  The [[User]] for whom the token should be checked
    * @return A boolean indicating whether or not the token is valid
    */
  override def isValidAccountActivationToken(token: String, user: User): Future[Boolean] =
    userTokenDAO
      // Hash the token since we only store token hashes (thus we need to find them by hashes instead of by strings)
      .find(hasher.hash(token), user, TokenSubType.ACCOUNT_ACTIVATION)
      .flatMap {
        case Some(userToken) =>
          if (isExpired(userToken)) {
            Future.successful(false)
          } else {
            userTokenDAO.remove(userToken)
          }
        case _ => Future.successful(false)
      }

  /**
    * @return A Seq containing all the removed tokens
    */
  override def removeExpiredTokens(): Future[Seq[UserToken]] =
    userTokenDAO
      .findExpired()
      .flatMap { expiredTokens =>
        // Trick obtained from: https://stackoverflow.com/a/48602938
        // (Only keep tokens that were successfully removed)
        Future.sequence(expiredTokens.map(userTokenDAO.remove)).map {
          removalResults => expiredTokens
            .zip(removalResults)
            .filter(_._2) // Only keep the tokens that were successfully removed
            .map(_._1) // And then return the remaining (thus deleted) tokens
        }
      }

  /**
    * Returns whether or not the given [[UserToken]] has expired
    *
    * @param token The [[UserToken]] to check
    * @return A boolean indicating whether or not it has been expired
    */
  private def isExpired(token: UserToken): Boolean =
    clock.instant().isAfter(token.validUntil)
}
