package core.token.service

import authentication.user.User
import core.token.model.UserToken

import scala.concurrent.Future
import scala.concurrent.duration._

/**
  * Implementation of this service should make it possible to interact with the
  * 'user token' system.
  */
trait UserTokenService {
  /**
    * @param user             The [[User]] for whom the [[UserToken]] should be created
    * @param validityDuration The duration for how long the created [[UserToken]] should be valid
    * @return Either a [[UserToken]] on success or [[None]] at failure
    */
  def createAccountActivationToken(user: User, validityDuration: FiniteDuration): Future[Option[UserToken]]

  /**
    * Implementations of this method will check to see if the given token/user combination is
    * a valid account activation combination.
    *
    * If it's valid, the token should be disabled (and thus no longer be valid on subsequent invocations)
    *
    * @param token The token to be checked
    * @param user  The [[User]] for whom the token should be checked
    * @return A boolean indicating whether or not the token is valid
    */
  def isValidAccountActivationToken(token: String, user: User): Future[Boolean]

  /**
    * Implementations of this method should remove all the expired tokens
    *
    * @return A Seq containing all the removed tokens
    */
  def removeExpiredTokens(): Future[Seq[UserToken]]
}
