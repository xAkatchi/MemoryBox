package core.token

import java.time.Clock

import com.google.inject.AbstractModule
import core.token.data.dao.{UserTokenDAO, UserTokenDAOImpl}
import core.token.jobs.{Scheduler, UserTokenCleaner}
import core.token.service.{UserTokenService, UserTokenServiceImpl}
import net.codingwell.scalaguice.ScalaModule
import play.api.libs.concurrent.AkkaGuiceSupport
import utils.Hasher

/**
  * The Guice module which wires all Token dependencies.
  *
  * [[https://www.playframework.com/documentation/2.6.x/ScalaDependencyInjection#programmatic-bindings Documentation]]
  */
class TokenModule extends AbstractModule with ScalaModule with AkkaGuiceSupport {
  /**
    * We override our parent to configure
    * the desired bindings used by the Token system
    */
  override def configure(): Unit = {
    bind[UserTokenService].to[UserTokenServiceImpl]
    bind[UserTokenDAO].to[UserTokenDAOImpl]
    bind[Clock].toInstance(Clock.systemUTC())
    bind[Hasher].toInstance(new Hasher)

    bindActor[UserTokenCleaner](UserTokenCleaner.Name)
    bind[Scheduler].asEagerSingleton()
  }
}
