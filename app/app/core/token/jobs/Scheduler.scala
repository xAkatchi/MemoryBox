package core.token.jobs

import java.util.Date

import akka.actor.{ActorRef, ActorSystem}
import com.google.inject.Inject
import com.google.inject.name.Named
import com.typesafe.akka.extension.quartz.QuartzSchedulerExtension
import utils.ClassLogger

/**
  * Schedules the jobs for the token system
  *
  * @param system           The actor system.
  * @param quartzScheduler  The Quartz scheduler.
  * @param userTokenCleaner The [[UserTokenCleaner]] actor.
  */
class Scheduler @Inject()(system: ActorSystem,
                          quartzScheduler: QuartzSchedulerExtension,
                          @Named(UserTokenCleaner.Name) userTokenCleaner: ActorRef)
  extends ClassLogger {

  val userTokenCleanerScheduleDate: Date =
    quartzScheduler.schedule("UserTokenCleaner", userTokenCleaner, UserTokenCleaner.Clean)

  logger.info(s"[UserTokenCleaner] scheduled for ${userTokenCleanerScheduleDate.toString}")
}
