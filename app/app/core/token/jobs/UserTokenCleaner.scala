package core.token.jobs

import java.time.Clock

import javax.inject.Inject
import core.token.model.UserToken
import akka.actor._
import core.token.service.UserTokenService
import core.token.jobs.UserTokenCleaner._
import utils.ClassLogger

import scala.concurrent.ExecutionContext

/**
  * A job which cleans expired [[UserToken]]s.
  *
  * @param service The auth token service implementation.
  * @param clock   The clock implementation.
  * @param ec      The used execution context (note that if this becomes slow / blocking the requests that are incoming
  *                we can swap the execution context for a different one (compared to the one that's being used for request handling)
  *                see:
  *                [[https://www.playframework.com/documentation/2.6.x/ScheduledTasks#Using-a-CustomExecutionContext]])
  */
class UserTokenCleaner @Inject()(service: UserTokenService, clock: Clock)(implicit ec: ExecutionContext)
  extends Actor with ClassLogger {

  /**
    * Process the received messages.
    */
  def receive: Receive = {
    case Clean =>
      val start = clock.instant().toEpochMilli
      val msg = new StringBuffer("\n")
      msg.append("Starting to clean up the user tokens\n")

      service.removeExpiredTokens().map { deleted =>
        val seconds = (clock.instant().toEpochMilli - start) / 1000
        msg.append("Total of %s user tokens(s) were deleted in %s seconds".format(deleted.length, seconds)).append("\n")
        logger.info(msg.toString)
      }
  }
}

/**
  * The companion object.
  */
object UserTokenCleaner {
  case object Clean

  /**
    * The name of the actor.
    */
  final val Name = "user-token-cleaner"
}
