package core.token.model

import java.time.Instant
import java.util.UUID

/**
  * @param id         The id of the token inside the persistence layer
  * @param token      The token itself
  * @param validUntil A moment in time indicating until when this token is valid
  * @param userId     The id of the [[authentication.user.User]] to whom this token belongs
  */
case class UserToken(id: Int, token: String, validUntil: Instant, userId: UUID)
