package authentication.user.dao

import java.util.UUID

import authentication.user.User
import com.mohiva.play.silhouette.api.LoginInfo
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import org.scalatestplus.play.PlaySpec
import play.api.db.slick.DatabaseConfigProvider
import utils.FutureUtils

import scala.concurrent.ExecutionContext

/**
  * This class focuses on testing the behavior of the implementations of
  * the [[UserDAO]]
  */
class UserDAOTest extends PlaySpec with GuiceOneAppPerSuite with FutureUtils {
  implicit val ec: ExecutionContext = app.injector.instanceOf[ExecutionContext]

  val userDAOs: Seq[UserDAO] = Seq(
    new DatabaseUserDAO(app.injector.instanceOf[DatabaseConfigProvider]),
    new InMemoryUserDAO()
  )

  for (userDAO <- userDAOs) {
    "The implementation of the UserDAO (" + userDAO.getClass + ")" must {
      val uuid = UUID.randomUUID()
      val loginInfo = LoginInfo("credentials", "test_db_user_dao_login_info@codekrijger.io")
      val user = User(uuid, loginInfo, "test_first_name", "test_last_name", "test_db_user_dao@codekrijger.io", None, activated = false)

      "return None when a non existing user is requested" in {
        val noUser: Option[User] = await(userDAO.find(loginInfo))

        noUser mustBe None
      }

      "be able to save a new user" in {
        val savedUser: User = await(userDAO.save(user))

        user mustBe savedUser
      }

      "be able to retrieve the new user by LoginInfo" in {
        val maybeUser = await(userDAO.find(loginInfo))

        maybeUser mustBe Some(user)
      }

      "be able to retrieve the new user by it's UUID" in {
        val maybeUser = await(userDAO.find(uuid))

        maybeUser mustBe Some(user)
      }

      "be able to retrieve the new user by it's email" in {
        val maybeUser = await(userDAO.find(user.email))

        maybeUser mustBe Some(user)
      }

      "not be able to retrieve the new user by it's LoginInfo email (given that it differs from the email)" in {
        loginInfo.providerKey must not be user.email
        val maybeUser = await(userDAO.find(loginInfo.providerKey))

        maybeUser mustBe None
      }

      "be able to update a user" in {
        val userToUpdate = await(userDAO.find(uuid)).get
        val newEmail = "new-email@codekrijger.io"
        val newFirstName = "newFirstName"
        val newLastName = "newLastName"
        val newActivated = true

        userToUpdate.email must not be newEmail
        userToUpdate.firstName must not be newFirstName
        userToUpdate.lastName must not be newLastName
        userToUpdate.activated must not be newActivated

        val newUser = userToUpdate.copy(
          email = newEmail,
          firstName = newFirstName,
          lastName = newLastName,
          activated = newActivated
        )

        val updatedUser = await(userDAO.save(newUser))
        val obtainedUpdatedUser = await(userDAO.find(newUser.id)).get

        // The updatedUser should be returned from the save() method
        // (thus matching the result of a find)
        updatedUser mustBe obtainedUpdatedUser

        updatedUser.id mustBe userToUpdate.id
        updatedUser.email mustBe newEmail
        updatedUser.firstName mustBe newFirstName
        updatedUser.lastName mustBe newLastName
        updatedUser.activated mustBe newActivated
        updatedUser.loginInfo mustBe userToUpdate.loginInfo
        updatedUser.avatarURL mustBe userToUpdate.avatarURL
      }
    }
  }
}
