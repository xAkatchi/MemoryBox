package authentication.user.service

import java.util.UUID

import authentication.user.User
import authentication.user.dao.InMemoryUserDAO
import com.mohiva.play.silhouette.api.LoginInfo
import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import utils.FutureUtils

import scala.concurrent.ExecutionContext

/**
  * This class tests the behavior of the [[UserServiceImpl]]
  */
class UserServiceImplTest extends PlaySpec with GuiceOneAppPerSuite with FutureUtils {
  implicit val ec: ExecutionContext = app.injector.instanceOf[ExecutionContext]
  val userServiceImpl = new UserServiceImpl(new InMemoryUserDAO)

  "The UserServiceImpl" must {
    val uuid = UUID.randomUUID()
    val loginInfo = LoginInfo("credentials", "test_user_service_impl_login_info@codekrijger.io")
    val user = User(uuid, loginInfo, "test_first_name", "test_last_name", "test_user_service_impl@codekrijger.io", None, activated = false)

    "be able to save a new user" in {
      val savedUser: User = await(userServiceImpl.save(user))

      user mustBe savedUser
    }

    "be able to retrieve the new user by LoginInfo" in {
      val maybeUser = await(userServiceImpl.retrieve(loginInfo))

      maybeUser mustBe Some(user)
    }

    "be able to retrieve the new user by it's UUID" in {
      val maybeUser = await(userServiceImpl.retrieve(uuid))

      maybeUser mustBe Some(user)
    }

    "be able to retrieve the new user by it's email" in {
      val maybeUser = await(userServiceImpl.retrieve(user.email))

      maybeUser mustBe Some(user)
    }

    "not be able to retrieve a user by it's LoginInfo email (given that it differs from the email)" in {
      loginInfo.providerKey must not be user.email
      val maybeUser = await(userServiceImpl.retrieve(loginInfo.providerKey))

      maybeUser mustBe None
    }
  }
}
