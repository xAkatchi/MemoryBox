package authentication.api

import java.util.concurrent.TimeUnit

import akka.util.Timeout
import authentication.user.dao.{InMemoryUserDAO, UserDAO}
import com.mohiva.play.silhouette.api.LoginInfo
import com.mohiva.play.silhouette.api.services.AvatarService
import com.mohiva.play.silhouette.api.util.PasswordInfo
import com.mohiva.play.silhouette.persistence.daos.{DelegableAuthInfoDAO, InMemoryAuthInfoDAO}
import org.scalatestplus.play.PlaySpec
import org.scalatest.mockito.MockitoSugar
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.Application
import play.api.inject.bind
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.libs.json.{JsValue, Json}
import play.api.mvc.Results
import play.api.test.FakeRequest
import play.api.mvc._
import play.api.test._
import play.api.test.Helpers._
import utils.FutureUtils
import org.mockito.Mockito._

import scala.concurrent.Future

/**
  * This test case focusses on testing the behavior of the [[SignUpController]]
  */
class SignUpControllerTest extends PlaySpec with MockitoSugar with GuiceOneAppPerSuite with Results with FutureUtils {
  val userDAO: UserDAO = app.injector.instanceOf[UserDAO]
  val firstName = "first_name"
  val lastName = "last_name"
  val email = "test_signup@codekrijger.io"
  val password = "LoremIpsum"
  val fakeRequestThatCreatesUser: FakeRequest[JsValue] = buildFakeRequest("{" +
    "\"firstName\": \"" + firstName + "\", " +
    "\"lastName\": \"" + lastName + "\", " +
    "\"email\": \"" + email + "\", " +
    "\"password\": \"" + password + "\"" +
    "}")

  "The `submit` action" must {
    val controller: SignUpController = app.injector.instanceOf[SignUpController]

    "return BadRequest on an empty request" in {
      val futureResult: Future[Result] = controller.submit.apply(buildFakeRequest("{}"))
      val result: Result = await(futureResult)
      val body: JsValue = contentAsJson(futureResult)(Timeout(2, TimeUnit.SECONDS))

      result.header.status mustBe BadRequest.header.status
      body mustBe Json.arr(
        Json.obj("key" -> "firstName", "message" -> "This field is required"),
        Json.obj("key" -> "lastName", "message" -> "This field is required"),
        Json.obj("key" -> "email", "message" -> "This field is required"),
        Json.obj("key" -> "password", "message" -> "This field is required"),
      )
    }

    "return BadRequest when invalid data is given" in {
      val fakeRequest = buildFakeRequest("""{"invalid_key": "some_value"}""")
      val futureResult: Future[Result] = controller.submit.apply(fakeRequest)
      val result: Result = await(futureResult)
      val body: JsValue = contentAsJson(futureResult)(Timeout(2, TimeUnit.SECONDS))

      result.header.status mustBe BadRequest.header.status
      body mustBe Json.arr(
        Json.obj("key" -> "firstName", "message" -> "This field is required"),
        Json.obj("key" -> "lastName", "message" -> "This field is required"),
        Json.obj("key" -> "email", "message" -> "This field is required"),
        Json.obj("key" -> "password", "message" -> "This field is required"),
      )
    }

    "create a new user when valid data is given, which should also return a token" in {
      val futureResult: Future[Result] = controller.submit.apply(fakeRequestThatCreatesUser)
      val result: Result = await(futureResult)
      val body: JsValue = contentAsJson(futureResult)(Timeout(2, TimeUnit.SECONDS))
      val newUser = await(userDAO.find(LoginInfo("credentials", "test_signup@codekrijger.io"))).get

      result.header.status mustBe Ok.header.status
      newUser.firstName mustBe firstName
      newUser.lastName mustBe lastName
      newUser.email mustBe email
      body.toString().matches("\\{\"token\":\".*\"\\}") mustBe true
    }

    "show an error when the user already exists" in {
      val futureResult: Future[Result] = controller.submit.apply(fakeRequestThatCreatesUser)
      val result: Result = await(futureResult)
      val body: JsValue = contentAsJson(futureResult)(Timeout(2, TimeUnit.SECONDS))

      result.header.status mustBe BadRequest.header.status
      body mustBe Json.obj("message" -> "This user already exists")
    }
  }

  /**
    * A simple convenience wrapper that creates a [[FakeRequest]].
    *
    * @param string The string which should be used as json body of the [[FakeRequest]]
    * @return
    */
  def buildFakeRequest(string: String): FakeRequest[JsValue] =
    FakeRequest(POST, "/register", FakeHeaders(), Json.parse(string))

  /**
    * We override the parent here because we'd like to load some custom dependencies
    * instead of the 'default' ones.
    *
    * @return
    */
  override def fakeApplication(): Application = {
    val avatarService = mock[AvatarService]
    when(avatarService.retrieveURL("test_signup@codekrijger.io")) thenReturn Future.successful(None)

    new GuiceApplicationBuilder()
      .overrides(bind[DelegableAuthInfoDAO[PasswordInfo]].to(new InMemoryAuthInfoDAO[PasswordInfo]))
      .overrides(bind[AvatarService].to(avatarService))
      .build()
  }
}
