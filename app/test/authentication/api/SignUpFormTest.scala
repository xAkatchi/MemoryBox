package authentication.api

import java.util.concurrent.TimeUnit

import akka.util.Timeout
import authentication.api.SignUpForm.{Data, form}
import org.scalatestplus.play.PlaySpec
import play.api.data.Form
import play.api.i18n.Messages
import play.api.libs.json.Json
import play.api.mvc.{AnyContentAsFormUrlEncoded, Result, Results}
import play.api.test.{FakeRequest, Helpers}
import play.api.test.Helpers.{POST, contentAsJson, contentAsString}

import scala.concurrent.Future
import scala.util.Random

/**
  * This test case tests the behavior of the [[SignUpForm]]
  */
class SignUpFormTest extends PlaySpec with Results {

  "The SignInForm" must {
    implicit val messages: Messages = Helpers.stubMessages()

    "be invalid when a non email string is given inside the email field" in {
      implicit val request: FakeRequest[AnyContentAsFormUrlEncoded] =
        buildFakeRequest("some_non_email_string", "password", "first_name", "last_name")

      val result = Future.successful(form.bindFromRequest().fold(errorFunc, successFunc))
      val body = contentAsJson(result)(Timeout(2, TimeUnit.SECONDS))

      body mustBe Json.obj("email" -> Json.arr("error.email"))
    }

    "be invalid when the email field is missing" in {
      implicit val request: FakeRequest[AnyContentAsFormUrlEncoded] =
        buildFakeRequest("", "password", "first_name", "last_name")

      val result = Future.successful(form.bindFromRequest().fold(errorFunc, successFunc))
      val body = contentAsJson(result)(Timeout(2, TimeUnit.SECONDS))

      body mustBe Json.obj("email" -> Json.arr("error.email"))
    }

    "be invalid when an empty password is given" in {
      implicit val request: FakeRequest[AnyContentAsFormUrlEncoded] =
        buildFakeRequest("test_sign_up_form@codekrijger.io", "", "first_name", "last_name")

      val result = Future.successful(form.bindFromRequest().fold(errorFunc, successFunc))
      val body = contentAsJson(result)(Timeout(2, TimeUnit.SECONDS))

      body mustBe Json.obj("password" -> Json.arr("error.minLength", "error.required"))
    }

    "be invalid when an empty firstName is given" in {
      implicit val request: FakeRequest[AnyContentAsFormUrlEncoded] =
        buildFakeRequest("test_sign_up_form@codekrijger.io", "password", "", "last_name")

      val result = Future.successful(form.bindFromRequest().fold(errorFunc, successFunc))
      val body = contentAsJson(result)(Timeout(2, TimeUnit.SECONDS))

      body mustBe Json.obj("firstName" -> Json.arr("error.required"))
    }

    "be invalid when an empty lastName is given" in {
      implicit val request: FakeRequest[AnyContentAsFormUrlEncoded] =
        buildFakeRequest("test_sign_up_form@codekrijger.io", "password", "first_name", "")

      val result = Future.successful(form.bindFromRequest().fold(errorFunc, successFunc))
      val body = contentAsJson(result)(Timeout(2, TimeUnit.SECONDS))

      body mustBe Json.obj("lastName" -> Json.arr("error.required"))
    }

    "be invalid when all the fields are missing" in {
      implicit val request: FakeRequest[AnyContentAsFormUrlEncoded] =
        buildFakeRequest("", "", "", "")

      val result = Future.successful(form.bindFromRequest().fold(errorFunc, successFunc))
      val body = contentAsJson(result)(Timeout(2, TimeUnit.SECONDS))

      body mustBe Json.obj(
        "password" -> Json.arr("error.minLength", "error.required"),
        "email" -> Json.arr("error.email"),
        "firstName" -> Json.arr("error.required"),
        "lastName" -> Json.arr("error.required"),
      )
    }

    "be invalid when the password is not long enough" in {
      implicit val request: FakeRequest[AnyContentAsFormUrlEncoded] =
        buildFakeRequest("test_sign_up_form@codekrijger.io", "pwd", "first_name", "last_name")

      val result = Future.successful(form.bindFromRequest().fold(errorFunc, successFunc))
      val body = contentAsJson(result)(Timeout(2, TimeUnit.SECONDS))

      body mustBe Json.obj("password" -> Json.arr("error.minLength"))
    }

    "be invalid when the password is too long" in {
      implicit val request: FakeRequest[AnyContentAsFormUrlEncoded] =
        buildFakeRequest("test_sign_up_form@codekrijger.io", Random.alphanumeric.take(150).mkString, "first_name", "last_name")

      val result = Future.successful(form.bindFromRequest().fold(errorFunc, successFunc))
      val body = contentAsJson(result)(Timeout(2, TimeUnit.SECONDS))

      body mustBe Json.obj("password" -> Json.arr("error.maxLength"))
    }

    "be ok when all the fields are present and valid" in {
      implicit val request: FakeRequest[AnyContentAsFormUrlEncoded] =
        buildFakeRequest("test_sign_up_form@codekrijger.io", "password", "first_name", "last_name")

      val result = Future.successful(form.bindFromRequest().fold(errorFunc, successFunc))
      val body = contentAsString(result)(Timeout(2, TimeUnit.SECONDS))

      body mustBe ""
    }
  }

  /**
    * A simple convenience wrapper that creates a [[FakeRequest]] with the given details.
    *
    * @param email     The email which should be set in the Form
    * @param password  The password which should be set in the Form
    * @param firstName The firstName which should be set in the Form
    * @param lastName  The lastName which should be set in the Form
    * @return
    */
  def buildFakeRequest(email: String, password: String, firstName: String, lastName: String): FakeRequest[AnyContentAsFormUrlEncoded] =
    FakeRequest(POST, "/").withFormUrlEncodedBody(
      "email" -> email,
      "password" -> password,
      "firstName" -> firstName,
      "lastName" -> lastName
    )

  /**
    * The function which will be invoked whenever a form failed it's validation
    *
    * @param badForm  The form
    * @param messages The implicit messages which are used to translate the error messages
    * @return
    */
  def errorFunc(badForm: Form[Data])(implicit messages: Messages): Result =
    BadRequest(badForm.errorsAsJson)

  /**
    * The function which will be invoked whenever a form succeeded it's validation
    *
    * @param data The data from the form
    * @return
    */
  def successFunc(data: Data): Result =
    Redirect("/").flashing("success" -> "success form!")

}
