package authentication.api

import java.util.concurrent.TimeUnit

import akka.util.Timeout
import authentication.api.SignInForm.Data
import org.scalatestplus.play.PlaySpec
import play.api.data.Form
import play.api.i18n.Messages
import play.api.libs.json.Json
import play.api.mvc.{AnyContentAsFormUrlEncoded, Result, Results}
import play.api.test.{FakeRequest, Helpers}
import play.api.test.Helpers.{POST, contentAsJson, contentAsString}
import SignInForm._

import scala.concurrent.Future

/**
  * This test case tests the behavior of the [[SignInForm]]
  */
class SignInFormTest extends PlaySpec with Results {

  "The SignInForm" must {
    implicit val messages: Messages = Helpers.stubMessages()

    "be invalid when a non email string is given inside the email field" in {
      implicit val request: FakeRequest[AnyContentAsFormUrlEncoded] =
        buildFakeRequest("some_non_email_string", "password")

      val result = Future.successful(form.bindFromRequest().fold(errorFunc, successFunc))
      val body = contentAsJson(result)(Timeout(2, TimeUnit.SECONDS))

      body mustBe Json.obj("email" -> Json.arr("error.email"))
    }

    "be invalid when the email field is missing" in {
      implicit val request: FakeRequest[AnyContentAsFormUrlEncoded] =
        buildFakeRequest("", "password")

      val result = Future.successful(form.bindFromRequest().fold(errorFunc, successFunc))
      val body = contentAsJson(result)(Timeout(2, TimeUnit.SECONDS))

      body mustBe Json.obj("email" -> Json.arr("error.email"))
    }

    "be invalid when an empty password is given" in {
      implicit val request: FakeRequest[AnyContentAsFormUrlEncoded] =
        buildFakeRequest("test_sign_in_form@codekrijger.io", "")

      val result = Future.successful(form.bindFromRequest().fold(errorFunc, successFunc))
      val body = contentAsJson(result)(Timeout(2, TimeUnit.SECONDS))

      body mustBe Json.obj("password" -> Json.arr("error.required"))
    }

    "be invalid when all the fields are missing" in {
      implicit val request: FakeRequest[AnyContentAsFormUrlEncoded] =
        buildFakeRequest("", "")

      val result = Future.successful(form.bindFromRequest().fold(errorFunc, successFunc))
      val body = contentAsJson(result)(Timeout(2, TimeUnit.SECONDS))

      body mustBe Json.obj(
        "password" -> Json.arr("error.required"),
        "email" -> Json.arr("error.email")
      )
    }

    "be ok when all the fields are present and valid" in {
      implicit val request: FakeRequest[AnyContentAsFormUrlEncoded] =
        buildFakeRequest("test_sign_in_form@codekrijger.io", "password")

      val result = Future.successful(form.bindFromRequest().fold(errorFunc, successFunc))
      val body = contentAsString(result)(Timeout(2, TimeUnit.SECONDS))

      body mustBe ""
    }
  }

  /**
    * A simple convenience wrapper that creates a [[FakeRequest]] with the given details.
    *
    * @param email    The email which should be set in the Form
    * @param password The password which should be set in the Form
    * @return
    */
  def buildFakeRequest(email: String, password: String): FakeRequest[AnyContentAsFormUrlEncoded] =
    FakeRequest(POST, "/").withFormUrlEncodedBody(
      "email" -> email,
      "password" -> password
    )

  /**
    * The function which will be invoked whenever a form failed it's validation
    *
    * @param badForm  The form
    * @param messages The implicit messages which are used to translate the error messages
    * @return
    */
  def errorFunc(badForm: Form[Data])(implicit messages: Messages): Result =
    BadRequest(badForm.errorsAsJson)

  /**
    * The function which will be invoked whenever a form succeeded it's validation
    *
    * @param data The data from the form
    * @return
    */
  def successFunc(data: Data): Result =
    Redirect("/").flashing("success" -> "success form!")
}