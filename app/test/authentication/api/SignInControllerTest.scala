package authentication.api

import java.util.UUID
import java.util.concurrent.TimeUnit

import akka.util.Timeout
import authentication.authInfo.dao.PasswordInfoDAO
import authentication.user.User
import authentication.user.dao.UserDAO
import com.mohiva.play.silhouette.api.LoginInfo
import com.mohiva.play.silhouette.api.util.PasswordInfo
import com.mohiva.play.silhouette.persistence.daos.{DelegableAuthInfoDAO, InMemoryAuthInfoDAO}
import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.Application
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.{JsValue, Json}
import play.api.mvc.{Result, Results}
import play.api.test.Helpers.{POST, contentAsJson}
import play.api.test.{FakeHeaders, FakeRequest}
import utils.FutureUtils

import scala.concurrent.Future

/**
  * This test case focuses on testing the behavior of the [[SignInController]]
  */
class SignInControllerTest extends PlaySpec with GuiceOneAppPerSuite with Results with FutureUtils {
  val userDAO: UserDAO = app.injector.instanceOf[UserDAO]
  val passwordInfoDAO: PasswordInfoDAO = app.injector.instanceOf[PasswordInfoDAO]

  "The `submit` action" must {
    val controller: SignInController = app.injector.instanceOf[SignInController]

    "return BadRequest on an empty request" in {
      val futureResult: Future[Result] = controller.submit.apply(buildFakeRequest("{}"))
      val result: Result = await(futureResult)
      val body: JsValue = contentAsJson(futureResult)(Timeout(2, TimeUnit.SECONDS))

      result.header.status mustBe BadRequest.header.status
      body mustBe Json.obj("message" -> "The given data is invalid")
    }

    "return BadRequest when invalid data is given" in {
      val fakeRequest = buildFakeRequest("""{"invalid_key": "some_value"}""")
      val futureResult: Future[Result] = controller.submit.apply(fakeRequest)
      val result: Result = await(futureResult)
      val body: JsValue = contentAsJson(futureResult)(Timeout(2, TimeUnit.SECONDS))

      result.header.status mustBe BadRequest.header.status
      body mustBe Json.obj("message" -> "The given data is invalid")
    }

    "return BadRequest when a non existing user is given" in {
      val fakeRequest = buildFakeRequest(
        """{"email": "test_sign_in_non_existing_user@codekrijger.io", "password": "LoremIpsum"}"""
      )
      val futureResult: Future[Result] = controller.submit.apply(fakeRequest)
      val result: Result = await(futureResult)
      val body: JsValue = contentAsJson(futureResult)(Timeout(2, TimeUnit.SECONDS))

      result.header.status mustBe BadRequest.header.status
      body mustBe Json.obj("message" -> "The given credentials are invalid")
    }

    "return BadRequest when a non activated user tries to login (with a note that they must activate their account)" in {
      val loginInfo = LoginInfo("credentials", "test_sign_in@codekrijger.io")
      val user = User(
        UUID.randomUUID(), loginInfo, "test_first_name", "test_last_name",
        "test_sign_in@codekrijger.io", None, activated = false
      )
      val authInfo = PasswordInfo(
        // This is a BCrypted password where the password is: 'loremipsum'
        "bcrypt-sha256", "$2a$10$SjudWH0szf5BwbyjUoLRL.ke8tQ44OoSf1Wmr2Rf5iX7XfO8niwPW", None
      )

      // We need to create the user before we're able to test the authentication
      await(userDAO.save(user))
      await(passwordInfoDAO.save(loginInfo, authInfo))

      val fakeRequest = buildFakeRequest(
        """{"email": "test_sign_in@codekrijger.io", "password": "loremipsum"}"""
      )
      val futureResult: Future[Result] = controller.submit.apply(fakeRequest)
      val result: Result = await(futureResult)
      val body: JsValue = contentAsJson(futureResult)(Timeout(2, TimeUnit.SECONDS))

      result.header.status mustBe BadRequest.header.status
      body mustBe Json.obj("message" -> "Please activate your account before continuing")
    }

    "return a token when an activated user tries to login" in {
      val loginInfo = LoginInfo("credentials", "test_sign_in_successful@codekrijger.io")
      val user = User(
        UUID.randomUUID(), loginInfo, "test_first_name", "test_last_name",
        "test_sign_in_successful@codekrijger.io", None, activated = true
      )
      val authInfo = PasswordInfo(
        // This is a BCrypted password where the password is: 'loremipsum'
        "bcrypt-sha256", "$2a$10$SjudWH0szf5BwbyjUoLRL.ke8tQ44OoSf1Wmr2Rf5iX7XfO8niwPW", None
      )

      // We need to create the user before we're able to test the authentication
      await(userDAO.save(user))
      await(passwordInfoDAO.save(loginInfo, authInfo))

      val fakeRequest = buildFakeRequest(
        """{"email": "test_sign_in_successful@codekrijger.io", "password": "loremipsum"}"""
      )
      val futureResult: Future[Result] = controller.submit.apply(fakeRequest)
      val result: Result = await(futureResult)
      val body: JsValue = contentAsJson(futureResult)(Timeout(2, TimeUnit.SECONDS))

      result.header.status mustBe Ok.header.status
      body.toString().matches("\\{\"token\":\".*\"\\}") mustBe true
    }
  }

  /**
    * A simple convenience wrapper that creates a [[FakeRequest]].
    *
    * @param string The string which should be used as json body of the [[FakeRequest]]
    * @return
    */
  def buildFakeRequest(string: String): FakeRequest[JsValue] =
    FakeRequest(POST, "/login", FakeHeaders(), Json.parse(string))

  /**
    * We override the parent here because we'd like to load some custom dependencies
    * instead of the 'default' ones.
    *
    * @return
    */
  override def fakeApplication(): Application = {
    new GuiceApplicationBuilder()
      .overrides(bind[DelegableAuthInfoDAO[PasswordInfo]].to(new InMemoryAuthInfoDAO[PasswordInfo]))
      .build()
  }
}
