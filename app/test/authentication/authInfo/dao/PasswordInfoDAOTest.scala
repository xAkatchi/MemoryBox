package authentication.authInfo.dao

import java.util.UUID

import authentication.user.User
import authentication.user.dao.UserDAO
import com.mohiva.play.silhouette.api.LoginInfo
import com.mohiva.play.silhouette.api.util.PasswordInfo
import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import utils.FutureUtils

/**
  * This class focuses on testing the behavior of the [[PasswordInfoDAO]]
  */
class PasswordInfoDAOTest extends PlaySpec with GuiceOneAppPerSuite with FutureUtils {
  val passwordInfoDAO: PasswordInfoDAO = app.injector.instanceOf[PasswordInfoDAO]
  val userDAO: UserDAO = app.injector.instanceOf[UserDAO]

  "PasswordInfoDAO" must {
    val loginInfo = LoginInfo("credentials", "test_password_info_dao@codekrijger.io")
    val user = User(
      UUID.randomUUID(), loginInfo, "test_first_name", "test_last_name",
      "test_password_info_dao@codekrijger.io", None, activated = false
    )
    val authInfo = PasswordInfo("test_hasher", "test_password", Some("test_salt"))
    val authInfo2 = PasswordInfo("test_hasher2", "test_password2", Some("test_salt2"))

    // We need to save the user so that the login info get's stored (which is a side effect from
    // saving the user)
    await(userDAO.save(user))

    "not be able to find PasswordInfo for the given LoginInfo, since it's not yet added" in {
      val maybePasswordInfo = await(passwordInfoDAO.find(loginInfo))

      maybePasswordInfo mustBe None
    }

    "be able to add new PasswordInfo" in {
      val addedAuthInfo = await(passwordInfoDAO.add(loginInfo, authInfo))

      addedAuthInfo mustBe authInfo
    }

    "be able to find the newly added PasswordInfo" in {
      val maybePasswordInfo = await(passwordInfoDAO.find(loginInfo))

      maybePasswordInfo mustBe Some(authInfo)
    }

    "be able to update the PasswordInfo" in {
      val updatedPasswordInfo = await(passwordInfoDAO.update(loginInfo, authInfo2))

      updatedPasswordInfo mustBe authInfo2
    }

    "be able to find the updated PasswordInfo" in {
      val maybePasswordInfo = await(passwordInfoDAO.find(loginInfo))

      maybePasswordInfo mustBe Some(authInfo2)
    }

    "be able to remove the PasswordInfo for the given LoginInfo" in {
      await(passwordInfoDAO.remove(loginInfo))
      val maybePasswordInfo = await(passwordInfoDAO.find(loginInfo))

      maybePasswordInfo mustBe None
    }

    "be able to add new PasswordInfo through save" in {
      val newPasswordInfo = await(passwordInfoDAO.save(loginInfo, authInfo))
      val foundPasswordInfo = await(passwordInfoDAO.find(loginInfo))

      newPasswordInfo mustBe authInfo
      foundPasswordInfo mustBe Some(authInfo)
    }

    "be able to update new PasswordInfo through save" in {
      val updatedPasswordInfo = await(passwordInfoDAO.save(loginInfo, authInfo2))
      val foundPasswordInfo = await(passwordInfoDAO.find(loginInfo))

      updatedPasswordInfo mustBe authInfo2
      foundPasswordInfo mustBe Some(authInfo2)
    }
  }
}
