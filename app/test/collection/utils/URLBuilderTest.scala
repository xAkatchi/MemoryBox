package collection.utils

import java.util.Date

import collection.models.Collection
import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.Configuration

/**
  * This test tests the working of the [[URLBuilder]]
  */
class URLBuilderTest extends PlaySpec with GuiceOneAppPerSuite {
  "The URLBuilder" must {
    val builder = app.injector.instanceOf[URLBuilder]
    val config = app.injector.instanceOf[Configuration]
    val collection = Collection("slug", "name", None, new Date())

    "create a valid detail url for the given collection" in {
      val url = builder.buildCollectionDetailUrl(collection)

      url mustBe s"${config.get[String]("ui.base-url")}dashboard/collection/${collection.slug}/photos"
    }
  }

}
