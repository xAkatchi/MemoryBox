package collection.utils

import java.io.File

import org.scalatestplus.play.PlaySpec

/**
  * This method tests the behavior of the [[FileHasher]]
  */
class FileHasherTest extends PlaySpec {
  "The FileHasher" must {
    val hasher: FileHasher = new FileHasher
    val tempFile = File.createTempFile("test-file", "png")
    tempFile.deleteOnExit()

    "calculate an MD5 hash on the given file" in {
      // See: https://stackoverflow.com/a/1896748
      hasher.computeMD5Hash(tempFile).matches("^[a-fA-F0-9]{32}$") mustBe true
    }

    "calculate an identical hash on multiple invocations" in {
      val hashOne = hasher.computeMD5Hash(tempFile)
      val hashTwo = hasher.computeMD5Hash(tempFile)

      hashOne mustBe hashTwo
    }
  }
}
