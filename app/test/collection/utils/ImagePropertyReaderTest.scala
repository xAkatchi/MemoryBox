package collection.utils

import java.awt.Dimension
import java.io.File

import org.scalatestplus.play.PlaySpec

/**
  * This test tests the behavior of the [[ImagePropertyReader]]
  */
class ImagePropertyReaderTest extends PlaySpec {
  "The ImagePropertyReader" must {
    val reader = new ImagePropertyReader
    val image = new File("test/resources/test_image.png")
    val textFile = new File("test/resources/test_textfile.txt")
    val imageWidth = 150
    val imageHeight = 200

    "return the correct dimensions from the given file" in {
      val imageDimension = reader.getImageDimensions(image)

      imageDimension mustBe Some(new Dimension(imageWidth, imageHeight))
    }

    "return null in the case of a file that's not an image" in {
      val textFileDimension = reader.getImageDimensions(textFile)

      textFileDimension mustBe None
    }
  }
}
