package collection.utils

import java.io.File
import java.nio.file.Paths

import org.apache.commons.io.FileUtils
import org.scalatest.BeforeAndAfterAll
import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.libs.Files
import play.api.mvc.MultipartFormData
import play.api.libs.Files.{SingletonTemporaryFileCreator, TemporaryFile}

/**
  * This unit test, tests the behavior of the [[FilePartMover]]
  */
class FilePartMoverTest extends PlaySpec with GuiceOneAppPerSuite with BeforeAndAfterAll {
  val mover: FilePartMover = app.injector.instanceOf[FilePartMover]
  val oldFolder = "file_part_mover_test_old_tmp"
  val oldFilePath1 = s"$oldFolder/test_image_tmp_1.png"
  val oldFilePath2 = s"$oldFolder/test_image_tmp_2.png"
  val oldFilePath3 = s"$oldFolder/test_image_tmp_3.png"
  val newFolder = "file_part_mover_test_new_tmp"
  val newFilePath1 = s"$newFolder/moved_file_1.png"
  val newFilePath2 = s"$newFolder/moved_file_2.png"

  "The FilePartMover" must {
    "successfully move the given file to the given directory (whilst also creating the directory)" in {
      val filePart = getFilePart(new File(oldFilePath1))

      java.nio.file.Files.exists(Paths.get(oldFilePath1)) mustBe true
      java.nio.file.Files.exists(Paths.get(newFilePath1)) mustBe false
      java.nio.file.Files.exists(Paths.get(newFolder)) mustBe false

      val movedFile: File = mover.moveFile(filePart, newFilePath1).get

      java.nio.file.Files.exists(Paths.get(oldFilePath1)) mustBe false
      java.nio.file.Files.exists(Paths.get(newFilePath1)) mustBe true
      java.nio.file.Files.exists(Paths.get(newFolder)) mustBe true

      Paths.get(newFilePath1).toString mustBe movedFile.getPath
    }

    "successfully move the given file to the given directory, even if the directory already exists" in {
      val filePart = getFilePart(new File(oldFilePath2))

      java.nio.file.Files.exists(Paths.get(oldFilePath2)) mustBe true
      java.nio.file.Files.exists(Paths.get(newFilePath2)) mustBe false
      java.nio.file.Files.exists(Paths.get(newFolder)) mustBe true

      val movedFile: File = mover.moveFile(filePart, newFilePath2).get

      java.nio.file.Files.exists(Paths.get(oldFilePath2)) mustBe false
      java.nio.file.Files.exists(Paths.get(newFilePath2)) mustBe true
      java.nio.file.Files.exists(Paths.get(newFolder)) mustBe true

      Paths.get(newFilePath2).toString mustBe movedFile.getPath
    }

    "return None when we give a filepath that already exists" in {
      val filePart = getFilePart(new File(oldFilePath3))

      java.nio.file.Files.exists(Paths.get(oldFilePath3)) mustBe true
      java.nio.file.Files.exists(Paths.get(newFilePath1)) mustBe true

      mover.moveFile(filePart, newFilePath1) mustBe None
    }
  }

  /**
    * Based off: [[https://stackoverflow.com/a/13412641]]
    *
    * @param fileToMove The file that should be encapsulated inside the returned FilePart
    * @return The [[MultipartFormData.FilePart]] that should be moved
    */
  private def getFilePart(fileToMove: File): MultipartFormData.FilePart[Files.TemporaryFile] = {
    val tempFile: TemporaryFile = SingletonTemporaryFileCreator.create(fileToMove.toPath)

    MultipartFormData.FilePart[Files.TemporaryFile](
      "key",
      "test_image_tmp.png",
      Some("image/png"),
      tempFile
    )
  }

  /**
    * We need to make copies from the 'original' file, this because the mover moves the files
    * which will cause issues for other test cases that are using the 'original' file.
    */
  override protected def beforeAll(): Unit = {
    val from: File = new File("test/resources/test_image.png")
    val to1: File = new File(oldFilePath1)
    val to2: File = new File(oldFilePath2)
    val to3: File = new File(oldFilePath3)

    // Make the old folder directory since it shouldn't exist
    new File(oldFolder).mkdir()

    // See: https://stackoverflow.com/a/30327959
    java.nio.file.Files.copy(from.toPath, to1.toPath).toString
    java.nio.file.Files.copy(from.toPath, to2.toPath).toString
    java.nio.file.Files.copy(from.toPath, to3.toPath).toString

    super.beforeAll()
  }

  /**
    * We need to clean up after our selves (once the test is finished),
    * thus we'll remove the newly created directories
    */
  override protected def afterAll(): Unit = {
    FileUtils.deleteDirectory(new File(oldFolder))
    FileUtils.deleteDirectory(new File(newFolder))

    super.afterAll()
  }
}
