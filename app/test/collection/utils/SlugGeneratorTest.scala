package collection.utils

import org.scalatestplus.play.PlaySpec

/**
  * This method tests the working of the [[SlugGenerator]]
  */
class SlugGeneratorTest extends PlaySpec {
  "The SlugGenerator" must {
    val generator: SlugGenerator = new SlugGenerator

    "return a random UUID whenever the generate method is invoked" in {
      val slug = generator.generate

      slug.matches("[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}") mustBe true
    }
  }
}
