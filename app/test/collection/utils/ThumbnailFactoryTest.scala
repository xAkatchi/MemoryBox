package collection.utils

import java.io.File
import java.util.{Date, UUID}

import authentication.user.User
import collection.models._
import com.sksamuel.scrimage.Image
import org.scalatest.mockito.MockitoSugar
import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import org.mockito.Mockito._
import org.scalatest.BeforeAndAfterAll

/**
  * This class focuses on testing the behavior of the [[ThumbnailFactory]]
  */
class ThumbnailFactoryTest extends PlaySpec with GuiceOneAppPerSuite with MockitoSugar {
  val factory: ThumbnailFactory = app.injector.instanceOf[ThumbnailFactory]

  "The ThumbnailFactory" must {
    val user = mock[User]
    when(user.id) thenReturn UUID.randomUUID()

    "Not create the thumbnail for items not stored on the file system" in {
      val item = buildCollectionItem(mock[Storage], buildPhotoDetails(), user)
      val result = factory.createThumbnail(item, 300)

      result mustBe None
    }

    "Not create the thumbnail for items that are not photos" in {
      val item = buildCollectionItem(buildFileSystemStorage(), mock[FileDetails], user)
      val result = factory.createThumbnail(item, 300)

      result mustBe None
    }

    "Create the thumbnail for photos stored on the file system" in {
      val item = buildCollectionItem(buildFileSystemStorage(), buildPhotoDetails(), user)
      val result = factory.createThumbnail(item, 300)

      result.isDefined mustBe true

      val unpackedResult = result.get
      val file = new File(unpackedResult.fileSystemStorage.filePath)
      val image = Image.fromFile(file)

      unpackedResult.collectionItem mustBe item
      unpackedResult.photoDetails.contentType.startsWith("image/") mustBe true
      unpackedResult.photoDetails.height mustBe 300
      Image.fromFile(file).height mustBe 300

      file.exists() mustBe true
      // Also delete the thumbnail on successful test so that we leave no 'littering' behind
      file.delete() mustBe true
    }
  }

  private def buildCollectionItem(storage: Storage, fileDetails: FileDetails, user: User): CollectionItem =
    CollectionItem(UUID.randomUUID(), mock[Collection], "name", storage, fileDetails, new Date(), user)

  private def buildPhotoDetails(): PhotoDetails =
    PhotoDetails(500, 500, 1000, "some-hash", "image/jpeg")

  private def buildFileSystemStorage(): FileSystemStorage =
    FileSystemStorage("test/resources/test_image.png")
}
