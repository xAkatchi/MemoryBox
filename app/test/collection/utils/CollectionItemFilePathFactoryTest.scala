package collection.utils

import java.io.File
import java.util.UUID

import authentication.user.User
import com.mohiva.play.silhouette.api.LoginInfo
import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.Configuration
import play.api.libs.Files
import play.api.libs.Files.SingletonTemporaryFileCreator
import play.api.mvc.MultipartFormData

/**
  * This unit test, tests the behavior of the [[CollectionItemFilePathFactory]]
  */
class CollectionItemFilePathFactoryTest extends PlaySpec with GuiceOneAppPerSuite {
  val factory: CollectionItemFilePathFactory = app.injector.instanceOf[CollectionItemFilePathFactory]
  val config: Configuration = app.injector.instanceOf[Configuration]

  "The CollectionItemFilePathFactory" must {
    val user = User(UUID.randomUUID(), LoginInfo("id", "key"), "fname", "lname", "email", None, activated = true)
    val file = new File("test/resources/test_image_tmp.png")
    val multipartFormFile = MultipartFormData.FilePart[Files.TemporaryFile](
      "key",
      file.getName,
      Some("image/png"),
      SingletonTemporaryFileCreator.create(file.toPath)
    )

    "create an unique file path" in {
      val filePath1 = factory.getUploadPath(multipartFormFile, user)
      val filePath2 = factory.getUploadPath(multipartFormFile, user)

      filePath1 must not be filePath2
    }

    "create an unique file path for storing thumbnails" in {
      val filePath1 = factory.getThumbnailUploadPath(file, user)
      val filePath2 = factory.getThumbnailUploadPath(file, user)

      filePath1 must not be filePath2
    }
  }
}
