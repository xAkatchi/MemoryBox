package collection.api

import java.util.UUID
import java.util.concurrent.TimeUnit

import akka.util.Timeout
import authentication.user.User
import authentication.utils.JwtEnv
import com.mohiva.play.silhouette.api.{Environment, LoginInfo, Silhouette, SilhouetteProvider}
import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.mvc.{Result, Results}
import utils.FutureUtils
import com.mohiva.play.silhouette.test._
import org.scalatest.mockito.MockitoSugar
import play.api.Application
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.{JsValue, Json}
import play.api.test.{FakeHeaders, FakeRequest}
import play.test.WithApplication
import play.api.test.Helpers.{POST, contentAsJson}
import play.api.test._

import scala.concurrent.{ExecutionContext, Future}

/**
  * This test case focuses on testing the behavior of the [[CreateCollectionController]]
  *
  * @note this whole test is currently commented out due to that there are some issues
  *       with unit testing the authenticated flow.
  *       There is a question currently awaiting answers over here: http://discourse.silhouette.rocks/t/issue-whilst-unit-testing-the-authenticated-flow-of-the-securedaction/252/1
  *       Once we have an answer, we can re-enable this unit test.
  *       The unit test is commented out since all the business logic which should be tested is already present
  *       and it would be a waste to re-do that.
  */
class CreateCollectionControllerTest extends PlaySpec with GuiceOneAppPerSuite with Results with FutureUtils with MockitoSugar {
  implicit val ec: ExecutionContext = app.injector.instanceOf[ExecutionContext]

  val fakeHeaders: FakeHeaders = FakeHeaders(Seq(
    // We need to set the json headers, otherwise we'll get a HTML response (application/text)
    // which causes issues when comparing the body
    "Accept" -> "application/json; charset=utf-8",
    "Content-Type" -> "application/json; charset=utf-8",
  ))
  val identity = User(UUID.randomUUID(), LoginInfo("credentials", "create_collection_test@codekrijger.io"),
    "first_name", "last_name", "email", None, activated = true)
  implicit val env: FakeEnvironment[JwtEnv] = new FakeEnvironment[JwtEnv](Seq(identity.loginInfo -> identity))

//  "The `submit` action" must {
//    val controller: CreateCollectionController = app.injector.instanceOf[CreateCollectionController]
//
//    "return Unauthorized when no authenticated user is present inside the request" in {
//      val fakeRequest: FakeRequest[JsValue] = FakeRequest(
//        POST, "", fakeHeaders, Json.parse("{}")
//      )
//
//      val futureResult: Future[Result] = controller.submit(fakeRequest)
//      val result: Result = await(futureResult)
//      val body: JsValue = contentAsJson(futureResult)(Timeout(2, TimeUnit.SECONDS))
//
//      result.header.status mustBe Unauthorized.header.status
//      body mustBe Json.obj("success" -> false, "message" -> "Authentication required")
//    }

//    "return BadRequest if valid credentials are given, but no valid formdata is passed" in {
//      val fakeRequest: FakeRequest[JsValue] = FakeRequest(
//        POST, "/", fakeHeaders, Json.parse("{}")
//      ).withAuthenticator[JwtEnv](identity.loginInfo)
//
//      val futureResult: Future[Result] = controller.submit(fakeRequest)
//      val result: Result = await(futureResult)
//      val body: JsValue = contentAsJson(futureResult)(Timeout(2, TimeUnit.SECONDS))
//
//      result.header.status mustBe BadRequest.header.status
//      body mustBe Json.obj("message" -> "The given data is invalid")
//    }

//    "should not work on non-POST requests" in {
//
//    }
//
//    "return a BadRequest if the form validation failed" in {
//
//    }
//
//    "return the created Collection if such a collection could be created" in {
//      // TODO should include the current user
//    }
//  }

  override def fakeApplication(): Application = {
    new GuiceApplicationBuilder()
      .overrides(bind[Environment[JwtEnv]].to(env))
      .build()
  }
}
