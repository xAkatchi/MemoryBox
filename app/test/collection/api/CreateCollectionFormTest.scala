package collection.api

import java.util.concurrent.TimeUnit

import akka.util.Timeout
import org.scalatestplus.play.PlaySpec
import play.api.data.Form
import play.api.i18n.Messages
import play.api.libs.json.Json
import play.api.mvc.{AnyContentAsFormUrlEncoded, Result, Results}
import play.api.test.{FakeRequest, Helpers}
import play.api.test.Helpers.{POST, contentAsJson, contentAsString}
import CreateCollectionForm._

import scala.concurrent.Future
import scala.util.Random

/**
  * This unit test tests the working of the [[CreateCollectionForm]]
  */
class CreateCollectionFormTest extends PlaySpec with Results {

  "The CreateCollectionForm" must {
    implicit val messages: Messages = Helpers.stubMessages()

    "be invalid when the name is missing/empty" in {
      implicit val emptyNameRequest: FakeRequest[AnyContentAsFormUrlEncoded] =
        buildFakeRequest("", None)

      val result = Future.successful(form.bindFromRequest().fold(errorFunc, successFunc))
      val body = contentAsJson(result)(Timeout(2, TimeUnit.SECONDS))

      body mustBe Json.obj("name" -> Json.arr("error.minLength", "error.required"))
    }

    "be invalid when the given name is longer then 64 characters" in {
      implicit val okRequest: FakeRequest[AnyContentAsFormUrlEncoded] =
        buildFakeRequest(Random.alphanumeric.take(65).mkString, None)

      val result = Future.successful(form.bindFromRequest().fold(errorFunc, successFunc))
      val body = contentAsJson(result)(Timeout(2, TimeUnit.SECONDS))

      body mustBe Json.obj("name" -> Json.arr("error.maxLength"))
    }

    "be ok when we omit the description (since the description should be optional)" in {
      implicit val okRequest: FakeRequest[AnyContentAsFormUrlEncoded] =
        buildFakeRequest("test_name", None)

      val result = Future.successful(form.bindFromRequest().fold(errorFunc, successFunc))
      val body = contentAsString(result)(Timeout(2, TimeUnit.SECONDS))

      body mustBe ""
    }

    "be ok when all the fields are present and valid" in {
      implicit val okRequest: FakeRequest[AnyContentAsFormUrlEncoded] =
        buildFakeRequest("test_name", Some("test_description"))

      val result = Future.successful(form.bindFromRequest().fold(errorFunc, successFunc))
      val body = contentAsString(result)(Timeout(2, TimeUnit.SECONDS))

      body mustBe ""
    }
  }

  /**
    * A simple convenience wrapper that creates a [[FakeRequest]] with the given details.
    *
    * @param name             The name which should be set in the Form
    * @param maybeDescription The description which should be set in the Form (if any)
    * @return
    */
  def buildFakeRequest(name: String, maybeDescription: Option[String]): FakeRequest[AnyContentAsFormUrlEncoded] =
    maybeDescription match {
      case Some(description) =>
        FakeRequest(POST, "/").withFormUrlEncodedBody("name" -> name, "description" -> description)
      case _ =>
        FakeRequest(POST, "/").withFormUrlEncodedBody("name" -> name)
    }

  /**
    * The function which will be invoked whenever a form failed it's validation
    *
    * @param badForm  The form
    * @param messages The implicit messages which are used to translate the error messages
    * @return
    */
  def errorFunc(badForm: Form[Data])(implicit messages: Messages): Result =
    BadRequest(badForm.errorsAsJson)

  /**
    * The function which will be invoked whenever a form succeeded it's validation
    *
    * @param data The data from the form
    * @return
    */
  def successFunc(data: Data): Result =
    Redirect("/").flashing("success" -> "success form!")
}
