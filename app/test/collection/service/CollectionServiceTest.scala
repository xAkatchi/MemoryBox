package collection.service

import java.util.{Date, UUID}

import authentication.user.User
import authentication.user.dao.UserDAO
import collection.data.dao.{CollectionDAO, UserCollectionDAO}
import collection.models.Collection
import collection.utils.SlugGenerator
import com.mohiva.play.silhouette.api.LoginInfo
import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import utils.FutureUtils

import scala.concurrent.ExecutionContext

/**
  * This class focuses on unit testing the implementations of the [[CollectionService]]
  */
class CollectionServiceTest extends PlaySpec with GuiceOneAppPerSuite with FutureUtils {
  implicit val ec: ExecutionContext = app.injector.instanceOf[ExecutionContext]

  val collectionDAO: CollectionDAO = app.injector.instanceOf[CollectionDAO]
  val userCollectionDAO: UserCollectionDAO = app.injector.instanceOf[UserCollectionDAO]
  val userDAO: UserDAO = app.injector.instanceOf[UserDAO]
  val slugGenerator: SlugGenerator = app.injector.instanceOf[SlugGenerator]

  val services: Seq[CollectionService] = Seq(
    new CollectionServiceImpl(collectionDAO, userCollectionDAO)
  )

  for (collectionService <- services) {
    "The implementation of the CollectionService (" + collectionService.getClass + ")" must {
      val collection = Collection(slugGenerator.generate, "name", Some("desc"), new Date())
      val user = User(UUID.randomUUID(), LoginInfo("credentials", "collection_service_test@codekrijger.io"),
        "fname", "lname", "collection_service_test@codekrijger.io", None, activated = true)
      val otherUser = User(UUID.randomUUID(), LoginInfo("credentials", "collection_service_test_2@codekrijger.io"),
        "fname_2", "lname_2", "collection_service_test_2@codekrijger.io", None, activated = true)

      // Make sure that the user exists, otherwise the relationships can't be created.
      await(userDAO.save(user))
      await(userDAO.save(otherUser))

      "return None when we try to find a non-existing collection" in {
        await(collectionService.retrieveSingle(collection.slug, user)) mustBe None
      }

      "return None when we try to send an invite to a collection that we don't belong to" in {
        await(collectionService.retrieveAll(user)) mustBe Seq()
        await(collectionService.retrieveAll(otherUser)) mustBe Seq()

        await(collectionService.addMember(otherUser, user, collection)) mustBe None
      }

      "return the created collection when it's successfully created" in {
        await(collectionService.create(collection, user)) mustBe Some(collection)
        // The given user, should also be linked to the collection so that it can access it
        await(userCollectionDAO.find(user)) mustBe Seq(collection)
      }

      "return None when the service can't create the given collection (due to a duplicate slug)" in {
        await(collectionService.create(collection, user)) mustBe None
      }

      "return the collection (linked to the given user) when we try to find an existing collection" in {
        await(collectionService.retrieveSingle(collection.slug, user)) mustBe Some(collection)
      }

      "return None when an user is not linked to the collection that we try to obtain" in {
        await(collectionService.retrieveSingle(collection.slug, otherUser)) mustBe None
      }

      "return a sequence with the just created collection in case of the 'user'" in {
        await(collectionService.retrieveAll(user)) mustBe Seq(collection)
      }

      "return an empty sequence when the given user has no access to collections" in {
        await(collectionService.retrieveAll(otherUser)) mustBe Seq()
      }

      "return None if we try to invite a user to a collection that he's already part of (and we're not)" in {
        await(collectionService.retrieveAll(user)) mustBe Seq(collection)
        await(collectionService.retrieveAll(otherUser)) mustBe Seq()

        await(collectionService.addMember(user, otherUser, collection)) mustBe None
      }

      "return the collection if we try to (successfully) invite a new user to the collection" in {
        await(collectionService.retrieveAll(otherUser)) mustBe Seq()
        await(collectionService.getMembersFromCollection(collection)) mustBe Seq(user)

        await(collectionService.addMember(otherUser, user, collection)) mustBe Some(collection)

        await(collectionService.retrieveAll(otherUser)) mustBe Seq(collection)
        await(collectionService.getMembersFromCollection(collection)) mustBe Seq(user, otherUser)
      }

      "return None if we try to invite a user that's already part of the collection" in {
        await(collectionService.retrieveAll(user)) mustBe Seq(collection)
        await(collectionService.retrieveAll(otherUser)) mustBe Seq(collection)

        await(collectionService.addMember(otherUser, user, collection)) mustBe None
      }
    }
  }
}
