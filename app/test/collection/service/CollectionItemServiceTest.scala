package collection.service

import java.util.{Date, UUID}

import authentication.user.User
import authentication.user.dao.UserDAO
import collection.data.dao.{CollectionDAO, CollectionItemDAO}
import collection.models.{Collection, CollectionItem, FileSystemStorage, PhotoDetails}
import com.mohiva.play.silhouette.api.LoginInfo
import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import utils.FutureUtils
import play.api.test.Helpers._

import scala.concurrent.ExecutionContext

/**
  * This class focuses on unit testing the implementations of the [[CollectionItemService]]
  */
class CollectionItemServiceTest extends PlaySpec with GuiceOneAppPerSuite with FutureUtils {
  implicit val ec: ExecutionContext = app.injector.instanceOf[ExecutionContext]

  val userDAO: UserDAO = app.injector.instanceOf[UserDAO]
  val collectionDAO: CollectionDAO = app.injector.instanceOf[CollectionDAO]
  val collectionItemDAO: CollectionItemDAO = app.injector.instanceOf[CollectionItemDAO]

  val services: Seq[CollectionItemService] = Seq(
    new CollectionItemServiceImpl(collectionItemDAO)
  )

  for (collectionItemService <- services) {
    s"The implementation of the CollectionItemService (${collectionItemService.getClass})" must {
      val height, width = 50
      val fileSize = 100
      val user = User(UUID.randomUUID(), LoginInfo("credentials", "collection_item_service_test@codekrijger.io"),
        "fname", "lname", "collection_item_service_test@codekrijger.io", None, activated = true)
      val collection = Collection("collection_item_service_slug", "name", Some("desc"), new Date())
      val collectionItem = CollectionItem(
        UUID.randomUUID(), collection, "name",
        FileSystemStorage("/path/to/file"), PhotoDetails(height, width, fileSize, "hash", "image/png"),
        new Date(), user)

      await(userDAO.save(user)) mustBe user
      await(collectionDAO.add(collection)) mustBe Some(collection)

      "be able to create the given collection item (linked to the given collection)" in {
        // No items should be present before we insert the item
        await(collectionItemService.find(collection)) mustBe Seq()
        await(collectionItemService.find(collectionItem.slug)) mustBe None

        await(collectionItemService.create(collectionItem)) mustBe Some(collectionItem)

        // The find() method should now return a single entry since we've created the collection item
        await(collectionItemService.find(collection)) mustBe Seq(collectionItem)
        await(collectionItemService.find(collectionItem.slug)) mustBe Some(collectionItem)
      }
    }
  }
}
