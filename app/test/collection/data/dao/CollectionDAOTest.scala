package collection.data.dao

import java.util.Date

import collection.models.Collection
import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.db.slick.DatabaseConfigProvider
import utils.FutureUtils

import scala.concurrent.ExecutionContext

/**
  * This class focuses on testing the [[CollectionDAO]]
  */
class CollectionDAOTest extends PlaySpec with GuiceOneAppPerSuite with FutureUtils {
  implicit val ec: ExecutionContext = app.injector.instanceOf[ExecutionContext]

  val collectionDAOs: Seq[CollectionDAO] = Seq(
    new DatabaseCollectionDAO(app.injector.instanceOf[DatabaseConfigProvider]),
  )

  for (collectionDAO <- collectionDAOs) {
    "The implementation of the CollectionDAO (" + collectionDAO.getClass + ")" must {
      val slug = "testcase-slug"
      val collection = Collection(
        slug,
        "A test collection",
        None,
        new Date()
      )
      val collectionWithDescription = collection.copy(slug = "another-test-slug", description = Some("description"))
      val updatedCollectionWithoutDescription = collectionWithDescription.copy(description = None)
      val updatedCollection = Collection(
        collection.slug,
        collection.name + "_updated",
        Some(collection.description + "_updated"),
        new Date()
      )

      "return None when no collection can be found" in {
        val maybeCollection = await(collectionDAO.find(slug))

        maybeCollection mustBe None
      }

      "be able to save a new collection without a description" in {
        await(collectionDAO.add(collection)) mustBe Some(collection)
      }

      "be able to save a new collection with a description" in {
        await(collectionDAO.add(collectionWithDescription)) mustBe Some(collectionWithDescription)
      }

      "be able to retrieve the newly added collection without a description" in {
        await(collectionDAO.find(collection.slug)) mustBe Some(collection)
      }

      "be able to retrieve the newly added collection with a description" in {
        await(collectionDAO.find(collectionWithDescription.slug)) mustBe Some(collectionWithDescription)
      }

      "be able to update the collection (without description) whenever we've new data" in {
        await(collectionDAO.update(updatedCollection)) mustBe Some(updatedCollection)
      }

      "be able to update the collection (with a description) whenever we've new data" in {
        await(collectionDAO.update(updatedCollectionWithoutDescription)) mustBe Some(updatedCollectionWithoutDescription)
      }

      "have updated the collection (without a description) when we try to access it by it's slug" in {
        val maybeCollection = await(collectionDAO.find(collection.slug))

        maybeCollection mustBe Some(updatedCollection)
        maybeCollection must not be Some(collection)
      }

      "have updated the collection (with a description) when we try to access it by it's slug" in {
        val maybeCollection = await(collectionDAO.find(collectionWithDescription.slug))

        maybeCollection mustBe Some(updatedCollectionWithoutDescription)
        maybeCollection must not be Some(collectionWithDescription)
      }

      "return None when a non existing collection gets updated" in {
        val nonExistingCollection = Collection(
          "non_existing_collection_slug",
          "non_existing_collection_name",
          Some("non_existing_collection_description"),
          new Date()
        )

        val maybeFoundCollection = await(collectionDAO.find(nonExistingCollection.slug))

        // The collection must not exist yet before we execute this test
        maybeFoundCollection mustBe None

        val maybeCollection = await(collectionDAO.update(nonExistingCollection))

        maybeCollection mustBe None
      }

      "return None whenever a collection with an existing slug gets added again" in {
        val collectionToAdd = Collection(
          "collection_to_add_slug",
          "collection_to_add_name",
          Some("collection_to_add_description"),
          new Date()
        )

        val maybeFoundCollection = await(collectionDAO.find(collectionToAdd.slug))

        // The collection must not exist yet before we execute this test
        maybeFoundCollection mustBe None

        val firstMaybeCollection = await(collectionDAO.add(collectionToAdd))
        val secondMaybeCollection = await(collectionDAO.add(collectionToAdd))

        firstMaybeCollection mustBe Some(collectionToAdd)
        secondMaybeCollection mustBe None
      }
    }
  }
}
