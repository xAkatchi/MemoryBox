package collection.data.dao

import java.util.{Date, UUID}

import authentication.user.User
import authentication.user.dao.UserDAO
import collection.models._
import com.mohiva.play.silhouette.api.LoginInfo
import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import utils.FutureUtils

import scala.concurrent.ExecutionContext

/**
  * This class focuses on testing the implementations of the [[CollectionItemThumbnailDAO]]
  */
class CollectionItemThumbnailDAOTest extends PlaySpec with GuiceOneAppPerSuite with FutureUtils {
  implicit val ec: ExecutionContext = app.injector.instanceOf[ExecutionContext]

  val userDAO: UserDAO = app.injector.instanceOf[UserDAO]
  val collectionDAO: CollectionDAO = app.injector.instanceOf[CollectionDAO]
  val collectionItemDAO: CollectionItemDAO = app.injector.instanceOf[CollectionItemDAO]
  val collectionItemThumbnailDAOs: Seq[CollectionItemThumbnailDAO] = Seq(
    app.injector.instanceOf[CollectionItemThumbnailDAO]
  )

  for (collectionItemThumbnailDAO <- collectionItemThumbnailDAOs) {
    "The implementation of the CollectionItemThumbnailDAO (" + collectionItemThumbnailDAO.getClass + ")" must {
      val user = User(
        UUID.randomUUID(), LoginInfo("credentials", "collection_item_thumbnail_dao_test@codekrijger.io"),
        "test_first_name", "test_last_name", "collection_item_thumbnail_dao_test@codekrijger.io",
        None, activated = true
      )
      val collection = Collection(
        "thumbnail_test_slug", "thumbnail_test_name", None, new Date()
      )
      val fileSize = 1000
      val height = 1440
      val width = 1024
      val collectionItem = CollectionItem(
        UUID.randomUUID(), collection, "thumbnail_test_name", FileSystemStorage("/dev/null"),
        PhotoDetails(height, width, fileSize, "some-collection-item-hash", "image/jpeg"), new Date(), user
      )
      val nonExistingCollectionItem = collectionItem.copy(slug = UUID.randomUUID())
      val thumbnail = CollectionItemThumbnail(
        UUID.randomUUID(), collectionItem,
        PhotoDetails(height, width, fileSize, "some-thumbnail-hash", "image/jpeg"),
        FileSystemStorage("/dev/null")
      )
      val nonExistingThumbnail = thumbnail.copy(id = UUID.randomUUID(), collectionItem = nonExistingCollectionItem)

      await(userDAO.save(user))
      await(collectionDAO.add(collection))
      await(collectionItemDAO.save(collectionItem))

      "return None whenever we request a non existing thumbnail" in {
        await(collectionItemThumbnailDAO.find(UUID.randomUUID())) mustBe None
      }

      "return an empty Seq whenever we request the thumbnails from a collection item without thumbnails" in {
        await(collectionItemThumbnailDAO.find(collectionItem)) mustBe Seq()
      }

      "not create the thumbnail whenever we link it to a 'non existing' collection item" in {
        await(collectionItemThumbnailDAO.save(nonExistingThumbnail)) mustBe None
        await(collectionItemThumbnailDAO.find(nonExistingThumbnail.id)) mustBe None
        await(collectionItemThumbnailDAO.find(nonExistingThumbnail.collectionItem)) mustBe Seq()
        await(collectionItemThumbnailDAO.find(nonExistingCollectionItem)) mustBe Seq()
      }

      "create the thumbnail whenever we link it to an existing collection item" in {
        await(collectionItemThumbnailDAO.save(thumbnail)) mustBe Some(thumbnail)
        await(collectionItemThumbnailDAO.find(thumbnail.id)) mustBe Some(thumbnail)
        await(collectionItemThumbnailDAO.find(thumbnail.collectionItem)) mustBe Seq(thumbnail)
        await(collectionItemThumbnailDAO.find(collectionItem)) mustBe Seq(thumbnail)
      }
    }
  }
}
