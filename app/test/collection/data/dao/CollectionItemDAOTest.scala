package collection.data.dao

import java.util.{Date, UUID}

import authentication.user.User
import authentication.user.dao.UserDAO
import collection.models._
import com.mohiva.play.silhouette.api.LoginInfo
import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.db.slick.DatabaseConfigProvider
import utils.FutureUtils

import scala.concurrent.ExecutionContext

/**
  * This class focuses on testing the [[CollectionItemDAO]]
  */
class CollectionItemDAOTest extends PlaySpec with GuiceOneAppPerSuite with FutureUtils {
  implicit val ec: ExecutionContext = app.injector.instanceOf[ExecutionContext]

  val userDAO: UserDAO = app.injector.instanceOf[UserDAO]
  val collectionDAO: CollectionDAO = app.injector.instanceOf[CollectionDAO]
  val collectionItemDAOs: Seq[CollectionItemDAO] = Seq(
    new DatabaseCollectionItemDAO(
      app.injector.instanceOf[DatabaseConfigProvider],
      collectionDAO,
      userDAO,
      app.injector.instanceOf[DatabaseFileDetailsDAO],
      app.injector.instanceOf[DatabaseStorageDAO],
    ),
  )

  for (collectionItemDAO <- collectionItemDAOs) {
    "The implementation of the CollectionItemDAO (" + collectionItemDAO.getClass + ")" must {
      val user = User(
        UUID.randomUUID(), LoginInfo("credentials", "collection_item_dao_test@codekrijger.io"),
        "test_first_name", "test_last_name", "collection_item_dao_test@codekrijger.io",
        None, activated = true
      )
      val nonExistingUser = User(
        UUID.randomUUID(), LoginInfo("credentials", "collection_item_dao_non_existing_user_test@codekrijger.io"),
        "non_existing_test_first_name", "non_existing_test_last_name", "collection_item_dao_non_existing_user_test@codekrijger.io",
        None, activated = true
      )
      val collection = Collection(
        "slug", "name", Some("description"), new Date()
      )
      val nonExistingCollection = Collection(
        "non_existing_slug", "non_existing_name", None, new Date()
      )
      val fileSize = 1000
      val height = 1440
      val width = 1024
      val collectionItem = CollectionItem(
        UUID.randomUUID(), collection, "test_name", FileSystemStorage("/dev/null"),
        PhotoDetails(height, width, fileSize, "some-hash", "image/jpeg"), new Date(), user
      )
      val collectionItemWithNonExistingUser = collectionItem.copy(uploadedBy = nonExistingUser)

      await(userDAO.save(user))
      await(collectionDAO.add(collection))

      "return an empty set whenever it can't find any items for an existing collection" in {
        await(collectionItemDAO.find(collection)) mustBe Seq()
      }

      "return None whenever it can't find the CollectionItem for the given id" in {
        await(collectionItemDAO.find(UUID.randomUUID())) mustBe None
      }

      "be able to save a new (valid) CollectionItem" in {
        await(collectionItemDAO.save(collectionItem)) mustBe Some(collectionItem)
      }

      "be able to find the newly created CollectionItem by collection" in {
        await(collectionItemDAO.find(collection)) mustBe Seq(collectionItem)
      }

      "be able to find the newly created CollectionItem by id" in {
        await(collectionItemDAO.find(collectionItem.slug)) mustBe Some(collectionItem)
      }

      "return None whenever a non-existing user has uploaded the CollectionItem" in {
        await(collectionItemDAO.save(collectionItemWithNonExistingUser)) mustBe None
      }

      "return None when a CollectionItem gets added to a non-existing Collection" in {
        await(collectionItemDAO.save(collectionItem.copy(collection = nonExistingCollection))) mustBe None
      }

      "return an empty Seq() whenever we pass a non existing collection to find()" in {
        await(collectionItemDAO.find(nonExistingCollection)) mustBe Seq()
      }
    }
  }
}
