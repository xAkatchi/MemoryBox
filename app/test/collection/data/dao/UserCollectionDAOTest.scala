package collection.data.dao

import java.util.{Date, UUID}

import authentication.user.User
import authentication.user.dao.UserDAO
import collection.models.Collection
import com.mohiva.play.silhouette.api.LoginInfo
import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.db.slick.DatabaseConfigProvider
import utils.FutureUtils

import scala.concurrent.ExecutionContext

/**
  * This class focuses on testing the behavior of the [[UserCollectionDAO]] implementations
  */
class UserCollectionDAOTest extends PlaySpec with GuiceOneAppPerSuite with FutureUtils {
  implicit val ec: ExecutionContext = app.injector.instanceOf[ExecutionContext]

  val userDAO: UserDAO = app.injector.instanceOf[UserDAO]
  val collectionDAO: CollectionDAO = app.injector.instanceOf[CollectionDAO]
  val userCollectionDAOs: Seq[UserCollectionDAO] = Seq(
    new DatabaseUserCollectionDAO(app.injector.instanceOf[DatabaseConfigProvider], userDAO, collectionDAO),
  )

  for (userCollectionDAO <- userCollectionDAOs) {
    "The implementation of the UserCollectionDAO (" + userCollectionDAO.getClass + ")" must {
      val user = User(UUID.randomUUID(), LoginInfo("credentials", "user_collection_dao_test@codekrijger.io"), "test_first_name", "test_last_name", "user_collection_dao_test@codekrijger.io", None, activated = true)
      val nonExistingUser = User(UUID.randomUUID(), LoginInfo("credentials", "non_existing_user_user_collection_dao_test@codekrijger.io"), "test_non_existing_user_first_name", "test_non_existing_user_last_name", "non_existing_user_user_collection_dao_test@codekrijger.io", None, activated = true)
      val linkedCollection = Collection("linked_slug", "linked_name", Some("linked_description"), new Date())
      val nonExistingCollection = Collection("non_existing_slug", "non_existing_name", None, new Date())

      await(userDAO.save(user))
      await(collectionDAO.add(linkedCollection))

      "return an empty sequence when there are no collections for the given user" in {
        await(userCollectionDAO.find(user)) mustBe Seq()
      }

      "return an empty sequence when we've linked no users to the given collection" in {
        await(userCollectionDAO.getMembers(linkedCollection)) mustBe Seq()
      }

      "return false when we ask if the not yet linked user belongs to the given collection" in {
        await(userCollectionDAO.belongsToCollection(user, linkedCollection)) mustBe false
      }

      "be able to create a relation between the given user and an existing collection" in {
        await(userCollectionDAO.add(user, linkedCollection)) mustBe Some(linkedCollection)
      }

      "be able to return the just created relationship when retrieving the collections from the given user" in {
        await(userCollectionDAO.find(user)) mustBe Seq(linkedCollection)
      }

      "return true when we ask if the newly linked user belongs to the given collection" in {
        await(userCollectionDAO.belongsToCollection(user, linkedCollection)) mustBe true
      }

      "return false when we ask if the newly linked user belongs to the given non existing collection" in {
        await(userCollectionDAO.belongsToCollection(user, nonExistingCollection)) mustBe false
      }

      "return false when we ask if a non existing user belongs to the given collection" in {
        await(userCollectionDAO.belongsToCollection(nonExistingUser, linkedCollection)) mustBe false
      }

      "return the just linked member when retrieving the members" in {
        await(userCollectionDAO.getMembers(linkedCollection)) mustBe Seq(user)
      }

      "return None when an already existing connection is being added" in {
        await(userCollectionDAO.add(user, linkedCollection)) mustBe None
      }

      "return an empty sequence when the collections of a non existing user are requested" in {
        await(userCollectionDAO.find(nonExistingUser)) mustBe Seq()
      }

      "return None when an existing collection is being linked to a non-existing user" in {
        await(userCollectionDAO.add(nonExistingUser, linkedCollection)) mustBe None
      }

      "return None when a non-existing collection is being linked to an existing user" in {
        await(userCollectionDAO.add(user, nonExistingCollection)) mustBe None
      }

      "return None when a non-existing collection is being linked to an non-existing user" in {
        await(userCollectionDAO.add(nonExistingUser, nonExistingCollection)) mustBe None
      }

      "return an empty Seq() when we request the members of a non existing collection" in {
        await(userCollectionDAO.getMembers(nonExistingCollection)) mustBe Seq()
      }
    }
  }
}
