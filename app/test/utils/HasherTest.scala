package utils

import org.scalatestplus.play.PlaySpec

/**
  * This class tests the behavior of the [[Hasher]]
  */
class HasherTest extends PlaySpec {
  "The Hasher" must {
    val hasher: Hasher = new Hasher
    val stringToHash = "test-string"

    "create a hash from any given string" in {
      hasher.hash(stringToHash) must not be stringToHash
    }

    "create a hash that's identical on subsequent invocations" in {
      hasher.hash(stringToHash) mustBe hasher.hash(stringToHash)
    }
  }
}
