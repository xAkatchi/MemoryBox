package utils

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

/**
  * This class contains some convenience methods that can be used by tests
  * who are interacting with [[Future]]s.
  */
trait FutureUtils {
  /**
    * A simple wrapper around [[Await.result()]] that waits infinitely
    * until the given [[Future]] is completed.
    *
    * @param v The [[Future]] which should be waited for
    * @tparam T
    * @return The result of the given [[Future]]
    */
  def await[T](v: Future[T]): T = Await.result(v, Duration.Inf)
}
