package core.token.service

import java.time.Clock
import java.util.UUID

import authentication.user.User
import authentication.user.dao.UserDAO
import com.mohiva.play.silhouette.api.LoginInfo
import core.token.data.dao.UserTokenDAO
import core.token.data.enums.TokenSubType
import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import utils.{FutureUtils, Hasher}

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

/**
  * This class focuses on testing the [[UserTokenService]]
  */
class UserTokenServiceTest extends PlaySpec with GuiceOneAppPerSuite with FutureUtils {
  implicit val ec: ExecutionContext = app.injector.instanceOf[ExecutionContext]

  private val clock: Clock = app.injector.instanceOf[Clock]
  private val userDAO: UserDAO = app.injector.instanceOf[UserDAO]
  private val userTokenDAO: UserTokenDAO = app.injector.instanceOf[UserTokenDAO]
  private val hasher: Hasher = app.injector.instanceOf[Hasher]
  private val services: Seq[UserTokenService] = Seq(
    new UserTokenServiceImpl(userTokenDAO, app.injector.instanceOf[Clock], hasher)
  )

  private val user = User(
    UUID.randomUUID(), LoginInfo("credentials", "user_token_service_test@codekrijger.io"),
    "test_first_name", "test_last_name", "user_token_service_test@codekrijger.io",
    None, activated = true
  )
  private val secondUser = User(
    UUID.randomUUID(), LoginInfo("credentials", "second_user_token_service_test@codekrijger.io"),
    "test_first_name", "test_last_name", "second_user_token_service_test@codekrijger.io",
    None, activated = true
  )

  await(userDAO.save(user))
  await(userDAO.save(secondUser))

  for (userTokenService <- services) {
    s"The implementation of the UserTokenService (${userTokenService.getClass})" must {
      "create a token" in {
        val token = await(userTokenService.createAccountActivationToken(user, 30 minutes))
        token.isDefined mustBe true
        token.get.userId mustBe user.id
        // Not exactly 30 minutes since some time has passed since we've created the token (and it's millisecond precise)
        // So we'll do an approximation
        token.get.validUntil must be > clock.instant().plusSeconds((29 minutes).toSeconds)
        token.get.validUntil must be < clock.instant().plusSeconds((30 minutes).toSeconds)
      }
      "create a token whilst respecting the given validity duration" in {
        val token = await(userTokenService.createAccountActivationToken(user, 5 minutes))
        token.isDefined mustBe true
        token.get.userId mustBe user.id
        // Not exactly 5 minutes since some time has passed since we've created the token (and it's millisecond precise)
        // So we'll do an approximation
        token.get.validUntil must be > clock.instant().plusSeconds((4 minutes).toSeconds)
        token.get.validUntil must be < clock.instant().plusSeconds((5 minutes).toSeconds)
      }
      "stop creating tokens once we've surpassed the token limit" in {
        // We've already created 2 tokens, so after creating 3 more tokens, we should have been limited
        await(userTokenService.createAccountActivationToken(user, 30 minutes)).isDefined mustBe true
        await(userTokenService.createAccountActivationToken(user, 30 minutes)).isDefined mustBe true
        await(userTokenService.createAccountActivationToken(user, 30 minutes)).isDefined mustBe true
        await(userTokenDAO.find(user, TokenSubType.ACCOUNT_ACTIVATION)).length mustBe 5
        await(userTokenService.createAccountActivationToken(user, 30 minutes)) mustBe None
      }
      "ignore expired tokens when checking the token limit" in {
        await(userTokenService.createAccountActivationToken(secondUser, 0 minutes)).isDefined mustBe true
        await(userTokenService.createAccountActivationToken(secondUser, 0 minutes)).isDefined mustBe true
        await(userTokenService.createAccountActivationToken(secondUser, 0 minutes)).isDefined mustBe true
        await(userTokenService.createAccountActivationToken(secondUser, 0 minutes)).isDefined mustBe true
        await(userTokenService.createAccountActivationToken(secondUser, 0 minutes)).isDefined mustBe true
        await(userTokenService.createAccountActivationToken(secondUser, 0 minutes)).isDefined mustBe true
      }
      "have removed all the expired tokens after invoking the 'remove' method whilst keeping the non expired tokens untouched" in {
        val userTokens = await(userTokenDAO.find(user, TokenSubType.ACCOUNT_ACTIVATION))
        val secondUserTokens = await(userTokenDAO.find(secondUser, TokenSubType.ACCOUNT_ACTIVATION))

        userTokens.length must be > 0
        // The secondUserTokens list should only be expired tokens (since that's all that's created for this user
        // in the above test), thus if this method returns an empty Seq(), we know that the removal was successful
        secondUserTokens.length must be > 0

        await(userTokenService.removeExpiredTokens()) mustBe secondUserTokens

        // The userTokens should be untouched since nothing is expired here
        await(userTokenDAO.find(user, TokenSubType.ACCOUNT_ACTIVATION)) mustBe userTokens
        // Everything from the secondUserTokens should be removed since the tokens were all expired
        await(userTokenDAO.find(secondUser, TokenSubType.ACCOUNT_ACTIVATION)) mustBe Seq()
      }
      "return false whenever we're checking the validity of an invalid token" in {
        await(userTokenService.isValidAccountActivationToken("some-invalid-token", user)) mustBe false
      }
      "return false whenever we're checking the validity of an expired token" in {
        val token = await(userTokenService.createAccountActivationToken(secondUser, 0 minutes)).get
        await(userTokenService.isValidAccountActivationToken(token.token, secondUser)) mustBe false
      }
      "return true whenever we're checking the validity of a valid token (and it should delete the token as well)" in {
        val token = await(userTokenService.createAccountActivationToken(secondUser, 30 minutes)).get
        val hashedToken = hasher.hash(token.token)

        // We store the hashed token, thus we must hash the token before we can find it in the DB
        await(userTokenDAO.find(hashedToken, secondUser, TokenSubType.ACCOUNT_ACTIVATION)) mustBe Some(token.copy(token = hashedToken))

        // We should however check the validity with the unhashed token, since the hash is an internal thing
        await(userTokenService.isValidAccountActivationToken(token.token, secondUser)) mustBe true

        await(userTokenDAO.find(hashedToken, secondUser, TokenSubType.ACCOUNT_ACTIVATION)) mustBe None
        // The validity should be false on second invocation since the token has been deleted
        await(userTokenService.isValidAccountActivationToken(token.token, secondUser)) mustBe false
      }
    }
  }

}
