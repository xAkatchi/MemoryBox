package core.token.data.dao

import java.sql.SQLException
import java.time.{Clock, Instant}
import java.util.UUID

import authentication.user.User
import authentication.user.dao.UserDAO
import com.mohiva.play.silhouette.api.LoginInfo
import core.token.model.UserToken
import core.token.data.enums.TokenSubType
import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.db.slick.DatabaseConfigProvider
import utils.FutureUtils

import scala.concurrent.ExecutionContext
import scala.util.Random

/**
  * This class tests the working of the [[UserTokenDAO]] classes (and it's implementations)
  */
class UserTokenDAOTest extends PlaySpec with GuiceOneAppPerSuite with FutureUtils {
  implicit val ec: ExecutionContext = app.injector.instanceOf[ExecutionContext]

  private val userDAO: UserDAO = app.injector.instanceOf[UserDAO]
  private val clock: Clock = app.injector.instanceOf[Clock]
  private val userTokenDAOs: Seq[UserTokenDAO] = Seq(
    new UserTokenDAOImpl(app.injector.instanceOf[DatabaseConfigProvider], app.injector.instanceOf[Clock]),
  )

  private val user = User(
    UUID.randomUUID(), LoginInfo("credentials", "user_token_dao_test@codekrijger.io"),
    "test_first_name", "test_last_name", "user_token_dao_test@codekrijger.io",
    None, activated = true
  )
  private val token = Random.alphanumeric.take(5).mkString
  private val validUntil = Instant.now().plusSeconds(60)
  private val userToken = UserToken(1, token, validUntil, user.id)

  await(userDAO.save(user))

  for (userTokenDAO <- userTokenDAOs) {
    s"The implementation of the UserTokenDAO (${userTokenDAO.getClass})" must {
      "return an empty sequence when there are no tokens found" in {
        await(userTokenDAO.find(user, TokenSubType.ACCOUNT_ACTIVATION)) mustBe Seq()
      }
      "return None when there is no token found" in {
        await(userTokenDAO.find(userToken.token, user, TokenSubType.ACCOUNT_ACTIVATION)) mustBe None
      }
      "return an empty sequence when we try to find the expired tokens (since no tokens are created yet)" in {
        await(userTokenDAO.findExpired()) mustBe Seq()
      }
      "return the created token on a successful save" in {
        await(userTokenDAO.save(token, validUntil, TokenSubType.ACCOUNT_ACTIVATION, user)) mustBe Some(userToken)
      }
      "throw an exception whenever we try to insert a duplicate token" in {
        intercept[SQLException]  {
          await(userTokenDAO.save(token, validUntil, TokenSubType.ACCOUNT_ACTIVATION, user))
        }
      }
      "return the created token (as a seq) when we try to request it" in {
        await(userTokenDAO.find(user, TokenSubType.ACCOUNT_ACTIVATION)) mustBe Seq(userToken)
      }
      "return the created token when we try to find it" in {
        await(userTokenDAO.find(userToken.token, user, TokenSubType.ACCOUNT_ACTIVATION)) mustBe Some(userToken)
      }
      "still return an empty array as expired tokens since the just created token should still be valid" in {
        await(userTokenDAO.findExpired()) mustBe Seq()
      }
      "return false if the token does not exist (with existance check)" in {
        await(userTokenDAO.doesTokenExist("non-existent", TokenSubType.ACCOUNT_ACTIVATION)) mustBe false
      }
      "return true if the token does exist (with the existance check)" in {
        await(userTokenDAO.doesTokenExist(token, TokenSubType.ACCOUNT_ACTIVATION)) mustBe true
      }
      "return true whenever we try to delete the token" in {
        await(userTokenDAO.remove(userToken)) mustBe true
      }
      "return false whenever we try to delete an already deleted (/ non existent) token" in {
        await(userTokenDAO.remove(userToken)) mustBe false
      }
      "return an empty sequence when there are no tokens found (since it should be deleted)" in {
        await(userTokenDAO.find(user, TokenSubType.ACCOUNT_ACTIVATION)) mustBe Seq()
      }
      "return None when there is no token found (since it should be deleted)" in {
        await(userTokenDAO.find(userToken.token, user, TokenSubType.ACCOUNT_ACTIVATION)) mustBe None
      }
      "return expired tokens if there are such tokens" in {
        await(userTokenDAO.findExpired()) mustBe Seq()

        // Create tokens that are expired

        // A token that surely has been expired
        val expiredToken = await(userTokenDAO.save(
          Random.alphanumeric.take(4).mkString,
          clock.instant().minusNanos(1),
          TokenSubType.ACCOUNT_ACTIVATION,
          user
        )).get
        // A token that's expiring at the current timestamp, this should also be included inside the expire check
        val expiredTokenAtCurrentTimestamp = await(userTokenDAO.save(
          Random.alphanumeric.take(3).mkString,
          clock.instant(),
          TokenSubType.ACCOUNT_ACTIVATION,
          user
        )).get

        await(userTokenDAO.findExpired()) mustBe Seq(expiredToken, expiredTokenAtCurrentTimestamp)

        // Remove the expired tokens so that they don't interfere with other tests
        // Ideally we should wipe the DB after every test, but that is a thing for the roadmap
        await(userTokenDAO.remove(expiredToken)) mustBe true
        await(userTokenDAO.remove(expiredTokenAtCurrentTimestamp)) mustBe true
      }
    }
  }
}
