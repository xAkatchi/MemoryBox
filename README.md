# MemoryBox

[![pipeline status](https://gitlab.com/xAkatchi/MemoryBox/badges/master/pipeline.svg)](https://gitlab.com/xAkatchi/MemoryBox/commits/master)
[![coverage report](https://gitlab.com/xAkatchi/MemoryBox/badges/master/coverage.svg)](https://gitlab.com/xAkatchi/MemoryBox/commits/master)

WIP

### Get started:
1. Copy the `environment.env.sample` and remove the `.sample` extension.
    - Don't forget to change the values in this file to your liking.