#!/usr/bin/env bash

export VERSION=${1}

mkdir -p .generated

for template in kubernetes-templates/*.yaml
do
    # Replace the environmental variables in the template and create deployment files
    envsubst < $template > ".generated/$(basename $template)"
    echo $(basename $template)
done

kubectl apply -f .generated/