import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from "@angular/router";
import {User} from "../../authentication/core/model/user";
import {UserService} from "./user.service";
import {EMPTY, Observable, of} from "rxjs";
import {catchError, mergeMap, take} from "rxjs/operators";
import {ToasterService} from "angular2-toaster";

/**
 * The goal of this resolver is to load the
 * logged in user object.
 *
 * @link https://angular.io/guide/router#resolve-pre-fetching-component-data
 */
@Injectable({ providedIn: 'root' })
export class LoggedInUserResolverService implements Resolve<User> {

  constructor(private userService: UserService,
              private router: Router,
              private toasterService: ToasterService) { }

  /**
   * This method is invoked before the component is loaded.
   *
   * The goal of this method is to obtain the logged in user
   * (if any) before the component itself is loaded.
   *
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<User> | Promise<User> | User {
    return this
      .userService
      .getLoggedInUser()
      .pipe(
        take(1),
        mergeMap(user => { return of(user); }),
        catchError(error => {
          this.toasterService.popAsync('error', 'Failed to obtain the logged in user');
          // TODO log to Sentry (in the interceptor itself) + go to 500/400 page
          console.log(error);
          this.router.navigate(['/']);
          return EMPTY;
        })
      )
  }

}
