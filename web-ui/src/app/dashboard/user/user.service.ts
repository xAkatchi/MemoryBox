import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {User} from "../../authentication/core/model/user";
import {environment} from "../../../environments/environment";
import {map} from "rxjs/operators";

/**
 * The goal of this service is to interact with the user related
 * endpoints from the backend.
 */
@Injectable()
export class UserService {
  constructor(private http: HttpClient) { }

  /**
   * This method tries to return the logged in user (if any)
   *
   * @return {Observable<User>|Observable<null>}
   */
  getLoggedInUser() {
    return this.http
      .get<any>(environment.baseUrl + '/user')
      .pipe(map(response => {
        if(response && response.id) {
          return new User(
            response.id,
            response.firstName,
            response.lastName,
            response.email,
            response.avatarURL
          );
        }

        return null;
      }));
  }

  /**
   * This method tries to update the logged in user with respect to
   * the given parameters.
   *
   * @param {string} firstName
   * @param {string} lastName
   *
   * @return {Observable>User>|Observable<null>}
   */
  updateLoggedInUser(firstName: string, lastName: string) {
    return this.http
      .patch<any>(
        environment.baseUrl + '/user',
        { firstName, lastName }
      ).pipe(map(response => {
        if(response && response.id) {
          return new User(
            response.id,
            response.firstName,
            response.lastName,
            response.email,
            response.avatarURL
          );
        }

        return null;
      }))
  }
}
