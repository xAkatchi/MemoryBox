import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {User} from "../../../authentication/core/model/user";
import {UserService} from "../user.service";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {ActivatedRoute} from "@angular/router";
import {first} from "rxjs/operators";
import {NavbarService} from "../../navbar/navbar.service";
import {ToasterService} from "angular2-toaster";

/**
 * The goal of this component is to make it possible for the user
 * to update his / her profile.
 */
@Component({
  selector: 'app-dashboard-user-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  providers: [FontAwesomeModule]
})
export class ProfileComponent implements OnInit {
  user: User;
  profileForm: FormGroup;
  loading = false;
  submitted = false;

  constructor(private formBuilder: FormBuilder,
              private userService: UserService,
              private route: ActivatedRoute,
              private navbarService: NavbarService,
              private toasterService: ToasterService) { }

  ngOnInit() {
    this.profileForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: [{value: null, disabled: true}, Validators.required]
    });

    this.route.data.subscribe((data: { user: User}) => {
      this.user = data.user;

      this.form.firstName.setValue(this.user.firstName);
      this.form.lastName.setValue(this.user.lastName);
      this.form.email.setValue(this.user.email);
    });
  }

  /**
   * Whenever the submit button is pressed,
   * the current form values will be used to update the user
   */
  onSubmit() {
    this.submitted = true;

    if (this.profileForm.invalid) {
      return;
    }

    this.loading = true;
    this.userService
      .updateLoggedInUser(
        this.form.firstName.value,
        this.form.lastName.value
      )
      .pipe(first())
      .subscribe(
        user => {
          this.toasterService.popAsync('success', 'Profile updated');
          this.loading = false;
          this.navbarService.updateUser(user);
        },
        error => {
          this.toasterService.popAsync('error', 'Failed to update profile', error);
          this.loading = false;
        }
      )
  }

  /**
   * A convenience getter to make it possible to easily access the form fields.
   *
   * @return {AbstractControl}
   */
  get form() {
    return this.profileForm.controls;
  }
}
