import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {RouterModule} from "@angular/router";
import {NgxSmartModalModule} from "ngx-smart-modal";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {DeferLoadModule} from "@trademe/ng-defer-load";
import {CollectionModule} from "./collection/collection.module";
import {DashboardComponent} from './dashboard.component';
import {NavbarComponent} from "./navbar/navbar.component";
import {UserAvatarModule} from "../user-avatar/user-avatar.module";
import {NgProgressModule} from "@ngx-progressbar/core";
import {ProfileComponent} from './user/profile/profile.component';
import {UserService} from "./user/user.service";
import {NavbarService} from "./navbar/navbar.service";

@NgModule({
  declarations: [
    NavbarComponent,
    DashboardComponent,
    ProfileComponent,
  ],
  imports: [
    CommonModule,

    NgbModule,
    RouterModule,
    NgxSmartModalModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    FormsModule,
    DeferLoadModule,
    UserAvatarModule,
    NgProgressModule,

    CollectionModule
  ],
  providers: [
    UserService,
    NavbarService,
  ]
})
export class DashboardModule {
}
