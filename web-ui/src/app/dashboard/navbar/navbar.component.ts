import {Component, Input, OnInit} from '@angular/core';
import {User} from "../../authentication/core/model/user";
import {AuthTokenService} from "../../authentication/core/service/auth-token.service";
import {Router} from "@angular/router";
import {NavbarService} from "./navbar.service";

@Component({
  selector: 'app-dashboard-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  providers: [AuthTokenService]
})
export class NavbarComponent implements OnInit {
  @Input() user: User;

  constructor(private authTokenService: AuthTokenService,
              private router: Router,
              private navbarService: NavbarService) {
  }

  ngOnInit() {
    this.navbarService.user$
      .subscribe(user => this.user = user);
  }

  /**
   * This logs the currently logged in user out and then redirects
   * them back to the login screen.
   */
  logout() {
    this.authTokenService.deleteToken();
    this.router.navigate(['/login'])
  }
}
