import {Injectable} from '@angular/core';
import {Subject} from "rxjs";
import {User} from "../../authentication/core/model/user";

/**
 * The goal of this service is to be able to communicate with the navbar component
 * and thus update some properties of the navbar
 *
 * @link https://www.reddit.com/r/Angular2/comments/4qhumr/communication_between_navbar_and_pages/d4t7mt8
 * @link https://angular.io/guide/component-interaction#parent-and-children-communicate-via-a-service
 */
@Injectable()
export class NavbarService {
  private userSource = new Subject<User>();

  user$ = this.userSource.asObservable();

  /**
   * This method can be invoked to push a {User} update
   * to the navbar component.
   *
   * @param {User} user
   */
  updateUser(user: User) {
    this.userSource.next(user);
  }
}
