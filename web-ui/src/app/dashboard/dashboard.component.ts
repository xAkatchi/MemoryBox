import {Component, OnInit} from '@angular/core';
import {User} from "../authentication/core/model/user";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html'
})
export class DashboardComponent implements OnInit {
  user: User;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.data.subscribe((data: { user: User}) => {
      this.user = data.user;
    });
  }
}


