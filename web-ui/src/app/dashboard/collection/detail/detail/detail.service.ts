import {Injectable} from "@angular/core";
import {Subject} from "rxjs";
import {User} from "../../../../authentication/core/model/user";

/**
 * The goal of this service is to be able to communicate with the collection-detail
 * component and thus update some of the properties there.
 */
@Injectable()
export class DetailService {
  private memberSource = new Subject<User>();

  members$ = this.memberSource.asObservable();

  /**
   * This method can be invoked to push a member update
   * to the collection-detail component
   *
   * @param {User} member
   */
  addMember(member: User) {
    this.memberSource.next(member);
  }
}
