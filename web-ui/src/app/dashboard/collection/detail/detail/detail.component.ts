import {Component, OnInit} from '@angular/core';
import {Collection} from "../../core/model/collection";
import {ActivatedRoute} from "@angular/router";
import {DetailService} from "./detail.service";

@Component({
  selector: 'app-dashboard-collection-detail-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  collection: Collection;

  constructor(private route: ActivatedRoute,
              private detailService: DetailService) { }

  ngOnInit() {
    this.route.data.subscribe((data: { collection: Collection }) => {
      this.collection = data.collection;
    });

    this.detailService.members$.subscribe(item => {
      this.collection.members.push(item);
    })
  }
}
