import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PhotoService} from "./photo/photo.service";
import {PhotoSwipeComponent} from "../../../photo-swipe/photo-swipe.component";
import {CollectionDetailComponent} from "./detail.component";
import {CollectionDetailItemComponent} from "./photo/item/item.component";
import {UploadItemComponent} from "./navbar/upload-item/upload-item.component";
import {PhotoComponent} from "./photo/photo.component";
import {NavbarComponent} from "./navbar/navbar.component";
import {DetailComponent} from "./detail/detail.component";
import {AddMemberComponent} from "./navbar/add-member/add-member.component";
import {AvatarModule} from "ngx-avatar";
import {UserAvatarModule} from "../../../user-avatar/user-avatar.module";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {RouterModule} from "@angular/router";
import {NgxSmartModalModule} from "ngx-smart-modal";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DeferLoadModule} from "@trademe/ng-defer-load";
import {DetailService} from "./detail/detail.service";
import {ThumbnailRetrieverService} from "../core/services/thumbnail-retriever.service";
import {FileRetrievalService} from "../../../common/services/file-retrieval.service";

@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    RouterModule,
    NgxSmartModalModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    FormsModule,
    DeferLoadModule,
    CommonModule,
    AvatarModule,
    UserAvatarModule,
  ],
  declarations: [
    PhotoSwipeComponent,
    CollectionDetailComponent,
    CollectionDetailItemComponent,
    UploadItemComponent,
    PhotoComponent,
    NavbarComponent,
    DetailComponent,
    AddMemberComponent
  ],
  providers: [
    PhotoService,
    DetailService,
    FileRetrievalService,
    ThumbnailRetrieverService
  ]
})
export class DetailModule { }
