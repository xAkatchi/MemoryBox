import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-dashboard-collection-detail',
  templateUrl: './detail.component.html'
})
export class CollectionDetailComponent implements OnInit {
  collectionSlug: string;

  constructor(private route: ActivatedRoute) { }

  ngOnInit(){
    this.route.params.subscribe(queryParams => {
      this.collectionSlug = queryParams['slug'];
    })
  }
}
