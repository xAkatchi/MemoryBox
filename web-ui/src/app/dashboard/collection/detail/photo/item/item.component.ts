import {Component, Input, OnInit} from '@angular/core';
import {CollectionItem} from "../../../core/model/collection-item";
import {ThumbnailRetrieverService} from "../../../core/services/thumbnail-retriever.service";
import {filter} from "rxjs/operators";

/**
 * This component represents a single CollectionItem inside the detail
 * page.
 */
@Component({
    selector: 'app-dashboard-collection-detail-item',
    templateUrl: './item.component.html',
    styleUrls: ['./item.component.scss'],
})
export class CollectionDetailItemComponent implements OnInit {
    @Input() collectionItem: CollectionItem;
    imageToShow: string|ArrayBuffer;
    isImageLoading: boolean;

    constructor(private thumbnailRetrieverService: ThumbnailRetrieverService) { }

    /**
     * Handles the initialization of this class
     */
    ngOnInit() {
      this.isImageLoading = true;

      this
        .thumbnailRetrieverService
        .getThumbnail(this.collectionItem, 300)
        .pipe(
          filter(data => data !== null)
        )
        .subscribe(image => this.imageToShow = image);

      this.isImageLoading = false;
    }
}
