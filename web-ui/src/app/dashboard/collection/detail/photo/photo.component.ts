import {Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {PhotoSwipeComponent} from "../../../../photo-swipe/photo-swipe.component";
import {PhotoSwipeImage} from "../../../../photo-swipe/image";
import {ActivatedRoute} from "@angular/router";
import {CollectionItem} from "../../core/model/collection-item";
import {PhotoDetails} from "../../core/model/file-details-photo";
import {environment} from "../../../../../environments/environment";
import {PhotoService} from "./photo.service";
import justifyLayout from "justified-layout";
import {Subject} from "rxjs";
import {bufferTime, debounceTime, filter} from "rxjs/operators";

@Component({
  selector: 'app-dashboard-collection-detail-photos',
  templateUrl: './photo.component.html',
  styleUrls: ['./photo.component.scss'],
  providers: [PhotoSwipeComponent]
})
export class PhotoComponent implements OnInit {
  @ViewChild('photoSwipe') photoSwipe: PhotoSwipeComponent;
  items: CollectionItem[];
  shouldShow = [];
  images: PhotoSwipeImage[] = [];
  itemContainer: any;
  private resizeEvents = new Subject<UIEvent>();
  private resizeEvents$ = this.resizeEvents.asObservable();

  constructor(private route: ActivatedRoute,
              private photoService: PhotoService) { }

  ngOnInit() {
    this.route.data.subscribe((data: { items: CollectionItem[] }) => {
      this.items = data.items;
      this.redraw(this.items, window.innerHeight, window.innerWidth);
      this.images = this.images.concat(
        data.items
          .map(this.itemToImage)
          .filter(function (item) { return item !== null; })
      );
    });

    this.photoService.items$
      .pipe(
        bufferTime(5000),
        filter(items => items.length > 0)
      )
      .subscribe((items: CollectionItem[]) => {
        this.items = this.items.concat(items);
        this.redraw(this.items, window.innerHeight, window.innerWidth);
        this.images = this.images.concat(
          items
            .map(this.itemToImage)
            .filter(function (item) { return item !== null; })
        );
      });

    this.resizeEvents$
      // Only redraw the whole grid if there has been no other resize event in the given due time
      .pipe(debounceTime(150))
      .subscribe(event => {
        let target = event.target as Window;
        this.redraw(this.items, target.innerHeight, target.innerWidth);
      });
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.resizeEvents.next(event);
  }

  /**
   * Invoking this method will redraw the item-grid on the page containing
   * the given items.
   *
   * @param {CollectionItem[]} items
   * @param {number} targetHeight The height of the view (used for scaling / offsets)
   * @param {number} targetWidth The width of the view (used for scaling / offsets)
   */
  private redraw(items: CollectionItem[], targetHeight: number, targetWidth: number) {
    let formats = items.map(item => {
      let thumbnail = item.getThumbnailWithHeight(300);

      if (thumbnail) {
        return { width: thumbnail.fileDetails.width, height: thumbnail.fileDetails.height };
      }

      return { width: 200, height: 300 };
    });
    this.itemContainer = justifyLayout(formats, {
      containerWidth: targetWidth,
      targetRowHeight: targetHeight / 3.5,
      targetRowHeightTolerance: 0.25,
      containerPadding: {
        top: 10,
        right: 25,
        bottom: 10,
        left: 10
      }
    });
  }

  /**
   * This method tries to convert the given {CollectionItem}
   * to a {PhotoSwipeImage} that can be used in the gallery.
   *
   * @param {CollectionItem} item
   *
   * @return {PhotoSwipeImage|null} null on failure
   */
  private itemToImage(item: CollectionItem): PhotoSwipeImage|null {
    if (item.fileDetails instanceof PhotoDetails) {
      return {
        src: environment.baseUrl + item.fileUrl,
        w: item.fileDetails.width,
        h: item.fileDetails.height,
      };
    }

    console.log('Unsupported item type.', item);
    return null;
  }

  /**
   * Opens the gallery at the given start slide
   *
   * @param {number} startAtSlide
   */
  openGallery(startAtSlide: number) {
    this.photoSwipe.openGallery(startAtSlide);
  }
}
