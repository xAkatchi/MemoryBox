import {Injectable} from "@angular/core";
import {CollectionItem} from "../../core/model/collection-item";
import {Subject} from "rxjs";

/**
 * The goal of this service is to be able to communicate with the collection-photos
 * component and thus update some of the properties there.
 */
@Injectable()
export class PhotoService {
  private itemSource = new Subject<CollectionItem>();

  items$ = this.itemSource.asObservable();

  /**
   * This method can be invoked to push a {CollectionItem} update
   * to the collection-photos component
   *
   * @param {CollectionItem} item
   */
  addItem(item: CollectionItem) {
    this.itemSource.next(item);
  }
}
