import {Component, Input, OnInit} from '@angular/core';
import {CollectionItemService} from "../../../core/services/collection-item.service";
import {range, of} from "rxjs";
import {catchError, map, mergeMap} from "rxjs/operators";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {ByteUtils} from "../../../../../common/utils/bytes.utils";
import {PhotoService} from "../../photo/photo.service";
import {ToasterService} from "angular2-toaster";

/**
 * The goal of this class is to handle the uploading of items for
 * a given collection.
 */
@Component({
    selector: 'app-dashboard-collection-detail-navbar-upload-item',
    templateUrl: './upload-item.component.html',
    styleUrls: ['./upload-item.component.scss'],
    providers: [CollectionItemService, FontAwesomeModule, ByteUtils]
})
export class UploadItemComponent implements OnInit{
    @Input() collectionSlug: string;
    uploadForm: FormGroup;
    loading = false;
    filesToUpload = [];
    itemsUploaded: number = 0;
    itemsFailedToUpload: number = 0;

    constructor(private collectionItemService: CollectionItemService,
                private formBuilder: FormBuilder,
                private byteUtils: ByteUtils,
                private photoService: PhotoService,
                private toasterService: ToasterService) {
    }

    /**
     * Initializes all the parameters whenever this component is being initialized
     */
    public ngOnInit() {
        this.uploadForm = this.formBuilder.group({
            files: ['', Validators.required],
        });
    }

    /**
     * Whenever the file input is changed, we'd like to update our internal filesToUpload
     * array to make sure that it contains the latest selected files.
     *
     * @param {Event} event
     */
    public onFileChange(event) {
        this.filesToUpload = [];

        if(event.target.files) {
            let length = event.target.files.length;

            for(let i = 0; i < length; i++) {
                let file: File = event.target.files[i];

                this.filesToUpload.push({
                    'file': file,
                    'name': file.name,
                    'size': this.byteUtils.bytesToSize(file.size),
                    'type': file.type,
                    'status': 'waiting',
                    'message': 'Ready to upload',
                })
            }
        }
    }

    /**
     * Whenever the submit button is pressed,
     * all the selected files will be uploaded.
     */
    public onSubmit() {
        if (this.uploadForm.invalid) {
            return;
        }

        this.loading = true;
        this.itemsUploaded = 0;
        this.itemsFailedToUpload = 0;

        range(0, this.filesToUpload.length)
            .pipe(
                map(i => { return this.filesToUpload[i]; }),
                mergeMap(object => {
                    object.status = 'uploading';
                    object.message = 'Uploading';

                    return this
                        .collectionItemService
                        .uploadItem(
                            this.collectionSlug,
                            object.file,
                            object.name
                        )
                        .pipe(
                            map(result => {
                                this.itemsUploaded++;

                                object.status = 'success';
                                object.message = 'Success';

                                this.photoService.addItem(result);

                                return of(result);
                            }),
                            catchError(error => {
                                this.itemsFailedToUpload++;

                                object.status = 'failed';
                                object.message = error;

                                return of(object);
                            })
                        );
                }, this.getMaxConcurrentRequests()),
            ).subscribe(result => {
                if (this.uploadedEverything()) {
                    this.loading = false;

                    if (this.itemsFailedToUpload > 0) {
                      this.toasterService.popAsync('warning', 'Item upload complete', this.itemsFailedToUpload + ' items weren\'t uploaded.');
                    } else {
                      this.toasterService.popAsync('success', 'Item upload complete');
                    }
                }
            });
    }

    /**
     * Returns whether or not we've uploaded everything
     *
     * @return {boolean}
     */
    private uploadedEverything(): boolean {
        return this.itemsFailedToUpload + this.itemsUploaded === this.filesToUpload.length;
    }

    /**
     * Returns the maximum number of conccurrent requests that we'd like to use
     * when uploading items.
     *
     * @return {number}
     */
    private getMaxConcurrentRequests(): number {
        // For now we only allow 1 concurrent upload at a time
        // this so that we can safely use a simple server without overloading it all the time
        return 1;
    }
}
