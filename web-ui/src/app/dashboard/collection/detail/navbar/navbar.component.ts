import {Component, Input} from '@angular/core';
import {NgbModal, NgbModalRef} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-dashboard-collection-detail-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {
  @Input() collectionSlug: string;
  modal: NgbModalRef;

  constructor(private ngbModalService: NgbModal) { }

  /**
   * Opens the modal with the given identifier
   *
   * @param {object} identifier
   * @param {object} options
   */
  public open(identifier: object, options: object = {}) {
      this.modal = this.ngbModalService.open(identifier, options);
  }
}
