import {Component, Input, OnInit} from '@angular/core';
import {CollectionService} from "../../../core/services/collection.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {DetailService} from "../../detail/detail.service";
import {ToasterService} from "angular2-toaster";

/**
 * The goal of this component is to handle all the logic related
 * to 'adding/inviting' members (users) to a collection.
 */
@Component({
  selector: 'app-dashboard-collection-detail-navbar-add-member',
  templateUrl: './add-member.component.html',
  providers: [CollectionService, FontAwesomeModule]
})
export class AddMemberComponent implements OnInit {
  @Input() collectionSlug: string;
  addMemberForm: FormGroup;
  loading = false;
  submitted = false;

  constructor(private collectionService: CollectionService,
              private formBuilder: FormBuilder,
              private detailService: DetailService,
              private toasterService: ToasterService) {

  }

  ngOnInit() {
    this.addMemberForm = this.formBuilder.group({
        email: ['', [Validators.required, Validators.email]]
    });
  }

  /**
   * Whenever the submit button is pressed,
   * the 'invitation' should be send (given that everything is valid)
   */
  onSubmit() {
    this.submitted = true;

    if (this.addMemberForm.invalid) {
      return;
    }

    this.loading = true;

    this.collectionService
      .addMember(this.collectionSlug, this.form.email.value)
      .subscribe(
        data => {
          this.toasterService.popAsync('success', 'Successfully added ' + this.form.email.value);
          this.detailService.addMember(data);
          this.loading = false;
        },
        error => {
          this.toasterService.popAsync('error', 'Failed to add the member', error);
          this.loading = false;
        }
      )
  }

  /**
   * A convenience getter to make it possible to easily access the form fields.
   *
   * @return {AbstractControl}
   */
  get form() {
    return this.addMemberForm.controls;
  }
}
