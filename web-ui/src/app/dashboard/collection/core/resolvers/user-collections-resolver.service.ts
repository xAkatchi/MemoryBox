import {Injectable} from "@angular/core";
import {Collection} from "../model/collection";
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from "@angular/router";
import {CollectionService} from "../services/collection.service";
import {EMPTY, Observable, of} from "rxjs";
import {catchError, mergeMap, take} from "rxjs/operators";
import {ToasterService} from "angular2-toaster";

/**
 * The goal of this resolver is to load all the
 * {Collection}s that belong to the logged in {User}.
 */
@Injectable({ providedIn: 'root' })
export class UserCollectionsResolverService implements Resolve<Collection[]> {

  constructor(private collectionService: CollectionService,
              private router: Router,
              private toasterService: ToasterService) { }

  /**
   * This method is invoked before the component is loaded.
   *
   * The goal of this method is to obtain the {Collection}s
   * that belong to the logged in {User}.
   *
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Collection[]> | Promise<Collection[]> | Collection[] {
    return this
      .collectionService
      .getAllCollectionsFromCurrentUser()
      .pipe(
        take(1),
        mergeMap(collections => { return of(collections) }),
        catchError(error => {
          this.toasterService.popAsync('error', 'Failed to obtain the collections');
          // TODO log to Sentry (in the interceptor itself) + go to 500/400 page
          console.log(error);
          this.router.navigate(['/']);
          return EMPTY;
        })
      )
  }
}
