import {Injectable} from "@angular/core";
import {CollectionService} from "../services/collection.service";
import {Collection} from "../model/collection";
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from "@angular/router";
import {EMPTY, Observable, of} from "rxjs";
import {catchError, mergeMap, take} from "rxjs/operators";
import {ToasterService} from "angular2-toaster";

/**
 * The goal of this resolver is to load a single
 * {Collection} object.
 */
@Injectable({ providedIn: 'root' })
export class CollectionDetailsResolverService implements Resolve<Collection> {

  constructor(private collectionService: CollectionService,
              private router: Router,
              private toasterService: ToasterService) { }

  /**
   * This method is invoked before the component is loaded.
   *
   * The goal of this method is to obtain the relevant
   * {Collection} that matches the slug defined in the url.
   *
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Collection> | Promise<Collection> | Collection {
    let slug = route.parent.paramMap.get('slug');

    return this
      .collectionService
      .getCollection(slug)
      .pipe(
        take(1),
        mergeMap(collection => { return of(collection); }),
        catchError(error => {
          this.toasterService.popAsync('error', 'Unable to find the requested collection');
          // TODO log to Sentry (in the interceptor itself) + go to 500/400 page
          console.log(error);
          this.router.navigate(['/dashboard']);
          return EMPTY;
        })
      );
  }
}
