import {Injectable} from "@angular/core";
import {CollectionService} from "../services/collection.service";
import {CollectionItem} from "../model/collection-item";
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from "@angular/router";
import {EMPTY, Observable, of} from "rxjs";
import {catchError, mergeMap, take} from "rxjs/operators";
import {ToasterService} from "angular2-toaster";

/**
 * The goal of this resolver is to load all the
 * {CollectionItem}s before showing the component.
 */
@Injectable({ providedIn: 'root' })
export class CollectionItemsResolverService implements Resolve<CollectionItem[]> {

  constructor(private collectionService: CollectionService,
              private router: Router,
              private toasterService: ToasterService) { }

  /**
   * This method is invoked before the component is loaded.
   *
   * The goal of this method is to obtain the relevant
   * {CollectionItem}s that belong to the slug defined in the url.
   *
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<CollectionItem[]> | Promise<CollectionItem[]> | CollectionItem[] {
    let slug = route.parent.paramMap.get('slug');

    return this
      .collectionService
      .getItems(slug)
      .pipe(
        take(1),
        mergeMap(items => { return of(items) }),
        catchError(error => {
          this.toasterService.popAsync('error', 'Unable to find the requested collection');
          // TODO log to Sentry (in the interceptor itself) + go to 500/400 page
          console.log(error);
          this.router.navigate(['/dashboard']);
          return EMPTY;
        })
      )
  }
}
