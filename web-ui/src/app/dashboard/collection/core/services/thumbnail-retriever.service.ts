import {Injectable} from "@angular/core";
import {FileRetrievalService} from "../../../../common/services/file-retrieval.service";
import {CollectionItem} from "../model/collection-item";
import {fromEvent, Observable, of} from "rxjs";
import {filter, flatMap, map} from "rxjs/operators";
import {CollectionItemThumbnail} from "../model/collection-item-thumbnail";

/**
 * The goal of this service is to be able to fetch a thumbnail from
 * a collection item, respecting the given constraints (e.g. height).
 */
@Injectable()
export class ThumbnailRetrieverService {
  /** @type Map<string (thumbnail-id), string|ArrayBuffer (data)> */
  private cache: Map<string, string|ArrayBuffer> = new Map<string, string|ArrayBuffer>();

  constructor(private fileRetrievalService: FileRetrievalService) {}

  /**
   * This method will try to return the thumbnail that belongs
   * to the given item respecting the given criteria (height).
   *
   * @param {CollectionItem} item
   * @param {number} height
   *
   * @return Observable<string|ArrayBuffer>
   */
  getThumbnail(item: CollectionItem, height: number): Observable<string|ArrayBuffer> {
    let thumbnail = item.getThumbnailWithHeight(height);

    if(!thumbnail) {
      return of(null);
    }

    let cachedThumbnail = this.getFromCache(thumbnail);

    if(cachedThumbnail) {
      return of(cachedThumbnail);
    }

    return this.fileRetrievalService
      .getFile(thumbnail.fileUrl)
      .pipe(
        filter(data => data !== null),
        flatMap((blob, _) => {
          let reader = new FileReader();

          reader.readAsDataURL(blob);

          return fromEvent(reader, 'load')
            .pipe(map(result => { return reader.result; }));
        }),
        map(image => {
          this.storeInCache(thumbnail, image);
          return image;
        }),
      );
  }

  /**
   * Tries to obtain the data from the cache (if any)
   *
   * @param {CollectionItemThumbnail} thumbnail
   *
   * @return string|ArrayBuffer|null
   */
  private getFromCache(thumbnail: CollectionItemThumbnail) {
    return this.cache.get(thumbnail.id);
  }

  /**
   * Stores the given data inside the 'cache'
   *
   * @param {CollectionItemThumbnail} thumbnail
   * @param {string|ArrayBuffer} data
   */
  private storeInCache(thumbnail: CollectionItemThumbnail, data: string|ArrayBuffer) {
    this.cache.set(thumbnail.id, data);
  }
}
