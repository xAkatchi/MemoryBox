import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../../environments/environment";
import {map} from "rxjs/internal/operators";
import {Collection} from "../model/collection";
import {PhotoDetails} from "../model/file-details-photo";
import {User} from "../../../../authentication/core/model/user";
import {CollectionItem} from "../model/collection-item";
import {CollectionItemThumbnail} from "../model/collection-item-thumbnail";

/**
 * The goal of this service is to be able to interact with the collection
 * endpoints from the backend.
 */
@Injectable()
export class CollectionService {
    constructor(private http: HttpClient) { }

    /**
     * Tries to create a collection with respect to the given details.
     *
     * @param {string} name
     * @param {string} description
     *
     * @return {Observable<Collection>}
     */
    create(name: string, description: string) {
        return this.http
            .post<any>(
                environment.baseUrl + '/collection',
                { name, description },
            )
            .pipe(map(response => {
                return new Collection(
                    response.slug,
                    response.name,
                    response.description,
                    new Date(response.createdAt)
                )
            }))
    }

    /**
     * Returns all the collections that the current user has access to
     *
     * @return {Observable<Collection[]>}
     */
    getAllCollectionsFromCurrentUser() {
        return this.http
            .get<any>(
                environment.baseUrl + '/collection',
            )
            .pipe(map(response => {
                return response.map(responseCollection =>
                    new Collection(
                        responseCollection.slug,
                        responseCollection.name,
                        responseCollection.description,
                        new Date(responseCollection.createdAt)
                    )
                );
            }))
    }

    /**
     * Returns all the CollectionItem[] objects that belong to the given
     * Collection
     *
     * @param slug The slug of the Collection
     *
     * @return {Observable<CollectionItem[]>}
     */
    getItems(slug: string) {
        return this.http
            .get<any>(
                environment.baseUrl + '/collection/' + slug + '/items',
            )
            .pipe(map(response => {
                // TODO move this building to a factory!
                return response.map(collectionItem => {
                    let fileDetails = new PhotoDetails(
                        collectionItem.fileDetails.fileSize,
                        collectionItem.fileDetails.contentType,
                        collectionItem.fileDetails.height,
                        collectionItem.fileDetails.width,
                    );
                    let user = new User(
                        collectionItem.uploadedBy.id,
                        collectionItem.uploadedBy.firstName,
                        collectionItem.uploadedBy.lastName,
                        collectionItem.uploadedBy.email,
                        collectionItem.uploadedBy.avatarURL,
                    );

                    let thumbnails = collectionItem.thumbnails.map(thumbnail => {
                        return new CollectionItemThumbnail(
                            thumbnail.id,
                            new PhotoDetails(
                                thumbnail.fileDetails.fileSize,
                                thumbnail.fileDetails.contentType,
                                thumbnail.fileDetails.height,
                                thumbnail.fileDetails.width,
                            ),
                            thumbnail.fileUrl
                        )
                    });

                    return new CollectionItem(
                        collectionItem.slug,
                        collectionItem.name,
                        collectionItem.fileUrl,
                        fileDetails,
                        new Date(collectionItem.createdAt),
                        thumbnails,
                        user
                    )
                });
            }))
    }

    /**
     * Obtains and returns the collection with the given slug (if any)
     *
     * @param {string} slug
     * @return {Observable<Collection>}
     */
    getCollection(slug: string) {
        return this.http
            .get<any>(
                environment.baseUrl + '/collection/' + slug,
            )
            .pipe(map(response => {
                // TODO move this building to a factory!
                let members = response.members.map(member => {
                   return new User(
                       member.id,
                       member.firstName,
                       member.lastName,
                       member.email,
                       member.avatarURL
                   )
                });

                return new Collection(
                    response.slug,
                    response.name,
                    response.description,
                    new Date(response.createdAt),
                    members
                );
            }))
    }

    /**
     * Tries to add the user (identified by the given email)
     * to the given collection.
     *
     * @param {string} collectionSlug
     * @param {string} inviteeEmail
     *
     * @return {Observable<User>}
     */
    addMember(collectionSlug: string, inviteeEmail: string) {
      return this.http
        .post<any>(
          environment.baseUrl + '/collection/' + collectionSlug + '/members',
          { email: inviteeEmail }
        )
        .pipe(map(response => {
          return new User(
            response.id,
            response.firstName,
            response.lastName,
            response.email,
            response.avatarURL
          )
        }))
  }
}
