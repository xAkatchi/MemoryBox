import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../../environments/environment";
import {map} from "rxjs/operators";
import {PhotoDetails} from "../model/file-details-photo";
import {User} from "../../../../authentication/core/model/user";
import {CollectionItemThumbnail} from "../model/collection-item-thumbnail";
import {CollectionItem} from "../model/collection-item";

/**
 * The goal of this service is to be able to interact with the collection item
 * endpoints of the bakcned
 */
@Injectable()
export class CollectionItemService {
  constructor(private http: HttpClient) { }

  /**
   * Tries to upload the given file for the given collection
   *
   * @param {string} collectionSlug
   * @param {File} file
   * @param {string} name
   *
   * @return {Observable<CollectionItem>}
   */
  uploadItem(collectionSlug: string, file: File, name: string) {
    let formData = new FormData();
    formData.append('file', file);
    formData.append('metadata', JSON.stringify({
      'name': name
    }));

    return this.http
      .post<any>(
        environment.baseUrl + '/collection/' + collectionSlug + '/item',
        formData
      )
      .pipe(map(response => {
        // TODO move this building to a factory!
        let fileDetails = new PhotoDetails(
          response.fileDetails.fileSize,
          response.fileDetails.contentType,
          response.fileDetails.height,
          response.fileDetails.width,
        );
        let user = new User(
          response.uploadedBy.id,
          response.uploadedBy.firstName,
          response.uploadedBy.lastName,
          response.uploadedBy.email,
          response.uploadedBy.avatarURL,
        );

        let thumbnails = response.thumbnails.map(thumbnail => {
          return new CollectionItemThumbnail(
            thumbnail.id,
            new PhotoDetails(
              thumbnail.fileDetails.fileSize,
              thumbnail.fileDetails.contentType,
              thumbnail.fileDetails.height,
              thumbnail.fileDetails.width,
            ),
            thumbnail.fileUrl
          )
        });

        return new CollectionItem(
          response.slug,
          response.name,
          response.fileUrl,
          fileDetails,
          new Date(response.createdAt),
          thumbnails,
          user
        );
      }));
  }
}
