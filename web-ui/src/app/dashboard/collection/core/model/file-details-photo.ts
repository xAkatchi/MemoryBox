import {FileDetails} from "./file-details";

export class PhotoDetails extends FileDetails {
    height: number;
    width: number;

    constructor(fileSize: number, contentType: string, height: number, width: number) {
        super(fileSize, contentType);
        this.height = height;
        this.width = width;
    }
}