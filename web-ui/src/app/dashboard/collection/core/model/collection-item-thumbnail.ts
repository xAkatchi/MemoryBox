import {PhotoDetails} from "./file-details-photo";

export class CollectionItemThumbnail {
    id: string;
    fileDetails: PhotoDetails;
    fileUrl: string;

    constructor(id: string, fileDetails: PhotoDetails, fileUrl: string) {
        this.id = id;
        this.fileDetails = fileDetails;
        this.fileUrl = fileUrl;
    }
}