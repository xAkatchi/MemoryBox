export class FileDetails {
    fileSize: number;
    contentType: string;

    constructor(fileSize: number, contentType: string) {
        this.fileSize = fileSize;
        this.contentType = contentType;
    }
}