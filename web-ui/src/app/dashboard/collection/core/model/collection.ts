import {User} from "../../../../authentication/core/model/user";

export class Collection {
    slug: string;
    name: string;
    description: string;
    createdAt: Date;
    members?: Array<User>;

    constructor(
        slug: string,
        name: string,
        description: string,
        createdAt: Date,
        members?: Array<User>
    ) {
        this.slug = slug;
        this.name = name;
        this.description = description;
        this.createdAt = createdAt;
        this.members = members;
    }
}