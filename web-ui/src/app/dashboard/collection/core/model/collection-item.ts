import {User} from "../../../../authentication/core/model/user";
import {FileDetails} from "./file-details";
import {CollectionItemThumbnail} from "./collection-item-thumbnail";

export class CollectionItem {
    slug: string;
    name: string;
    fileUrl: string;
    fileDetails: FileDetails;
    createdAt: Date;
    thumbnails: Array<CollectionItemThumbnail>;
    uploadedBy: User;

    constructor(
        slug: string,
        name: string,
        fileUrl: string,
        fileDetails: FileDetails,
        createdAt: Date,
        thumbnails: Array<CollectionItemThumbnail>,
        uploadedBy: User
    ) {
        this.slug = slug;
        this.name = name;
        this.fileUrl = fileUrl;
        this.fileDetails = fileDetails;
        this.createdAt = createdAt;
        this.thumbnails = thumbnails;
        this.uploadedBy = uploadedBy;
    }

    /**
     * Returns the thumbnail that matches the given height
     * (if any).
     *
     * @param {number} height
     *
     * @return {CollectionItemThumbnail|null}
     */
    public getThumbnailWithHeight(height: number) {
        return this.thumbnails.filter(thumbnail => {
            return thumbnail && thumbnail.fileDetails.height === height;
        })[0];
    }
}