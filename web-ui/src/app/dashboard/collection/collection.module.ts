import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {RouterModule} from "@angular/router";
import {NgxSmartModalModule} from "ngx-smart-modal";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DeferLoadModule} from "@trademe/ng-defer-load";
import {CollectionOverviewComponent} from "./overview/overview.component";
import {CollectionCreateComponent} from "./overview/create/create.component";
import {CollectionService} from "./core/services/collection.service";
import {DetailModule} from "./detail/detail.module";
import { CollectionComponent } from './overview/collection/collection.component';

@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    RouterModule,
    NgxSmartModalModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    FormsModule,
    DeferLoadModule,
    CommonModule,

    DetailModule,
  ],
  declarations: [
    CollectionOverviewComponent,
    CollectionCreateComponent,
    CollectionComponent,
  ],
  providers: [
    CollectionService,
  ]
})
export class CollectionModule {
}
