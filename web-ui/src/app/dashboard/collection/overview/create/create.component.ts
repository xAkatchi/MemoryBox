import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {CollectionService} from "../../core/services/collection.service";
import {first} from "rxjs/internal/operators";
import {NgbModal, NgbModalRef} from "@ng-bootstrap/ng-bootstrap";
import {Collection} from "../../core/model/collection";
import {ToasterService} from "angular2-toaster";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";

@Component({
    selector: 'app-dashboard-collection-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss'],
    providers: [CollectionService, FontAwesomeModule]
})
export class CollectionCreateComponent implements OnInit {
    collectionCreateForm: FormGroup;
    loading = false;
    submitted = false;
    createCollectionModal: NgbModalRef;
    @Output() onCollectionSaved = new EventEmitter<Collection>();
    @Input() searchQuery: string;

    constructor(private formBuilder: FormBuilder,
                private ngbModalService: NgbModal,
                private collectionService: CollectionService,
                private toasterService: ToasterService) {
    }

    /**
     * Initializes all the parameters whenever this component is being initialized
     */
    ngOnInit() {
        this.collectionCreateForm = this.formBuilder.group({
            name: ['', Validators.required],
            description: ['', Validators.nullValidator],
        });
    }

    /**
     * Opens the modal with the given identifier
     *
     * @param {object} identifier
     */
    open(identifier: object) {
        this.form.name.setValue(this.searchQuery);
        this.createCollectionModal = this.ngbModalService.open(identifier);
    }

    /**
     * This method will obtain the entered values and tries to post them to create
     * a collection.
     */
    onSubmit() {
        this.submitted = true;

        if (this.collectionCreateForm.invalid) {
            return;
        }

        this.loading = true;
        this.collectionService
            .create(this.form.name.value, this.form.description.value)
            .pipe(first())
            .subscribe(
                data => {
                    this.loading = false;
                    this.toasterService.popAsync('success', 'Collection created');
                    this.onCollectionSaved.emit(data);
                    this.createCollectionModal.close();
                },
                error => {
                    this.toasterService.popAsync('error', 'Failed to create the collection', error);
                    this.loading = false;
                });
    }

    /**
     * A convenience getter to make it possible to easily access the form fields.
     *
     * @return {AbstractControl}
     */
    get form() {
        return this.collectionCreateForm.controls;
    }
}
