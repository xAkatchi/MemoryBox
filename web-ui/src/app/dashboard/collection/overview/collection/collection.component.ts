import {Component, Input} from '@angular/core';
import {Collection} from "../../core/model/collection";
import {Router} from "@angular/router";

@Component({
  selector: 'app-dashboard-collection-overview-collection',
  templateUrl: './collection.component.html',
  styleUrls: ['./collection.component.scss']
})
export class CollectionComponent {
  @Input() collection: Collection;

  constructor(private router: Router) {}

  /**
   * Redirects the user to the collection that this component
   * 'wraps'.
   */
  openCollection() {
    this.router.navigate(['dashboard/collection', this.collection.slug]);
  }
}
