import {Component, OnInit} from '@angular/core';
import {Collection} from "../core/model/collection";
import {ActivatedRoute} from "@angular/router";

@Component({
    selector: 'app-dashboard-collection-overview',
    templateUrl: './overview.component.html',
    styleUrls: ['./overview.component.scss']
})
export class CollectionOverviewComponent implements OnInit {
    private collections: Collection[] = [];
    filteredCollections: Collection[] = [];
    shouldShow = [];
    searchQuery: string;

    constructor(private route: ActivatedRoute) { }

    /**
     * Obtains all the required details for this component whenever it's
     * being initialized.
     */
    ngOnInit() {
      this.route.data.subscribe((data: { collections: Collection[] }) => {
        this.collections = data.collections;
        this.filteredCollections = data.collections;
      });
    }

    /**
     * This method will be invoked every time our child has a saved collection
     *
     * @param {Collection} collection
     */
    onCollectionSaved(collection: Collection) {
      this.collections.push(collection);
    }

  /**
   * Filters out all the collections that don't match the given criteria
   *
   * @param {string} collectionName The required name of the collection
   */
  filterCollections(collectionName: string) {
      if (!collectionName) {
        this.filteredCollections = this.collections;
        return;
      }

      this.filteredCollections = this.collections
        .filter(item => {
          return item.name.toLowerCase().indexOf(collectionName.toLowerCase()) > -1
        })
    }
}
