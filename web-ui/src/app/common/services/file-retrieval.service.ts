import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";

/**
 * The goal of this service is to be able to obtain a file
 * from the given Url
 */
@Injectable()
export class FileRetrievalService {
    constructor(private http: HttpClient) { }

    /**
     * Returns the file that is being housed at the given file url.
     *
     * @param {string} fileUrl
     * @return {Observable<Blob>}
     */
    getFile(fileUrl: string) {
        return this.http
            .get(
                environment.baseUrl + fileUrl,
                {responseType: 'blob'}
            );
    }
}
