import {Injectable} from "@angular/core";

/**
 * This util class contains logic for interacting with bytes.
 */
@Injectable()
export class ByteUtils {
    /**
     * This method transforms the given amount of bytes to a human readable format.
     *
     * Obtained from {@link https://stackoverflow.com/a/23625419}
     *
     * @param {number} bytes
     *
     * @return {string}
     */
    public bytesToSize(bytes: number): string {
        if(bytes < 1024) return bytes + " Bytes";
        else if(bytes < 1048576) return(bytes / 1024).toFixed(3) + " KB";
        else if(bytes < 1073741824) return(bytes / 1048576).toFixed(3) + " MB";
        else return(bytes / 1073741824).toFixed(3) + " GB";
    };
}