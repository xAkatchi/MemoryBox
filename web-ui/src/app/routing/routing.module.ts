import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {LoginComponent} from '../authentication/login/login.component';
import {SignUpComponent} from '../authentication/sign-up/sign-up.component';
import {LandingPageComponent} from '../landing-page/landing-page.component';
import {AuthGuard} from "../authentication/core/guard/auth.guard";
import {CollectionOverviewComponent} from "../dashboard/collection/overview/overview.component";
import {CollectionDetailComponent} from "../dashboard/collection/detail/detail.component";
import {PhotoComponent} from "../dashboard/collection/detail/photo/photo.component";
import {DashboardComponent} from "../dashboard/dashboard.component";
import {DetailComponent} from "../dashboard/collection/detail/detail/detail.component";
import {ProfileComponent} from "../dashboard/user/profile/profile.component";
import {LoggedInUserResolverService} from "../dashboard/user/logged-in-user-resolver.service";
import {UserCollectionsResolverService} from "../dashboard/collection/core/resolvers/user-collections-resolver.service";
import {CollectionItemsResolverService} from "../dashboard/collection/core/resolvers/collection-items-resolver.service";
import {CollectionDetailsResolverService} from "../dashboard/collection/core/resolvers/collection-details-resolver.service";

@NgModule({
  imports: [
    RouterModule.forRoot([
      {path: '', component: LandingPageComponent},
      {path: 'login', component: LoginComponent},
      {path: 'sign_up', component: SignUpComponent},
      {
        path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard], resolve: { user: LoggedInUserResolverService }, children: [
          {path: '', redirectTo: 'collections', pathMatch: 'full'},
          {path: 'profile', component: ProfileComponent, resolve: { user: LoggedInUserResolverService }},
          {path: 'collections', component: CollectionOverviewComponent, resolve: { collections: UserCollectionsResolverService }},
          {
            path: 'collection/:slug', component: CollectionDetailComponent, children: [
              {path: '', redirectTo: 'photos', pathMatch: 'full'},
              {path: 'photos', component: PhotoComponent, resolve: { items: CollectionItemsResolverService }},
              {path: 'details', component: DetailComponent, resolve: { collection: CollectionDetailsResolverService }},
            ]
          },
        ]
      },
    ])
  ],
  exports: [RouterModule]
})
export class RoutingModule {
}
