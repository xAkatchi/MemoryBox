import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';

import {AppComponent} from './app.component';
import {LandingPageModule} from './landing-page/landing-page.module';
import {AuthenticationModule} from './authentication/authentication.module';
import {RoutingModule} from './routing/routing.module';
import {JwtInterceptor} from './authentication/core/interceptor/jwt.interceptor';
import {ErrorInterceptor} from './authentication/core/interceptor/error.interceptor';
import {DashboardModule} from "./dashboard/dashboard.module";
import {NgxSmartModalModule} from "ngx-smart-modal";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import { library } from '@fortawesome/fontawesome-svg-core';
import { far } from '@fortawesome/pro-regular-svg-icons';
import {AvatarModule} from "ngx-avatar";
import {NgProgressModule} from "@ngx-progressbar/core";
import {NgProgressHttpModule} from "@ngx-progressbar/http";
import {NgProgressRouterModule} from "@ngx-progressbar/router";
import {ToasterModule} from "angular2-toaster";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

library.add(far);

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        RoutingModule,
        HttpClientModule,

        NgbModule.forRoot(),
        NgxSmartModalModule.forRoot(),
        FontAwesomeModule,
        AvatarModule.forRoot({
            colors: ["#582707", "#972d07", "#ffd600", "#ff4b3e", "#91cb3e"]
        }),
        NgProgressModule.forRoot({
          color: '#ffd600'
        }),
        NgProgressHttpModule.forRoot(),
        NgProgressRouterModule.forRoot(),
        BrowserAnimationsModule,
        ToasterModule.forRoot(),

        LandingPageModule,
        AuthenticationModule,
        DashboardModule,
    ],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
