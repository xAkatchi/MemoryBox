import {Component, ViewChild, ElementRef, Input} from '@angular/core';

import PhotoSwipe from 'photoswipe';
import PhotoSwipeUI_Default from 'photoswipe/dist/photoswipe-ui-default';
import {PhotoSwipeImage} from "./image";

/**
 * This component can be used as gallery.
 *
 * Internally it uses the photoswipe system.
 *
 * {@link https://github.com/Gadhami/ng4-PhotoSwipe}
 * {@link http://photoswipe.com/}
 */
@Component({
    selector: 'app-photo-swipe',
    templateUrl: './photo-swipe.component.html',
})
export class PhotoSwipeComponent {
    @ViewChild('photoSwipe') photoSwipe: ElementRef;
    @Input() images: PhotoSwipeImage[] = [];

    /**
     * Opens the gallery for the given array of images at the given slide.
     *
     * @param {number} startAtSlide
     */
    openGallery(startAtSlide: number = 0) {
        const options = {
            index: startAtSlide,
            history: false,
            preload: [1, 3],
            shareEl: false,
        };

        const gallery = new PhotoSwipe(this.photoSwipe.nativeElement, PhotoSwipeUI_Default, this.images, options);
        gallery.init();
    }
}
