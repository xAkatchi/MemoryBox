/**
 * A class representing an image for the Photoswipe library
 */
export interface PhotoSwipeImage
{
    src: string,
    w: number,
    h: number,
}
