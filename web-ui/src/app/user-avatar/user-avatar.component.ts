import {Component, Input} from '@angular/core';
import {User} from "../authentication/core/model/user";
import {AvatarModule} from "ngx-avatar";

@Component({
    selector: 'app-common-user',
    templateUrl: './user-avatar.component.html',
    providers: [AvatarModule]
})
export class UserAvatarComponent {
    @Input() user: User;
    @Input() size: number = 50;
    @Input() showTooltip: boolean = true;
}
