import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UserAvatarComponent} from "./user-avatar.component";
import {AvatarModule} from "ngx-avatar";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";

@NgModule({
  imports: [
    CommonModule,

    NgbModule,
    AvatarModule
  ],
  declarations: [
    UserAvatarComponent
  ],
  exports: [
    UserAvatarComponent
  ]
})
export class UserAvatarModule { }
