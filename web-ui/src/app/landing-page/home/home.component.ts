import {Component} from '@angular/core';
import {NgbCarouselConfig} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-landing-page-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [NgbCarouselConfig]
})
export class HomeComponent {
  constructor(carousel: NgbCarouselConfig) {
    carousel.interval = 10000;
    carousel.wrap = true;
    carousel.keyboard = false;
    carousel.showNavigationArrows = false;
  }
}
