import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NavbarComponent} from './navbar/navbar.component';
import {LandingPageComponent} from './landing-page.component';
import {HomeComponent} from './home/home.component';
import {RouterModule} from '@angular/router';

@NgModule({
    imports: [
        CommonModule,
        NgbModule,
        RouterModule,
    ],
    declarations: [LandingPageComponent, NavbarComponent, HomeComponent]
})
export class LandingPageModule {
}
