import {Component} from '@angular/core';

@Component({
    selector: 'app-landing-page-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {
    isNavbarCollapsed: Boolean = true;
}
