import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { first } from "rxjs/internal/operators";
import { SignUpService } from "./sign-up.service";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {ToasterService} from "angular2-toaster";

/**
 * 'Controller' for the sign up page
 */
@Component({
    selector: 'app-authentication-sign-up',
    templateUrl: './sign-up.component.html',
    styleUrls: ['./sign-up.component.scss'],
    providers: [SignUpService, FontAwesomeModule]
})
export class SignUpComponent implements OnInit {
    signUpForm: FormGroup;
    loading = false;
    submitted = false;
    error = '';

    constructor(private formBuilder: FormBuilder,
                private router: Router,
                private signUpService: SignUpService,
                private toasterService: ToasterService) {
    }

    /**
     * Initializes all the parameters whenever this component is being initialized
     */
    ngOnInit() {
        this.signUpForm = this.formBuilder.group({
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(128)]],
        });
    }

    /**
     * This method will obtain the entered values and tries to sign them up
     * (if they are valid).
     */
    onSubmit() {
        this.submitted = true;

        if (this.signUpForm.invalid) {
            return;
        }

        this.loading = true;
        this.signUpService
            .register(
                this.form.firstName.value,
                this.form.lastName.value,
                this.form.email.value,
                this.form.password.value
            )
            .pipe(first())
            .subscribe(
                data => {
                    this.toasterService.popAsync('success', 'Account created! please check your email.');
                    this.router.navigate(['/login']);
                },
                error => {
                    this.error = error;
                    this.loading = false;
                });
    }

    /**
     * A convenience getter to make it possible to easily access the form fields.
     *
     * @return {AbstractControl}
     */
    get form() {
        return this.signUpForm.controls;
    }
}
