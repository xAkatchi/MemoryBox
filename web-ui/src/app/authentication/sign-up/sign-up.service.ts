import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { AuthToken } from "../core/model/auth-token";
import { AuthTokenService } from "../core/service/auth-token.service";

/**
 * This class contains all the 'sign up' related logic
 */
@Injectable()
export class SignUpService {
    constructor(private http: HttpClient, private authTokenService: AuthTokenService) { }

    /**
     * This method tries to register an account with respect to the given credentials.
     *
     * If the registration was successful, the user can directly access protected pages
     * due to that an authentication token is stored.
     *
     * @param {string} firstName
     * @param {string} lastName
     * @param {string} email
     * @param {string} password
     *
     * @return {Observable<boolean>} Indicating whether or not the registration was successful
     */
    register(firstName: string, lastName: string, email: string, password: string) {
        return this.http
            .post<any>(
                environment.baseUrl + '/register',
                { firstName, lastName, email, password },

            )
            .pipe(map(response => {
                if (response && response.token) {
                    const token = new AuthToken(response.token);

                    this.authTokenService.storeAuthToken(token);
                    return true;
                }

                return false;
            }));
    }
}
