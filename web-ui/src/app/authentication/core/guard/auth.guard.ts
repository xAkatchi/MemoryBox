import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthTokenService } from '../service/auth-token.service';

/**
 * The auth guard is used to prevent unauthenticated users from accessing restricted routes,
 *
 * {@link https://blog.thoughtram.io/angular/2016/07/18/guards-in-angular-2.html}
 */
@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {

    constructor(private router: Router, private authTokenService: AuthTokenService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (this.authTokenService.getAuthToken()) {
            return true;
        }

        this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});

        return false;
    }
}
