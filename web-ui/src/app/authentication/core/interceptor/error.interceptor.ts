import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { AuthTokenService } from '../service/auth-token.service';

/**
 * The Error Interceptor intercepts http responses from the api to check if there were any errors.
 * If there is a 401 Unauthorized response the user is automatically logged out of the application,
 * all other errors are re-thrown to be caught by the calling service so an alert can be displayed to the user.
 */
@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(private authTokenService: AuthTokenService) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next
            .handle(request)
            .pipe(catchError(err => {
                if (err.status === 401) {
                    this.authTokenService.deleteToken();
                    location.reload(true);
                }

                return throwError(err.statusText);
            }))
        ;
    }
}
