import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import {AuthTokenService} from '../service/auth-token.service';

/**
 * The JWT Interceptor intercepts http requests from the application
 * to add a JWT auth token to the Authorization header if the user is logged in.
 */
@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(private authTokenService: AuthTokenService) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const token = this.authTokenService.getAuthToken();

        if (token && token.token) {
            request = request.clone({
                headers: request.headers.append('X-Auth-Token', token.token),
            });
        }

        return next.handle(request);
    }
}
