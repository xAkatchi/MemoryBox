export class User {
    id: string;
    firstName: string;
    lastName: string;
    email: string;
    avatarUrl: string;

    constructor(id: string, firstName: string, lastName: string, email: string, avatarUrl: string) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.avatarUrl = avatarUrl;
    }

    /**
     * Returns the full name of the user
     *
     * @return string
     */
    getFullName(): string {
        return this.firstName + ' ' + this.lastName;
    }
}
