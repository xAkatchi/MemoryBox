import { Injectable } from '@angular/core';
import { AuthToken } from "../model/auth-token";

/**
 * This class handles all the logic with regards to the authentication token storage
 */
@Injectable({ providedIn: 'root' })
export class AuthTokenService {
    /**
     * Returns the auth token of the currently logged in user (if any)
     *
     * @return {AuthToken|null}
     */
    getAuthToken(): AuthToken|null {
        const token = localStorage.getItem('token');

        if(token) {
            return JSON.parse(token);
        }

        return null;
    }

    /**
     * Stores the given AuthToken for later reuse
     *
     * @param {AuthToken} token
     */
    storeAuthToken(token: AuthToken): void {
        localStorage.setItem('token', JSON.stringify(token));
    }

    /**
     * Deletes the stored token (if any)
     */
    deleteToken(): void {
        localStorage.removeItem('token');
    }
}
