import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { LoginService } from "./login.service";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";

/**
 * 'Controller' for the login/sign in page
 */
@Component({
    selector: 'app-authentication-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    providers: [LoginService, FontAwesomeModule]
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    error = '';

    constructor(private formBuilder: FormBuilder,
                private route: ActivatedRoute,
                private router: Router,
                private loginService: LoginService) {
    }

    /**
     * Initializes all the parameters whenever this component is being initialized
     */
    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', Validators.required]
        });

        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/dashboard';
    }

    /**
     * This method will obtain the entered credentials and tries to authenticate them
     * (if they are valid).
     */
    onSubmit() {
        this.submitted = true;

        if (this.loginForm.invalid) {
            return;
        }

        this.loading = true;
        this.loginService
            .login(this.form.email.value, this.form.password.value)
            .pipe(first())
            .subscribe(
                data => {
                    this.router.navigate([this.returnUrl]);
                },
                error => {
                    this.error = error;
                    this.loading = false;
                });
    }

    /**
     * A convenience getter to make it possible to easily access the form fields.
     *
     * @return {AbstractControl}
     */
    get form() {
        return this.loginForm.controls;
    }
}
