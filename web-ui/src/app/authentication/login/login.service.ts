import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { AuthToken } from "../core/model/auth-token";
import { AuthTokenService } from "../core/service/auth-token.service";

/**
 * This class contains all the 'login' related logic
 */
@Injectable()
export class LoginService {
    constructor(private http: HttpClient, private authTokenService: AuthTokenService) { }

    /**
     * This method tries to log the given credentials in.
     *
     * If the login was successful, the token will be stored for subsequent requests.
     *
     * @param {string} email
     * @param {string} password
     *
     * @return {Observable<boolean>} Indicating whether or not the login was successful
     */
    login(email: string, password: string) {
        return this.http
            .post<any>(
                environment.baseUrl + '/login',
                { email, password }
            )
            .pipe(map(response => {
                if (response && response.token) {
                    const token = new AuthToken(response.token);

                    this.authTokenService.storeAuthToken(token);
                    return false;
                }

                return false;
            }));
    }
}
