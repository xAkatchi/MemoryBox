import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {LoginComponent} from './login/login.component';
import {SignUpComponent} from './sign-up/sign-up.component';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";

@NgModule({
    imports: [
        CommonModule,
        NgbModule,
        RouterModule,
        ReactiveFormsModule,
        FontAwesomeModule,
        FormsModule
    ],
    declarations: [LoginComponent, SignUpComponent],
})
export class AuthenticationModule {
}
