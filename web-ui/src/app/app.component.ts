import {Component} from '@angular/core';
import {ToasterConfig} from "angular2-toaster";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  // See: https://www.npmjs.com/package/angular2-toaster#configurable-options
  toasterConfig: ToasterConfig =
    new ToasterConfig({
      limit: 5,
      timeout: 3000,
      mouseoverTimerStop: true,
      animation: 'fade',
      showCloseButton: true,
    })
}
